//
//  HD_AllMemerAnswerVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_AllMemerAnswerVC.h"
#import "HDSubjectVC.h"
#import "QuickAnswerResultView.h"
#import "TeamStudent.h"

@interface HD_AllMemerAnswerVC ()<QuickAnswerResultViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnGo;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *containerView;


@property (strong, nonatomic) QuickAnswerResultView *answerView;

@end

@implementation HD_AllMemerAnswerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    self.answerView  = [QuickAnswerResultView instanceView];
    self.answerView.delegate = self;
    _btnGo.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
//    [self showCoverView];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _btnGo.hidden = YES;
    _btnStart.hidden = NO;
    _lblTitle.text = @"倒计时结束时，\n按下答题键开始抢答！";
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == QuickAnswerState) {
       //跳转到抢答成功或者抢答失败
        TeamStudent *studentM = [TeamStudent mj_objectWithKeyValues:dic];
        self.answerView.model = studentM;
        [self.containerView addSubview:self.answerView];
    }
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_AllAnswer"]) {
        if (open == 1) {
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}

- (IBAction)startAction:(UIButton *)sender {
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartQuickAnswer) successBlock:^(id result) {
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
}

- (void)updateLabel
{
    self.lblTitle.text = @"按下答题键开始抢答！";
}

- (IBAction)goAction:(UIButton *)sender {

}

- (IBAction)closeAction:(UIButton *)sender {
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    //重置界面
    [self.answerView removeFromSuperview];
    _lblTitle.text = @"倒计时结束时，\n按下答题键开始抢答！";
    self.btnStart.hidden = NO;
    self.btnGo.hidden = YES;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateLabel) object:nil];
}
- (void)startAction{
    [self removeCoverView];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(QuickAnswer) successBlock:^(id result) {
    }];
}
#pragma mark === QuickAnswerResultViewDelegate =====
- (void)closeView
{
    [self.answerView removeFromSuperview];
    _lblTitle.text = @"倒计时结束时，\n按下答题键开始抢答！";
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartQuickAnswer) successBlock:^(id result) {
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
}
- (void)closeWindow
{
    [self closeAction:nil];
}




- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
