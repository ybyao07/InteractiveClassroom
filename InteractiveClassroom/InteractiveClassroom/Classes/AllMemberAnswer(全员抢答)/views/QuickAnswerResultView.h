//
//  QuickAnswerResultView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamStudent.h"

@protocol QuickAnswerResultViewDelegate<NSObject>

- (void)closeView;
- (void)closeWindow;

@end

@interface QuickAnswerResultView : UIView

@property (weak, nonatomic) IBOutlet UIButton *btnPraise;
@property (weak, nonatomic) IBOutlet UIButton *btnAgain;

@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (nonatomic, weak) id<QuickAnswerResultViewDelegate> delegate;

@property (nonatomic, strong) TeamStudent *model;

+(QuickAnswerResultView *)instanceView;

@end
