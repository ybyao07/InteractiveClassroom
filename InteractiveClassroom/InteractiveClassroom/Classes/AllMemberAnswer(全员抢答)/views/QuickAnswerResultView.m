//
//  QuickAnswerResultView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "QuickAnswerResultView.h"
#import "UIView+Extension.h"
#import "Masonry.h"
@implementation QuickAnswerResultView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0,0, SCREEN_WIDTH - 60, SCREEN_HEIGHT - 100);
}

- (IBAction)closeAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(closeWindow)]) {
        [self.delegate closeWindow];
    }
}

//表扬
- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(1)} commandCode:@(Praise) successBlock:^(id result) {
    }];
}
//再次抢答
- (IBAction)reaginAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(closeView)]) {
        [self.delegate closeView];
    }
}

- (void)setModel:(TeamStudent *)model
{
    _model = model;
    [self.btnStatus setTitle:model.studentName forState:UIControlStateNormal];
    if (_model.studentName.length > 0) {
        self.btnPraise.hidden = NO;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"quick_answer_success"] forState:UIControlStateNormal];
        [self.btnStatus setTitle:model.studentName forState:UIControlStateNormal];
        self.lblStatus.text = @"恭喜你抢答成功！";
        self.lblStatus.textColor = UIColorFromRGB(0xC01100);
    }else{
        self.btnPraise.hidden = YES;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"quick_answer_failure"] forState:UIControlStateNormal];
        [self.btnStatus setTitle:@"无" forState:UIControlStateNormal];
        self.lblStatus.text = @"本次无人抢答！";
        self.lblStatus.textColor = JUIColorFromRGB_Main1;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (_model.studentName.length <= 0) {
        self.btnAgain.frame = Rect((SCREEN_WIDTH - 60)/2.0-60, self.btnAgain.origin.y, 120, 40);
    }
}
+(QuickAnswerResultView *)instanceView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"QuickAnswerResultView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}


@end
