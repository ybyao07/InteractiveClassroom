//
//  HD_CallNameVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_CallNameVC.h"
#import "PopContainerVC.h"
#import "CallTypeModel.h"
#import "CallStudentModel.h"
#import "RandomGeneratorTool.h"
#import "TestStudentProgressCell.h"

#define Subject_CELL_WIDTH (SCREEN_WIDTH - 30*2 - 20*2 - 2*50  - 20*5)/6

@interface HD_CallNameVC ()<PopDeletegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, strong) UIPopoverController *popOver;
@property (weak, nonatomic) IBOutlet UIButton *btnCallType;
//数据
@property (strong, nonatomic) NSArray <CallTypeModel *> *callTypeArr;
@property (strong, nonatomic) CallTypeModel *currentMode;

@property (strong, nonatomic) NSArray <CallStudentModel *> *studentsData; //全部学生数据

@property (strong, nonatomic) NSMutableArray <CallStudentModel *> *collectionViewStudentsData; //举手点名学生数据

@property (strong, nonatomic) CallStudentModel *currentStudent;

@property (weak, nonatomic) IBOutlet UIButton *btnRandomName;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@property (weak, nonatomic) IBOutlet UIButton *btnCenter;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;

@end

@implementation HD_CallNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    [self.btnRandomName setTitleColor:JUIColorFromRGB_Main3 forState:UIControlStateNormal];
    self.btnRandomName.titleLabel.font = JFONT_BOLD(60);
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    NSArray *array = @[
                       @{
                           @"callModelName":@"随机点名",
                           @"callModeType":@"0"
                           },
                       @{
                           @"callModelName":@"花名册点名",
                           @"callModeType":@"1"
                           },
                       @{
                           @"callModelName":@"举手点名",
                           @"callModeType":@"2"
                           }
                          ];
    self.callTypeArr = [CallTypeModel mj_objectArrayWithKeyValuesArray:array];
    self.currentMode = self.callTypeArr.firstObject;
    self.lblTitle.text = self.currentMode.callModelName;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
    [self showCoverView];
//    [self addTestData];
}


- (void)sendInitSocket{
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":@(3)} commandCode:@(RollCall) successBlock:^(id result) {
    }];
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_Name"]) {
        if (open == 1) {
            [self sendInitSocket];
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"studentList":@[
                                  @{
                                      @"id":@"124",
                                      @"cardNum":@"2061205",
                                      @"studentName":@"张三"
                                      },
                                  @{
                                      @"id":@"125",
                                      @"cardNum":@"2061205",
                                      @"studentName":@"李四"
                                      }
                                  ]
                          };
    self.studentsData = [CallStudentModel mj_objectArrayWithKeyValuesArray:dic[@"studentList"]];
    [self removeLoadAnimation];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == StudentsList) { //获取学生列表
        self.studentsData = [CallStudentModel mj_objectArrayWithKeyValuesArray:dic[@"studentList"]];
        [self removeLoadAnimation];
    }else if ([dic[@"code"] integerValue] == RaiseHandsStudentInfo){//返回举手点名学生信息 --- 将对应的学生设置成true
        CallStudentModel *studentM = [CallStudentModel initWithStudentId:dic[@"studentId"] name:dic[@"studentName"]];
        studentM.isRaiseHand = NO;
        [self.collectionViewStudentsData addObject:studentM];
        [self.collectionView reloadData];
    }
}

- (IBAction)showPopVC:(UIButton *)sender {
    PopContainerVC *menuVC = [[PopContainerVC alloc] init];
    menuVC.delegate = self;
    NSMutableArray *strArray = [NSMutableArray arrayWithCapacity:self.callTypeArr.count];
    [self.callTypeArr enumerateObjectsUsingBlock:^(CallTypeModel * _Nonnull callType, NSUInteger idx, BOOL * _Nonnull stop) {
        [strArray addObject:callType.callModelName];
    }];
    menuVC.dataSource = strArray;
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:menuVC];
    // 设置内容的尺寸  如果内容控制器里设置了 preferredContentSize 大小 self.preferredContentSize = CGSizeMake(200, 200); 那么 self.popOver.popoverContentSize 将无意义，而且在实际开发中也应该由内容控制器控制大小
    self.popOver.popoverContentSize = CGSizeMake(200, strArray.count*50);
    // 这个方法是自定义出现的位置  Rect：箭头指向的区域范围，以View的左上角为坐标原点
    [self.popOver presentPopoverFromRect:Rect(150, SCREEN_HEIGHT - 130, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionViewStudentsData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    CallStudentModel *model = self.collectionViewStudentsData[indexPath.row];
    if (model.isRaiseHand) {
        cell.backgroundColor = JUIColorFromRGB_Main3;
    }else{
        cell.backgroundColor = JUIColorFromRGB_Major1;
    }
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CallStudentModel *model = self.collectionViewStudentsData[indexPath.row];
    model.isRaiseHand = !model.isRaiseHand;
    if ([self.currentMode.callModeType integerValue] == 1) {//花名册点名
        CallStudentModel *studentM = self.collectionViewStudentsData[indexPath.row];
        [HDSocketSendCommandTool sendSocketParam:@{
                                                   @"studentId":studentM.studentId,
                                                   @"mode":self.currentMode.callModeType,
                                                   @"isselected":@(model.isRaiseHand)
                                                   } commandCode:@(SelectedStudent) successBlock:^(id result) {
            
        }];
        [self.collectionView reloadData];
    }else if ([self.currentMode.callModeType integerValue] == 2){//举手点名
        CallStudentModel *studentM = self.collectionViewStudentsData[indexPath.row];
        [HDSocketSendCommandTool sendSocketParam:@{
                                                   @"studentId":studentM.studentId,
                                                   @"mode":self.currentMode.callModeType,
                                                   @"isselected":@(model.isRaiseHand)
                                                   } commandCode:@(SelectedStudent) successBlock:^(id result) {
                                                       
                                                   }];
        [self.collectionView reloadData];
    }
    
}

#pragma mark =========== PopContainerDelegate ===
- (void)changePopType:(int)typeIndex
{
    self.currentMode = self.callTypeArr[typeIndex];
    self.lblTitle.text = self.currentMode.callModelName;
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":self.currentMode.callModeType} commandCode:@(RollCall) successBlock:^(id result) {
    }];
    [self.btnCallType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentMode.callModelName] forState:UIControlStateNormal];
    [self initView];
}

- (void)initView
{
    if ([self.currentMode.callModeType intValue] == 0) {// 随机点名
        [self.btnRandomName setTitle:@"开始点名" forState:UIControlStateNormal];
        self.btnCenter.hidden = NO;
        self.btnLeft.hidden = YES;
        self.btnRight.hidden = YES;
        self.btnRandomName.hidden = NO;
        self.collectionView.hidden = YES;
        [self.btnCenter setTitle:@"开始点名" forState:UIControlStateNormal];
    }else if ([self.currentMode.callModeType intValue] == 1){ //花名册点名
        [self.collectionViewStudentsData removeAllObjects];
        [self.studentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CallStudentModel *newM = [obj copy];
            [self.collectionViewStudentsData addObject:newM];
        }];
        self.btnCenter.hidden = NO;
        self.btnLeft.hidden = YES;
        self.btnRight.hidden = YES;
        self.btnRandomName.hidden = YES;
        self.collectionView.hidden = NO;
        [self.btnCenter setTitle:@"表扬" forState:UIControlStateNormal];
        [self.collectionView reloadData];
    }else{//举手点名
        [self.collectionViewStudentsData removeAllObjects];
//        [self.studentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            CallStudentModel *newM = [obj copy];
//            [self.collectionViewStudentsData addObject:newM];
//        }];
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartRaiseHands) successBlock:^(id result) {
        }];
        self.btnCenter.hidden = YES;
        self.btnLeft.hidden = NO;
        self.btnRight.hidden = NO;
        self.btnRandomName.hidden = YES;
        self.collectionView.hidden = NO;
        [self.btnLeft setTitle:@"表扬" forState:UIControlStateNormal];
        [self.btnRight setTitle:@"刷新" forState:UIControlStateNormal];
        [self.collectionView reloadData];
    }
    [self.btnCallType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentMode.callModelName] forState:UIControlStateNormal];
}

#pragma mark ====== actions ====
- (IBAction)centerAction:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"开始点名"]) {
        [self sendRandomCallCodel];
    }else if ([sender.currentTitle isEqualToString:@"表扬"]){
        [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(2)} commandCode:@(Praise) successBlock:^(id result) {
        }];
        [self.collectionViewStudentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isRaiseHand = NO;
        }];
        [self.collectionView reloadData];
    }
}
- (IBAction)rightAction:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"再次点名"]){
        [self sendRandomCallCodel];
    }else if ([sender.currentTitle isEqualToString:@"刷新"]){
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartRaiseHands) successBlock:^(id result) {
        }];
        [self.collectionViewStudentsData removeAllObjects];
        [self.collectionView reloadData];
    }
}
- (IBAction)leftAction:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"表扬"]){
        [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(2)} commandCode:@(Praise) successBlock:^(id result) {
        }];
        [self.collectionViewStudentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isRaiseHand = NO;
        }];
        [self.collectionView reloadData];
    }
}


#pragma mark ===== 发送指令 =====
// 随机点名指令
- (void)sendRandomCallCodel
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    CallStudentModel *studentM = self.studentsData[[RandomGeneratorTool getRandomNumber:0 to:self.studentsData.count-1]];
    self.currentStudent = studentM;
    dic[@"studentId"] = studentM.studentId;;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartRandom) successBlock:^(id result) {
    }];
    self.btnCenter.hidden = YES;
    self.btnLeft.hidden = NO;
    self.btnRight.hidden = NO;
    [self.btnRandomName setTitle:self.currentStudent.studentName forState:UIControlStateNormal];
    [self.btnLeft setTitle:@"表扬" forState:UIControlStateNormal];
    [self.btnRight setTitle:@"再次点名" forState:UIControlStateNormal];
}
- (NSMutableArray <CallStudentModel *> *)collectionViewStudentsData
{
    if (!_collectionViewStudentsData) {
        _collectionViewStudentsData = [NSMutableArray array];
    }
    return _collectionViewStudentsData;
}


- (IBAction)closeAction:(UIButton *)sender {
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
}

- (void)startAction{
    [self removeCoverView];
//    [self showLoadAnimation];
    self.currentMode = self.callTypeArr.firstObject;
    [self initView];
    [self sendSocketData];
}


- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":self.currentMode.callModeType} commandCode:@(RollCall) successBlock:^(id result) {
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
