//
//  CallStudentModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CallStudentModel : NSObject<NSCopying, NSMutableCopying>

@property (nonatomic, copy) NSString *cardnum;
@property (nonatomic, copy) NSString *studentName;
@property (nonatomic, copy) NSString *studentId;

@property (nonatomic, copy) NSString *parentName;
@property (nonatomic, copy) NSString *parentTel;


@property (nonatomic, assign) BOOL isRaiseHand;

+ (instancetype)initWithStudentId:(NSString *)studentI name:(NSString *)name;


@end
