//
//  CallTypeModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CallTypeModel : NSObject

@property (nonatomic, copy) NSString *callModelName;
@property (nonatomic, copy) NSString *callModeType;

+ (CallTypeModel *)modelWithName:(NSString *)name type:(NSString *)type;



@end
