//
//  HDCloseVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDCloseVC.h"
#import "HDWifiView.h"

@interface HDCloseVC ()

@property (strong, nonatomic) HDWifiView *wifiView;

@end

@implementation HDCloseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.hd_navigationBar.hidden = YES;
    [self guideWifiView];
}

- (void)guideWifiView
{
    _wifiView = [[HDWifiView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _wifiView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_wifiView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
