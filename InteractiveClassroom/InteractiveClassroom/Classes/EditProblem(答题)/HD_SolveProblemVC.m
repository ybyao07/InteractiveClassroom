//
//  SolveProblemVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_SolveProblemVC.h"
#import "HDSubjectVC.h"

@interface HD_SolveProblemVC ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property(strong, nonatomic) NSMutableArray *pages;
@property(strong, nonatomic) UIPageViewController *pageController;

@property (weak, nonatomic) IBOutlet UIButton *btnLeftMine;
@property (weak, nonatomic) IBOutlet UIButton *btnRightRecommand;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation HD_SolveProblemVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    self.btnLeftMine.selected = YES;
    self.lblTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"chapterName"];
    [self setPageViewController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
//    [self showCoverView];
}

- (void)setPageViewController {
    [self addOrderPage];
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setDelegate:self];
    self.pageController.view.bounds = CGRectMake(0, 0, self.view.frame.size.width - 2*60, SCREEN_HEIGHT - 70 - 450);
    self.pageController.view.center = CGPointMake(self.view.center.x, self.view.center.y - 50);
    NSArray *viewControllers = [NSArray arrayWithObject:[self.pages objectAtIndex:0]];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (void)addOrderPage {
    _pages = [NSMutableArray array];
    for (NSInteger i = 0; i < 2; i++) {
        HDSubjectVC *page = [[HDSubjectVC alloc] init];
        switch (i) {
            case 0:
                page.pageType = SubjectType_Mine;
                break;
            case 1:
                page.pageType = SubjectType_Recommend;
                break;
            default:
                break;
        }
        [_pages addObject:page];
    }
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_AnswerQuestion"]) {
        if (open == 1) {
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}

#pragma mark - UIPageViewControllerDataSource & UIPageViewControllerDelegate
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger currentIndex = [self.pages indexOfObject:viewController];    // get the index of the current view controller on display
    // check if we are at the end and decide if we need to present the next viewcontroller
    if (currentIndex < [self.pages count] - 1) {
        return [self.pages objectAtIndex:currentIndex + 1];                   // return the next view controlle
    } else {
        return nil;                                                         // do nothing
    }
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger currentIndex = [self.pages indexOfObject:viewController];    // get the index of the current view controller on display
    
    // check if we are at the beginning and decide if we need to present the previous viewcontroller
    if (currentIndex > 0) {
        return [self.pages objectAtIndex:currentIndex - 1];                   // return the previous viewcontroller
    } else {
        return nil;                                                         // do nothing
    }
}


- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        NSUInteger currentIndex = [self.pages indexOfObject:[pageViewController.viewControllers lastObject]];
        if (currentIndex==0) {
            self.btnLeftMine.selected = YES;
//            self.btnRightRecommand.selected = NO;
        }else{
            self.btnLeftMine.selected = NO;
//            self.btnRightRecommand.selected = YES;
        }
    }
}



#pragma mark ========= 按钮点击 ========
- (IBAction)closeAction:(UIButton *)sender {
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        
    }];
}
- (void)startAction{
    [self removeCoverView];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTest) successBlock:^(id result) {
    }];
}

- (IBAction)didClickedButton:(UIButton *)btn {
    if (btn.tag==0) {
        self.btnLeftMine.selected = YES;
//        self.btnRightRecommand.selected = NO;
    }else{
        self.btnLeftMine.selected = NO;
//        self.btnRightRecommand.selected = YES;
    }
    
    NSArray *toViewController = [NSArray arrayWithObject:[self.pages objectAtIndex:btn.tag]];
    [_pageController setViewControllers:toViewController direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
