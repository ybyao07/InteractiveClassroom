//
//  HDSubjectMainViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSubjectMainViewController.h"
#import "SubjectOneView.h"
#import "SubjectResultView.h"
#import "SubjectResultAnalysize.h"
#import "UIButton+ImageTitleCentering.h"
#import "SubjectSchedualModel.h"
#import "PageMangerCenter.h"
typedef enum
{
    SubjectTag_Zero = 0,
    SubjectTag_One =  1,
    SubjectTag_Two = 2,
    SubjectTag_Three = 3,
    SubjectTag_Four = 4,
    SubjectTag_Five = 5,
}SubjectTag;




@interface HDSubjectMainViewController ()<OperationChangeSubjectDelegate,AnalizeViewDelegate,ResultViewDelegate>
{
    NSString *_classTestName;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
//@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoAuto;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeLook;
@property (weak, nonatomic) IBOutlet UIButton *btnFourResult;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveAnalize;
@property (weak, nonatomic) IBOutlet UIButton *btnSixProgress;
@property (weak, nonatomic) IBOutlet UIButton *btnSevenExit;


@property (strong, nonatomic) SubjectOneView *oneView;

@property (strong, nonatomic) SubjectResultView *analizeView;//答题分析
@property (strong, nonatomic) SubjectResultAnalysize *resultView;// 答题结果


@property (strong, nonatomic) NSMutableArray <SubjectSchedualModel *> *resources;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;



@end

@implementation HDSubjectMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
//    [self.btnOne centerVerticallyWithPadding:0];
    [self.btnTwoAuto centerVerticallyWithPadding:0];
    [self.btnThreeLook centerVerticallyWithPadding:0];
    [self.btnFourResult centerVerticallyWithPadding:0];
    [self.btnFiveAnalize centerVerticallyWithPadding:0];
    [self.btnSixProgress centerVerticallyWithPadding:0];
    [self.btnSevenExit centerVerticallyWithPadding:0];
    
    self.oneView = [SubjectOneView instanceSubjectView];
    [self.oneView.btnClose addTarget:self action:@selector(closeLookView) forControlEvents:UIControlEventTouchUpInside];
    self.resultView = [SubjectResultAnalysize instanceView];
    self.analizeView = [SubjectResultView instanceResultView];
    self.btnSixProgress.enabled = NO;
    
    
//    [self addTestData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unSelectedButton) name:@"NotificationThrowAction" object:nil];

}


- (void)addTestData
{
    NSDictionary *dic = @{
                          @"ClassSchedulesList":
                                @[
                                  @{
                                      @"problemHtml":@"1、-2017的相反数是（   ）. <br/><br/>A. 2017<br/><br/>B. -2017<br/><br/>C. - <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_2_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>D. <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_3_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>",
                                      @"answerHtml":@"<font color='red'>【答案】A</font><br/><br/>【知识点】相反数<br/><br/><span class=\"exam - analytic\">【解析】</span> <div class=\"analyticbox - body\"> 【解答】-2017的相反数是2017.<br>故选A.<br>【分析】负数的相反数是正数，且绝对值相等.    </div><br/><br/>",
                                      },
                                  @{
                                      @"problemHtml":@"1、-2016的相反数是（   ）. <br/><br/>A. 2016<br/><br/>B. -2016<br/><br/>C. - <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_2_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>D. <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_3_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>",
                                      @"answerHtml":@"<font color='red'>【答案】A</font><br/><br/>【知识点】相反数<br/><br/><span class=\"exam - analytic\">【解析】</span> <div class=\"analyticbox - body\"> 【解答】-2016的相反数是2016.<br>故选A.<br>【分析】负数的相反数是正数，且绝对值相等.    </div><br/><br/>",
                                      },
                                  @{
                                      @"problemHtml":@"1、-2015的相反数是（   ）. <br/><br/>A. 2015<br/><br/>B. -2015<br/><br/>C. - <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_2_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>D. <img class=\"mathml\" src=\"http://img.smartpm.cn/s202/3705533_options_3_0.png\" style=\"vertical-align: middle;\" draggable=\"false\"><br/><br/>",
                                      @"answerHtml":@"<font color='red'>【答案】A</font><br/><br/>【知识点】相反数<br/><br/><span class=\"exam - analytic\">【解析】</span> <div class=\"analyticbox - body\"> 【解答】-2015的相反数是2015.<br>故选A.<br>【分析】负数的相反数是正数，且绝对值相等.    </div><br/><br/>",
                                      }
                                  ]
                          };
    [self.resources removeAllObjects];
    NSArray *array = [SubjectSchedualModel mj_objectArrayWithKeyValuesArray:dic[@"ClassSchedulesList"]];
    [self.resources addObjectsFromArray:array];
    self.oneView.resources = self.resources;
    [self.containerView addSubview:self.oneView];
    [PageMangerCenter shareInstance].kIndexMax = (int)self.resources.count;
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == ClassroomTestInfo) { // 返回题目信息
        [self.resources removeAllObjects];
        NSArray *array = [SubjectSchedualModel mj_objectArrayWithKeyValuesArray:dic[@"ClassSchedulesList"]];
        [self.resources addObjectsFromArray:array];
        self.oneView.resources = self.resources;
        self.oneView.delegate = self;
        self.oneView.btnThrow.hidden = YES;
        self.oneView.btnClose.hidden = YES;
        _classTestName = dic[@"classroomTestName"];
        self.oneView.lblTitle.text = _classTestName;
//        self.resultView.lblTitle.text = _classTestName;
//        self.resultAnalysizeView.lblTitle.text = _classTestName;
        [self.containerView addSubview:self.oneView];
        [PageMangerCenter shareInstance].kIndexMax = (int)self.resources.count;
        [self removeLoadAnimation];
    }else if ([dic[@"code"] integerValue] == SwitchNextQuestion){ //返回切换下一题
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].currentIndex + 1;
         SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
        [self.oneView loadHtml:model.problemHtml];
        self.oneView.preBtn.enabled = YES;
        if ([PageMangerCenter shareInstance].currentIndex == self.resources.count-1) {
            self.oneView.nextBtn.enabled = NO;
        }else{
            self.oneView.nextBtn.enabled = YES;
        }
    }
}

- (void)unSelectedButton
{
    self.btnTwoAuto.selected = NO;
}

#pragma mark ==============
- (IBAction)changeItem:(UIButton *)btn
{
    btn.selected = !btn.selected;
    switch (btn.tag) {
        case SubjectTag_Zero: // 开始答题
        {
            if (btn.selected) {
                self.btnSixProgress.enabled = YES;
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
            }else{
                self.btnSixProgress.enabled = NO;
            }
            self.oneView.btnThrow.hidden = YES;
            self.oneView.btnClose.hidden = YES;
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationStartAnswer);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
            }else{
                dic[@"status"] = @(CloseOperation);
            }
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        case SubjectTag_One:  // 自动切换
        {
            if (btn.selected) {
                self.btnThreeLook.selected = NO;
                self.btnSixProgress.enabled = YES;
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
            }else{
                self.btnSixProgress.enabled = NO;
            }
            self.oneView.btnThrow.hidden = YES;
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAutoChange);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
            }else{
                dic[@"status"] = @(CloseOperation);
            }
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        case SubjectTag_Two:  //查看解析
        {
            if (btn.selected) {
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                self.oneView.btnThrow.hidden = NO;
                self.oneView.btnClose.hidden = NO;
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.answerHtml];
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                dic[@"operation"] = @(OperationAnalysis);
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                self.oneView.btnThrow.hidden = YES;
                self.oneView.btnClose.hidden = YES;
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                dic[@"operation"] = @(OperationAnalysis);
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
        }
            break;
        case SubjectTag_Three: //答题结果
        {
            if (!btn.selected) {
                [self.analizeView removeFromSuperview];
            }else{
                [self closeResultView];
                _analizeView.delegate = self;
                _analizeView.lblTitle.text = _classTestName;
                self.btnThreeLook.selected = NO;
                [self.containerView addSubview:_analizeView];
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerAnalysis);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
            if (!btn.selected) {
                [self.analizeView removeFromSuperview];
            }
        }
            break;
        case SubjectTag_Four:  //答题分析
        {
            if (!btn.selected) {
                [self.resultView removeFromSuperview];
            }else{
                [self closeAnalizeView];
                _resultView.delegate = self;
                _resultView.lblTitle.text = _classTestName;
                self.btnThreeLook.selected = NO;
                [self.containerView addSubview:_resultView];
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerResult);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
            if (!btn.selected) {
                [self.resultView removeFromSuperview];
            }
        }
            break;
        case SubjectTag_Five: //答题进度
        {
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerProgress);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
            }else{
                dic[@"status"] = @(CloseOperation);
            }
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        default:
            break;
    }
}


- (void)closeLookView
{
    self.btnThreeLook.selected = NO;
    self.oneView.btnThrow.hidden = YES;
    self.oneView.btnClose.hidden = YES;
    self.btnTwoAuto.selected = NO;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    [self.oneView loadHtml:model.problemHtml]; 
}
#pragma mark ======= OperationChangeSubjectDelegate ====
- (void)changeSubject
{
    self.btnThreeLook.selected = NO;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    if (model.problemAnswer == nil || model.problemAnswer.length == 0 || [model.problemAnswer isEqualToString:@"\"\""] || [model.problemAnswer isEqualToString:@"{}"]) {
//        self.btnOne.enabled = NO;
        self.btnTwoAuto.enabled = NO;
        self.btnFourResult.enabled = NO;
        self.btnFiveAnalize.enabled = NO;
    }else{
//        self.btnOne.enabled = YES;
        self.btnTwoAuto.enabled = YES;
        self.btnFourResult.enabled = YES;
        self.btnFiveAnalize.enabled = YES;
    }
}

- (void)closeAnalizeView
{
    [self.analizeView removeFromSuperview];
    self.btnFourResult.selected = NO;
}
- (void)closeResultView
{
    [self.resultView removeFromSuperview];
    self.btnFiveAnalize.selected = NO;
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
