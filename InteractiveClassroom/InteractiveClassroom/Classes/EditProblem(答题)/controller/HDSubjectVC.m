//
//  HDSubjectVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSubjectVC.h"
#import "SubjectCollectionCell.h"
#import "HDSubjectMainViewController.h"
#import "SubjectClassModel.h"

#define Subject_CELL_WIDTH (SCREEN_WIDTH - 60*2 - 10*3)/4

@interface HDSubjectVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *myCollectionView;

@property (strong, nonatomic) NSMutableArray *resources;


@end

static CGFloat cellHeight = 0.f;
static CGFloat cellWidth = 0.f;
@implementation HDSubjectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的题目单";
    self.view.backgroundColor = [UIColor clearColor];
    [self removeBackgourdImageView];
    [self.hd_navigationBar removeFromSuperview];
    cellHeight = (SCREEN_HEIGHT - 70 - 150 - 80)/3;
    cellWidth = (self.view.bounds.size.height - 10*3)/4;
    [self.view addSubview:self.myCollectionView];
//    [self addTestData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"code":@"0",
                          @"data":
                                  @[
                                      @{
                                          @"subjectId":@"11",
                                          @"classTestName":@"Test 1",
                                          @"problemCount":@(10),
                                          @"isTeach":@(YES),
                                          @"sort":@"1"
                                          },
                                      @{
                                          @"subjectId":@"12",
                                          @"classTestName":@"Test 2",
                                          @"problemCount":@(1),
                                          @"isTeach":@(NO),
                                          @"sort":@"2"
                                          }
                                      ]                                  
                          };
    [self.resources removeAllObjects];
    NSArray *array = [SubjectClassModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.myCollectionView reloadData];
}



- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == ClassroomTestList) {
            [self.resources removeAllObjects];
            NSArray *array = [SubjectClassModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.resources addObjectsFromArray:array];
            [self.myCollectionView reloadData];
    }
}

- (UICollectionView *)myCollectionView
{
    if (!_myCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _myCollectionView = [[UICollectionView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH - 60*2, self.view.bounds.size.height) collectionViewLayout:layout];
        _myCollectionView.backgroundColor = [UIColor clearColor];
        _myCollectionView.delegate = self;
        _myCollectionView.dataSource = self;
        [_myCollectionView registerNib:[UINib nibWithNibName:@"SubjectCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SubjectCollectionCell"];
    }
    return _myCollectionView;
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubjectCollectionCell" forIndexPath:indexPath];
    cell.subModel = self.resources[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, cellHeight);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectClassModel *model = self.resources[indexPath.row];
    [self sendSocketData:model];
}

- (void)sendSocketData:(SubjectClassModel *)model;
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"testId"] = model.subjectId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(OpenClassroomTest) successBlock:^(id result) {
    }];
    HDSubjectMainViewController *vc = [HDSubjectMainViewController new];
    [[JUITools currentRootController] pushViewController:vc animated:YES];
}


#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
