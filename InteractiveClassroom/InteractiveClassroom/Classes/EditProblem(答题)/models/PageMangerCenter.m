//
//  PageMangerCenter.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/25.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PageMangerCenter.h"

@interface PageMangerCenter ()

@end
@implementation PageMangerCenter

- (id)init
{
    if (self = [super init]) {
        self.kIndexMin = 0;
        self.kIndexMax = 0;
        self.currentIndex = 0;
    }
    return self;
}

+(instancetype) shareInstance
{
    static PageMangerCenter *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PageMangerCenter alloc] init];
    });
    return instance;
}


@end
