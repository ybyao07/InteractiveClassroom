//
//  StudentOptionModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/25.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface StudentOptionModel : NSObject

@property (nonatomic, assign) BOOL isRight;
@property (nonatomic, copy) NSString *option;
@property (nonatomic, strong) NSString *rate;
@property (nonatomic, assign) NSNumber *studentCount;
//@property (nonatomic, copy) NSString *studentName;

@end
