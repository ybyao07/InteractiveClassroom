//
//  SubjectAnswerResultModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubjectAnswerResultModel : NSObject

@property (nonatomic, strong) NSNumber *answerResult;
@property (nonatomic, copy) NSString *studentName;

@end
