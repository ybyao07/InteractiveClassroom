//
//  SubjectSchedualModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubjectSchedualModel : NSObject

//@property (nonatomic, copy) NSString *ProblemAnswer;
//@property (nonatomic, strong) NSArray  *ProblemAnswerList;
//@property (nonatomic, assign) BOOL Selected;
//@property (nonatomic, strong) NSString *analyses;
//@property (nonatomic, strong) NSString *answer;
@property (nonatomic, strong) NSString *answerHtml;

//@property (nonatomic, strong) NSString *knowledgeName;//知识点
@property (nonatomic, strong) NSString *classTestProblemId; // 客户端暂时不用

@property (nonatomic, strong) NSString *problemAnswer;
//@property (nonatomic, strong) NSString *problemContent;
//@property (nonatomic, strong) NSNumber *problemCount;
@property (nonatomic, strong) NSString *problemHtml;
//@property (nonatomic, strong) NSString *problemId;



@end
