//
//  SubjectOneView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectOneView.h"
#import "PageMangerCenter.h"
#import "HDSubjectMainViewController.h"

@interface SubjectOneView ()


@property (weak, nonatomic) IBOutlet UIView *containView;



@end


@implementation SubjectOneView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0,0, SCREEN_WIDTH - 60, SCREEN_HEIGHT - 30 - 140);
    self.wkView = [[WKWebView alloc] initWithFrame:Rect(0, 20, SCREEN_WIDTH-100, SCREEN_HEIGHT - 350)];
//    self.wkView.backgroundColor = [UIColor yellowColor];
    self.wkView.UIDelegate = self;
    [self.containView addSubview:self.wkView];
    _preBtn.enabled = NO;
}

+(SubjectOneView *)instanceSubjectView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"SubjectOneView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}
- (void)loadHtml:(NSString *)html
{
    if (html != nil) {
        [_wkView loadHTMLString:html baseURL:nil];
    }
}


- (IBAction)nextAction:(UIButton *)sender {
    int pageIndex = [PageMangerCenter shareInstance].currentIndex;
    [PageMangerCenter shareInstance].currentIndex = pageIndex + 1;
    if ([PageMangerCenter shareInstance].currentIndex > [PageMangerCenter shareInstance].kIndexMax) {
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].kIndexMax;
        return;
    }
    self.btnThrow.hidden = YES;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    [self loadHtml:model.problemHtml];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationNextProblems);
    dic[@"status"] = @(OpenOperation);
    self.preBtn.enabled = YES;
    if ([PageMangerCenter shareInstance].currentIndex == self.resources.count-1) {
        self.nextBtn.enabled = NO;
    }else{
        self.nextBtn.enabled = YES;
    }
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(changeSubject)]) {
        [self.delegate changeSubject];
    }
}

- (IBAction)preAction:(UIButton *)sender {
    int pageIndex = [PageMangerCenter shareInstance].currentIndex;
    
    [PageMangerCenter shareInstance].currentIndex = pageIndex - 1;
    if ([PageMangerCenter shareInstance].currentIndex < [PageMangerCenter shareInstance].kIndexMin) {
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].kIndexMin;
        return;
    }
    self.btnThrow.hidden = YES;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    [self loadHtml:model.problemHtml];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationLastProblems);
    dic[@"status"] = @(OpenOperation);
    self.nextBtn.enabled = YES;
    if ([PageMangerCenter shareInstance].currentIndex == 0) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(changeSubject)]) {
        [self.delegate changeSubject];
    }
}


- (void)setResources:(NSMutableArray<SubjectSchedualModel *> *)resources
{
    _resources = resources;
    SubjectSchedualModel *model = resources.firstObject;
    if (resources.count <= 1) {
        self.nextBtn.enabled = NO;
    }
    [self loadHtml:model.problemHtml];
}



- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    //    NSString *targetStr = navigationAction.request.URL.absoluteString;
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (IBAction)showAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationThrowAction" object:nil];
}



@end
