//
//  SubjectResultAnalysize.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ResultViewDelegate <NSObject>

- (void)closeResultView;

@end

@interface SubjectResultAnalysize : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) id<ResultViewDelegate> delegate;

+(SubjectResultAnalysize *)instanceView;

@end
