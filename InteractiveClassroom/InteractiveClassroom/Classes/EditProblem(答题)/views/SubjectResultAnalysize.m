//
//  SubjectResultAnalysize.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectResultAnalysize.h"
#import "SubjectAnswerResultModel.h"
#import "TestStudentProgressCell.h"
#import "AnswerStudentList.h"
#import "HDSubjectMainViewController.h"


#define Subject_CELL_WIDTH (SCREEN_WIDTH - 30*2 - 20*2 - 2*50 - 20*5)/6
@interface SubjectResultAnalysize ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSMutableArray *resources;

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;

@end

@implementation SubjectResultAnalysize

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0,0, SCREEN_WIDTH - 60, SCREEN_HEIGHT - 30 - 20 - 100 - 20);
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
//    [self addTestData];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"OptionList":@[
                                  @{
                                      @"studentName":@"周芷若",
                                      @"rate":@"0.4",
                                      @"option":@"A",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"张三",
                                      @"rate":@"0.4",
                                      @"option":@"B",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"王五",
                                      @"rate":@"0.4",
                                      @"option":@"A",
                                      @"isRight":@(NO),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"周芷若",
                                      @"rate":@"0.4",
                                      @"option":@"A",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"张三",
                                      @"rate":@"0.4",
                                      @"option":@"B",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"王五",
                                      @"rate":@"0.4",
                                      @"option":@"A",
                                      @"isRight":@(NO),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"周芷若",
                                      @"rate":@"0.4",
                                      @"option":@"",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"张三",
                                      @"rate":@"0.4",
                                      @"option":@"B",
                                      @"isRight":@(YES),
                                      @"studentCount":@(100)
                                      },
                                  @{
                                      @"studentName":@"王五",
                                      @"rate":@"0.4",
                                      @"option":@"A",
                                      @"isRight":@(NO),
                                      @"studentCount":@(100)
                                      }
                                  ]
                          };
    [self.resources removeAllObjects];
    NSArray *array = [SubjectAnswerResultModel mj_objectArrayWithKeyValuesArray:dic[@"OptionList"]];
    [self.resources addObjectsFromArray:array];
    [self.myCollectionView reloadData];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == AnswerResult) {
        [self.resources removeAllObjects];
        NSArray *array = [AnswerStudentList mj_objectArrayWithKeyValuesArray:dic[@"multipleAnswerList"]];
        [array enumerateObjectsUsingBlock:^(AnswerStudentList  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.studentAnswerList enumerateObjectsUsingBlock:^(SubjectAnswerResultModel * _Nonnull studentM, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.resources addObject:studentM];
            }];
        }];
        [self.myCollectionView reloadData];
    }
}

+(SubjectResultAnalysize *)instanceView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"SubjectResultAnalysize" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    SubjectAnswerResultModel *model = self.resources[indexPath.row];
    if ([model.answerResult intValue] == 2) { //正确
        cell.backgroundColor = JUIColorFromRGB_Major2;
    }else if([model.answerResult intValue] == 3){ //错误
        cell.backgroundColor = JUIColorFromRGB_Major3;
    }else{
        cell.backgroundColor = JUIColorFromRGB_Gray2;
    }
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (IBAction)refreshAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
    }];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(OperationAnswerResult),@"status":@(CloseOperation)} commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(closeResultView)]) {
        [self.delegate closeResultView];
    }
}

- (IBAction)throwAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationThrowAction" object:nil];
}


#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
