//
//  SubjectResultView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol AnalizeViewDelegate <NSObject>

- (void)closeAnalizeView;

@end
@interface SubjectResultView : UIView

@property (weak, nonatomic) IBOutlet UIView *containView;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

+(SubjectResultView *)instanceResultView;
@property (nonatomic, weak) id<AnalizeViewDelegate> delegate;


@end
