//
//  SubjectResultView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectResultView.h"
#import "PNBarChart.h"
#import "YBBarCharVerticalView.h"
#import "StudentOptionModel.h"
#import "HDSubjectMainViewController.h"
@interface SubjectResultView ()

@property (strong, nonatomic) NSMutableArray *resources;
@property (strong, nonatomic) YBBarCharVerticalView * barChart;
@end
@implementation SubjectResultView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0, 0, SCREEN_WIDTH - 60, SCREEN_HEIGHT - 140 - 30);
    
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    
    

}
- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == AnswerAnalyses) {
        [self.resources removeAllObjects];
        NSArray *array = [StudentOptionModel mj_objectArrayWithKeyValuesArray:dic[@"OptionList"]];
        [self.resources addObjectsFromArray:array];
        [_barChart removeFromSuperview];
        NSMutableArray *xlabels = [NSMutableArray arrayWithCapacity:self.resources.count];
        NSMutableArray *yValues = [NSMutableArray arrayWithCapacity:self.resources.count];
        NSMutableArray *colors = [NSMutableArray arrayWithCapacity:self.resources.count];
        __block int index = 0;
        __block int maxValue = 0;
        __block int noValue = 0;
        [self.resources enumerateObjectsUsingBlock:^(StudentOptionModel * _Nonnull starM, NSUInteger idx, BOOL * _Nonnull stop) {
            [xlabels addObject:[NSString stringWithFormat:@"%@",starM.option]];
            [yValues addObject:[NSString stringWithFormat:@"%d人",(int)[starM.studentCount integerValue]]];
            [colors addObject:JUIColorFromRGB_Main3];
            if ([starM.option isEqualToString:@"X"]||[starM.option isEqualToString:@"x"]) {
                noValue = [starM.studentCount intValue];
                [colors replaceObjectAtIndex:idx withObject:JUIColorFromRGB_Gray2];//灰色
                [xlabels replaceObjectAtIndex:idx withObject:@"未作答/无效"];
            }else{
                if ([starM.studentCount intValue]  > maxValue) {
                    maxValue = [starM.studentCount intValue];
                    index = (int)idx;
                }
            }
        }];
        [colors replaceObjectAtIndex:index withObject:JUIColorFromRGB_Major1];
        CGRect frame = CGRectMake(200, 70.0, SCREEN_WIDTH-400, SCREEN_HEIGHT - 300);
        CGFloat startX = 0;
        startX = ((SCREEN_WIDTH - 400) - self.resources.count*40 - (self.resources.count - 1)*40 - 90)/2.0;// -90未知
        CGPoint startPoint = CGPointMake(startX, 0);
        _barChart = [[YBBarCharVerticalView alloc] initWithFrame:frame
                                                                                startPoint:startPoint
                                                                                    values:yValues
                                                                                    colors:colors
                                                                                  maxValue:maxValue > noValue?maxValue:noValue
                                                                            textIndicators:xlabels
                                                                                 textColor:[UIColor blackColor]
                                                                                  barWidth:40
                                                                              barMaxHeight:SCREEN_HEIGHT-470];
        
        
        [self.containView addSubview:_barChart];
    }
}

+(SubjectResultView *)instanceResultView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"SubjectResultView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(OperationAnswerAnalysis),@"status":@(CloseOperation)} commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(closeAnalizeView)]) {
        [self.delegate closeAnalizeView];
    }
}
- (IBAction)refreshAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerResult);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
    }];
}
- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(0)} commandCode:@(Praise) successBlock:^(id result) {
    }];
}

- (IBAction)throwAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerResult);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationThrowAction" object:nil];
}


#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
