//
//  HD_OfflineTestVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_OfflineTestVC.h"
#import "SubjectCollectionCell.h"
#import "TestJudgeVC.h"
#import "TestModel.h"
#import "HDSocketSendCommandTool.h"

#define Subject_CELL_WIDTH (SCREEN_WIDTH - 60*2 - 10*3)/4

@interface HD_OfflineTestVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *resources;

@end

static CGFloat cellHeight = 0.f;

@implementation HD_OfflineTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SubjectCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SubjectCollectionCell"];
    cellHeight = (SCREEN_HEIGHT - 70 - 150 - 80)/3;
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction:) name:@"CloseOfflineTest" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
//    [self addTestData];
    [self showCoverView];
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_Test"]) {
        if (open == 1) {
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}


- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"data"] = @[@{
                               @"cardId":@"",
                               @"cardName":@"单选多选判读数字",
                               @"problemNum":@(9),
                               @"Selected":@(0),
                               @"sort":@"1",
                               },
                           @{
                               @"cardId":@"",
                               @"cardName":@"试卷登分",
                               @"problemNum":@(6),
                               @"Selected":@(1),
                               @"sort":@"2",
                               },
                           @{
                               @"cardId":@"",
                               @"cardName":@"试卷登分",
                               @"problemNum":@(1),
                               @"Selected":@(1),
                               @"sort":@"3",
                               }];
    
    [self.resources removeAllObjects];
    NSArray *array = [TestModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.collectionView reloadData];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperList) {
        [self.resources removeAllObjects];
        NSArray *array = [TestModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.resources addObjectsFromArray:array];
        [self.collectionView reloadData];
    }
}
#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubjectCollectionCell" forIndexPath:indexPath];
    cell.model = self.resources[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, cellHeight);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestJudgeVC *jdugeVc = [TestJudgeVC new];
    TestModel *model = self.resources[indexPath.row];
    jdugeVc.model = model;
    [self.navigationController pushViewController:jdugeVc animated:YES];
}

- (IBAction)startTest:(UIButton *)sender {
    UIAlertView *alertV = [[UIAlertView alloc] initWithTitle:@"" message:@"点击对应题目自动进入测评" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertV show];
}

- (IBAction)closeAction:(UIButton *)sender {
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
}
- (void)startAction{
    [self removeCoverView];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(TestPaper) successBlock:^(id result) {
    }];
}



#pragma mark ===== accessory =====
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
