//
//  TestJudgeVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/5.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "TestModel.h"
#import "TimeLabel.h"
@interface TestJudgeVC : YBaseViewController

@property (nonatomic, strong) TestModel *model;

@end
