//
//  TestJudgeVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/5.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TestJudgeVC.h"
#import "TestResultVC.h"
#import "HDProgress.h"
#import "TestProgressVC.h"

@interface TestJudgeVC ()
{
    HDProgress *progress;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeTitle;
@property (weak, nonatomic) IBOutlet TimeLabel *lblTimeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblProgressTitle;


@end

@implementation TestJudgeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    self.lblTitle.text = _model.cardName;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
//    [self.lblTimeCount startTimer:0];
    progress = [[HDProgress alloc]initWithFrame:Rect(100, CGRectGetMaxY(_lblProgressTitle.frame) + 40, SCREEN_WIDTH-200-40, 40)];
    [self.containerView addSubview:progress];
//    float value = (float)0/30;
//    progress.progressLableValue=[NSString stringWithFormat:@"已完成%.0f%%",value*100];
//    progress.progressValue = [NSString stringWithFormat:@"%f",value];
}


- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperProgress) {
        int finishCount = (int)[dic[@"finishCount"] integerValue];
        int totalCount = (int)[dic[@"totalCount"] integerValue];
        _lblProgressTitle.text = [NSString stringWithFormat:@"测评进度：已完成%d人/未完成%d人",finishCount,totalCount-finishCount];
        float value = (float)finishCount/totalCount;
        progress.progressLableValue=[NSString stringWithFormat:@"已完成%.1f%%",value*100];
        progress.progressValue = [NSString stringWithFormat:@"%f",value] ;
        self.lblTimeCount.text = dic[@"time"];
    }
}

- (void)sendSocketData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"cardId"] = self.model.cardId;    
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartTestPaper) successBlock:^(id result) {
    }];
}

#pragma mark ========= 按钮点击 ========
- (IBAction)didClickedButton:(UIButton *)btn {
    if (btn.tag == 0) { // 进度详情
        TestProgressVC *progressVC = [TestProgressVC new];
        progressVC.testM = self.model;
        [self.navigationController pushViewController:progressVC animated:YES];
    }
    if (btn.tag == 1) { // 刷新进度
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(RefreshTestPaperProgress) successBlock:^(id result) {
            
        }];
    }
    if (btn.tag == 2) {
        TestResultVC *resultVC = [TestResultVC new];
        [self.navigationController pushViewController:resultVC animated:YES];
    }
    
}
- (IBAction)close:(UIButton *)sender {
    [self.lblTimeCount endTimer];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
