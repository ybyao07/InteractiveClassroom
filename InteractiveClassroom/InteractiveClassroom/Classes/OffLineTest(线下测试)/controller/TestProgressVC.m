//
//  TestProgressVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TestProgressVC.h"
#import "TestStudentModel.h"
#import "TestStudentProgressCell.h"
#define Subject_CELL_WIDTH (SCREEN_WIDTH - 60*2 - 10*6)/7

@interface TestProgressVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSMutableArray *resources;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation TestProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;

    self.lblTitle.text = self.testM.cardName;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
//    [self addTestData];
}
- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"data"] = @[@{
                         @"cardId":@"",
                         @"sutdentName":@"张三",
                         @"TestState":@(1),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"李四",
                         @"TestState":@(2),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"王五",
                         @"TestState":@(1),
                         },@{
                         @"cardId":@"",
                         @"sutdentName":@"张三",
                         @"TestState":@(1),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"李四",
                         @"TestState":@(2),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"王五",
                         @"TestState":@(1),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"张三",
                         @"TestState":@(1),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"李四",
                         @"TestState":@(2),
                         },
                     @{
                         @"cardId":@"",
                         @"sutdentName":@"王五",
                         @"TestState":@(1),
                         }];
    
    [self.resources removeAllObjects];
    NSArray *array = [TestStudentModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.collectionView reloadData];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperDetailsList) {
        [self.resources removeAllObjects];
        NSArray *array = [TestStudentModel mj_objectArrayWithKeyValuesArray:dic[@"Students"]];
        [self.resources addObjectsFromArray:array];
        [self.collectionView reloadData];
    }
}

- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(TestPaperDetails) successBlock:^(id result) {
        
    }];
}

- (IBAction)backAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseTestPaperDetails) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    TestStudentModel *model = self.resources[indexPath.row];
    if ([model.TestState integerValue] == 2) {
        cell.backgroundColor = JUIColorFromRGB_Major1;
    }else{
        cell.backgroundColor = JUIColorFromRGB_Gray2;
    }
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
#pragma mark ===== accessory =====
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
