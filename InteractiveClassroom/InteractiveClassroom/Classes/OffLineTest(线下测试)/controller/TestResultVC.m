//
//  TestResultVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/5.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TestResultVC.h"

@interface TestResultVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblFinish;
@property (weak, nonatomic) IBOutlet UILabel *lblUnfinish;

@end

@implementation TestResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperResult) {
        NSString *time = dic[@"time"];
        NSString *finishCount = dic[@"finishCount"];
        NSString *unfinishCount = dic[@"unfinishCount"];
        _lblTime.text = time;
        _lblFinish.text = finishCount;
        _lblUnfinish.text = unfinishCount;
    }
}

- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(EndTestPaper) successBlock:^(id result) {
    }];
}
- (IBAction)continueTest:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(ContinueTestPaper) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeSaveAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(SaveTestPaper) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)closeAction:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseOfflineTest" object:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
