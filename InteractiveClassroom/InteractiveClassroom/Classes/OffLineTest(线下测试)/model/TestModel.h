//
//  TestModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestModel : NSObject

@property (nonatomic, copy) NSString *cardId;
@property (nonatomic, copy) NSString *cardName;//标题
@property (nonatomic, assign) NSNumber *problemNum; //题目数量

@property (nonatomic, assign) BOOL Selected; // 是否已做
@property (nonatomic, copy) NSString *sort; //序号


@end
