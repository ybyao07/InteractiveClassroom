//
//  HDProgress.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDProgress.h"

@implementation HDProgress
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self buildUI];
    }
    return self;
}
- (void)buildUI
{
    viewBottom = [[UIView alloc]initWithFrame:self.bounds];
    viewBottom.backgroundColor =[UIColor colorWithWhite:0 alpha:0.9];
    viewBottom.layer.cornerRadius = 20;
    viewBottom.layer.masksToBounds = YES;
    [self addSubview:viewBottom];
    
    viewTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, viewBottom.frame.size.height)];
    viewTop.backgroundColor = JUIColorFromRGB_Main3;
    viewTop.layer.cornerRadius = 20;
    viewTop.layer.masksToBounds = YES;
    [viewBottom addSubview:viewTop];
    
    progressLb=[[UILabel alloc]initWithFrame:self.bounds];
    progressLb.font=kFont18;
    progressLb.textColor=[UIColor whiteColor];
    progressLb.textAlignment=NSTextAlignmentCenter;
    [viewBottom addSubview:progressLb];
}

-(void)setTime:(float)time
{
    _time = time;
}

-(void)setProgressValue:(NSString *)progressValue
{
    if (!_time) {
        _time = 0.5f;
    }
    _progressValue = progressValue;
    [UIView animateWithDuration:_time animations:^{
        viewTop.frame = CGRectMake(viewTop.frame.origin.x, viewTop.frame.origin.y, viewBottom.frame.size.width*[progressValue floatValue], viewTop.frame.size.height);
//    progressLb.frame = CGRectMake(viewBottom.frame.size.width*[progressValue floatValue]-140, progressLb.frame.origin.y,120, progressLb.frame.size.height);
    }];
}

- (void)setProgressLableValue:(NSString *)progressLableValue
{
    _progressLableValue=progressLableValue;
    progressLb.text=_progressLableValue;
}

-(void)setBottomColor:(UIColor *)bottomColor
{
    _bottomColor = bottomColor;
    viewBottom.backgroundColor = bottomColor;
}

-(void)setProgressColor:(UIColor *)progressColor
{
    _progressColor = progressColor;
    viewTop.backgroundColor = progressColor;
}

@end
