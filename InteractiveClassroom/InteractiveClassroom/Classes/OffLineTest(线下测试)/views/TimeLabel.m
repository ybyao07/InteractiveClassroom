//
//  TimeLabel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TimeLabel.h"

@interface TimeLabel()
{
    dispatch_source_t _timer;
}
@end
@implementation TimeLabel

- (void)startTimer:(NSInteger)time
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    __block NSInteger timeout = time; //倒计时时间
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0 * NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.text = [self timeFormatted:timeout];
            });
            timeout++;
    });
    dispatch_resume(_timer);
}
- (void)endTimer
{
    if (_timer) {
        dispatch_source_cancel(_timer);
    }
}
//转换成时分秒
- (NSString *)timeFormatted:(NSInteger)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    if (hours > 0) {
            return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    }else{
        return [NSString stringWithFormat:@"%02d:%02d",minutes, seconds];
    }
}




@end
