//
//  HD_TeamCompetionVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_TeamCompetionVC.h"
#import "HD_StartCompeteVC.h"
#import "TeamGroupModel.h"
#import "TeamGroupSectionCell.h"
#import "PopContainerVC.h"
#import "HDSocketSendCommandTool.h"
#import "AlertScreenView.h"

@interface HD_TeamCompetionVC ()<UITableViewDelegate,UITableViewDataSource,PopDeletegate,AlertScreenViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnGroupType;
@property (nonatomic, strong) UIPopoverController *popOver;

@property (nonatomic, strong) NSArray *dataGroups;
@property (strong, nonatomic) NSMutableArray <SmallGroupModel *> *resources;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) TeamGroupModel *currentGroupM;

@end

@implementation HD_TeamCompetionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    [self.tableView registerNib:[UINib nibWithNibName:@"TeamGroupSectionCell" bundle:nil] forCellReuseIdentifier:@"TeamGroupSectionCell"];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeNotification) name:@"CloseTeamCompetion" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
//    [self addTestData];
    [self showCoverView];
}

- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"data"] = @[
                     @{
                         @"classId":@"15",
                         @"groupName":@"预设分组1",
                         @"id":@"22",
                         @"isPresent":@(1),
                         @"smallGroupsInfo":
                             @[
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"A",
                                     @"studentList":@[
                                     @{
                                         @"isLeader":@"1",
                                         @"studentId":@"124",
                                         @"studentName":@"张三"
                                         },
                                     @{
                                         @"isLeader":@"0",
                                         @"studentId":@"125",
                                         @"studentName":@"李四"
                                         }, @{
                                         @"isLeader":@"1",
                                         @"studentId":@"124",
                                         @"studentName":@"张三"
                                         },
                                     @{
                                         @"isLeader":@"0",
                                         @"studentId":@"125",
                                         @"studentName":@"李四"
                                         }, @{
                                         @"isLeader":@"1",
                                         @"studentId":@"124",
                                         @"studentName":@"张三"
                                         },
                                     @{
                                         @"isLeader":@"0",
                                         @"studentId":@"125",
                                         @"studentName":@"李四"
                                         }]
                                 },
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"126",
                                                 @"studentName":@"王五"
                                                 }]
                                     }
                                ]
                         },
                     @{
                         @"classId":@"15",
                         @"groupName":@"预设分组2",
                         @"id":@"22",
                         @"isPresent":@(1),
                         @"smallGroupsInfo":
                             @[
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"124",
                                                 @"studentName":@"张三"
                                                 },
                                             @{
                                                 @"isLeader":@"0",
                                                 @"studentId":@"125",
                                                 @"studentName":@"李四"
                                                 }]
                                     },
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"126",
                                                 @"studentName":@"王五"
                                                 }]
                                     }
                                 ]
                         }];
    
    self.dataGroups = [TeamGroupModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    self.currentGroupM = self.dataGroups.firstObject;
    [self.resources removeAllObjects];
    [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
//    [self.collectionView reloadData];
}


- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_Competition"]) {
        if (open == 1) {
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupsInfo) {
        self.dataGroups = [TeamGroupModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        self.currentGroupM = self.dataGroups.firstObject;
        [self.btnGroupType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentGroupM.groupName] forState:UIControlStateNormal];
        [self.resources removeAllObjects];
        [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
        [self.tableView reloadData];
    }
}

- (IBAction)showPopVC:(UIButton *)sender {
    PopContainerVC *menuVC = [[PopContainerVC alloc] init];
    menuVC.delegate = self;
    NSMutableArray *popArray = [NSMutableArray array];
    [self.dataGroups enumerateObjectsUsingBlock:^(TeamGroupModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [popArray addObject:obj.groupName];
    }];
    menuVC.dataSource = popArray;
    self.popOver = [[UIPopoverController alloc] initWithContentViewController:menuVC];
    // 设置内容的尺寸  如果内容控制器里设置了 preferredContentSize 大小 self.preferredContentSize = CGSizeMake(200, 200); 那么 self.popOver.popoverContentSize 将无意义，而且在实际开发中也应该由内容控制器控制大小
    self.popOver.popoverContentSize = CGSizeMake(200, popArray.count * 50);
    // 这个方法是自定义出现的位置  Rect：箭头指向的区域范围，以View的左上角为坐标原点
    [self.popOver presentPopoverFromRect:Rect(150, SCREEN_HEIGHT - 130, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

#pragma mark =========== PopContainerDelegate ===
- (void)changePopType:(int)typeIndex
{
    self.currentGroupM = self.dataGroups[typeIndex];
    [self.btnGroupType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentGroupM.groupName] forState:UIControlStateNormal];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"groupId"] = self.currentGroupM.groupId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(CurrentGroupInfo) successBlock:^(id result) {
        [self.resources removeAllObjects];
        [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
        [self.tableView reloadData];
    }];
    [self.resources removeAllObjects];
    [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
    [self.tableView reloadData];
}

- (IBAction)startAction:(UIButton *)sender {
    HD_StartCompeteVC *startVC = [HD_StartCompeteVC new];
    startVC.model = self.currentGroupM;
    [self.navigationController pushViewController:startVC animated:YES];
}

#pragma mark ====== UITableView ====
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resources.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SmallGroupModel *model = self.resources[indexPath.section];
    int line = (int) model.studentList.count / 5;
    if ( line >= 1) {
        return  60 + (line + 1) *50;
    }
    return 100;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TeamGroupSectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TeamGroupSectionCell"];
    SmallGroupModel *model = self.resources[indexPath.section];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}

- (void)closeNotification
{
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
}

#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [self showCoverView];
        [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
    }
}

- (void)startAction{
    [self removeCoverView];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(GroupCompetition) successBlock:^(id result) {
    }];
}


#pragma mark ===== accessory =====
- (NSMutableArray <SmallGroupModel *> *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
