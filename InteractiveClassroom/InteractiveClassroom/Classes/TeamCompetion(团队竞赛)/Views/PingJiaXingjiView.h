//
//  PingJiaXingjiView.h
//  pokuton
//
//  Created by apple on 2017/7/13.
//  Copyright © 2017年 wangjun. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface PingJiaXingjiView : UIView


/* 展示的星数 */
@property (nonatomic, assign) NSInteger show_star;
/* 星星之间的间隔 */
@property (nonatomic, assign) CGFloat space;


/* 距离左边的间距 */
@property (nonatomic, assign) IBInspectable CGFloat padding;
/* 最多的星数，默认为5 */
@property (nonatomic, assign) IBInspectable NSInteger max_star;
/* 是否支持选择星数 */
@property (nonatomic, assign) IBInspectable BOOL isSelect;
@property (nonatomic, strong) IBInspectable UIImage *normalImg;
@property (nonatomic, strong) IBInspectable UIImage *highlightImg;

@end
