//
//  QuickAnswerView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PingJiaXingjiView.h"
#import "TeamGroupModel.h"

@interface QuickAnswerView : UIView
{
    long _currentStarLevel;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *starViews;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnGo;

@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@property (weak, nonatomic) IBOutlet PingJiaXingjiView *starView;

@property (strong, nonatomic) TeamGroupModel *model;

+(QuickAnswerView *)instanceView;

@end
