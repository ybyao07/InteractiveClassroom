//
//  QuickAnswerView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "QuickAnswerView.h"
#import "HDRankStarVC.h"
#import "UIView+Extension.h"
#import "TeamAllAnswerVC.h"
#import "TeamNoAnswerVC.h"
#import "AlertScreenView.h"

static const NSInteger  starLevelMin = 1;
static const NSInteger  starLevelMax = 5;
@interface QuickAnswerView()<AlertScreenViewDelegate>
@end
@implementation QuickAnswerView


- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(30,  30, SCREEN_WIDTH - 60, SCREEN_HEIGHT - 30 * 2);
    self.layer.cornerRadius = 20;
    self.starView.space = 6;
    self.starView.userInteractionEnabled = NO;
    _currentStarLevel= 1;
    _btnGo.hidden = YES;
    self.starView.show_star = _currentStarLevel;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
}

+(QuickAnswerView *)instanceView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"QuickAnswerView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupAnswerState) {
        SmallGroupModel *groupM = [SmallGroupModel mj_objectWithKeyValues:dic];
        if (groupM.studentName.length > 0) {//有人抢答
            TeamAllAnswerVC *rankVC = [TeamAllAnswerVC new];
            rankVC.model = groupM;
            [[self getCurrentViewController].navigationController pushViewController:rankVC animated:YES];
        }else{ // 无人抢答
            TeamNoAnswerVC *noVC = [TeamNoAnswerVC new];
            [[self getCurrentViewController].navigationController pushViewController:noVC animated:YES];
        }
    }
}

- (IBAction)decreaseClicked:(UIButton *)sender {
    _currentStarLevel = _currentStarLevel - 1;
    if (_currentStarLevel <= starLevelMin) {
        _currentStarLevel = starLevelMin;
    }
    self.starView.show_star = _currentStarLevel;
}
- (IBAction)increaseClicked:(UIButton *)sender {
    _currentStarLevel = _currentStarLevel + 1;
    if (_currentStarLevel >= starLevelMax) {
        _currentStarLevel = starLevelMax;
    }
    self.starView.show_star = _currentStarLevel;
}

- (IBAction)goAction:(UIButton *)sender {
    self.starViews.hidden = YES;
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"groupId"] = self.model.groupId;
    dic[@"starValue"] = [NSString stringWithFormat:@"%d",(int)self.starView.show_star];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartGroupAnswer) successBlock:^(id result) {
        
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3.0;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
}




- (void)updateLabel
{
    self.lblTitle.text = @"按下答题键开始抢答！";
}

- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
    
}
#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseTeamCompetion" object:nil];
        [[self getCurrentViewController].navigationController popToRootViewControllerAnimated:YES];
    }
}


@end
