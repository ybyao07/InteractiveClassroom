//
//  RankStarView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamStarModel.h"

@interface RankStarView : UIView

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) NSArray <TeamStarModel *> *dataArr;

//@property (nonatomic, strong)
+(RankStarView *)instanceDeviceView;

@end
