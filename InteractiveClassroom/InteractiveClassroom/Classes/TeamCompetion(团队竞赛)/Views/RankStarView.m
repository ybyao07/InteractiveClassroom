//
//  RankStarView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "RankStarView.h"
#import "PNBarChart.h"
#import "UIView+Extension.h"
#import "YBBarCharVerticalView.h"
#import "AlertScreenView.h"

@interface RankStarView()<AlertScreenViewDelegate>

@end
@implementation RankStarView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(20, 30, SCREEN_WIDTH - 40, SCREEN_HEIGHT - 30*2);
}

+(RankStarView *)instanceDeviceView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"RankStarView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}

- (void)setDataArr:(NSArray<TeamStarModel *> *)dataArr
{
    _dataArr = dataArr;
    NSMutableArray *xlabels = [NSMutableArray arrayWithCapacity:dataArr.count];
    NSMutableArray *yValues = [NSMutableArray arrayWithCapacity:dataArr.count];
    NSMutableArray *colors = [NSMutableArray arrayWithCapacity:dataArr.count];
    __block int index = 0;
    __block int maxValue = 0;
    __block int noValue = 0;
    [_dataArr enumerateObjectsUsingBlock:^(TeamStarModel * _Nonnull starM, NSUInteger idx, BOOL * _Nonnull stop) {
        [xlabels addObject:[NSString stringWithFormat:@"%@",starM.smallGroup]];
        [yValues addObject:[NSString stringWithFormat:@"%d星",(int)[starM.StarCount integerValue]]];
        [colors addObject:JUIColorFromRGB_Main3];
        if ([starM.smallGroup isEqualToString:@"X"]||[starM.smallGroup isEqualToString:@"x"]) {
            noValue = [starM.StarCount intValue];
            [colors replaceObjectAtIndex:idx withObject:JUIColorFromRGB_Gray2];//灰色
            [xlabels replaceObjectAtIndex:idx withObject:@"无效"];
        }else{
            if ([starM.StarCount intValue]  > maxValue) {
                maxValue = [starM.StarCount intValue];
                index = (int)idx;
            }
        }
    }];
    [colors replaceObjectAtIndex:index withObject:JUIColorFromRGB_Major1];
    CGRect frame = CGRectMake(200, 70.0, SCREEN_WIDTH-400, SCREEN_HEIGHT - 300);
    CGFloat startX = 0;
    startX = ((SCREEN_WIDTH - 400) - _dataArr.count*40 - (_dataArr.count - 1)*40 -90)/2.0;
    CGPoint startPoint = CGPointMake(startX, 0);
    YBBarCharVerticalView *barChartView = [[YBBarCharVerticalView alloc] initWithFrame:frame
                                                              startPoint:startPoint
                                                                  values:yValues
                                                                  colors:colors
                                                                maxValue:maxValue > noValue?maxValue:noValue
                                                          textIndicators:xlabels
                                                               textColor:[UIColor blackColor]
                                                               barWidth:40
                                                             barMaxHeight:SCREEN_HEIGHT-470];
    
    
    [self.containerView addSubview:barChartView];
}
- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}
#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
        [[self getCurrentViewController].navigationController popToRootViewControllerAnimated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseTeamCompetion" object:nil];
    }
}


- (IBAction)backAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(ReturnGroupAnswer) successBlock:^(id result) {
        
    }];
    [[self getCurrentViewController].navigationController popViewControllerAnimated:YES];
}





@end
