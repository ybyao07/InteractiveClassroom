//
//  TeamGroupSectionCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TeamGroupSectionCell.h"
#import "TestStudentProgressCell.h"

#define Subject_CELL_WIDTH (SCREEN_WIDTH - 30*2 - 20*2 - 2*50 - 100 - 20 - 20*4)/5
@implementation TeamGroupSectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(SmallGroupModel *)model
{
    _model = model;
    _lblSmallGroupName.text = [NSString stringWithFormat:@"小组%@",model.smallGroup];
    [self.collectionView reloadData];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.model.studentList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    TeamStudent *model = self.model.studentList[indexPath.row];
    cell.backgroundColor = JUIColorFromRGB_Major1;
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

@end
