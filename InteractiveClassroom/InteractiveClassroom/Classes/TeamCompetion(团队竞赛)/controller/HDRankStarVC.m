//
//  HDRankStarVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDRankStarVC.h"
#import "RankStarView.h"
#import "TeamStarModel.h"
@interface HDRankStarVC ()
{
    RankStarView *rankView;
}

@property (strong, nonatomic) NSArray <TeamStarModel *> *dataArr;
@end

@implementation HDRankStarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"团队竞赛";
    self.hd_navigationBar.hidden = YES;
    rankView = [RankStarView instanceDeviceView];
    [self.view addSubview:rankView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
//    [self addTestData];
}


- (void)addTestData
{
    NSDictionary *dic = @{
                          @"smallGroupsInfo":
                              @[@{
                                    @"smallGroup":@"A",
                                    @"StarCount":@(5)
                                    },
                                @{
                                    @"smallGroup":@"B",
                                    @"StarCount":@(4)
                                    }]
                          };
    _dataArr = [TeamStarModel mj_objectArrayWithKeyValuesArray:dic[@"smallGroupsInfo"]];
    rankView.dataArr = _dataArr;
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupAnswerRankingInfo) {
        _dataArr = [TeamStarModel mj_objectArrayWithKeyValuesArray:dic[@"smallGroupsInfo"]];
        rankView.dataArr = _dataArr;
    }
}

- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(GroupAnswerRanking) successBlock:^(id result) {
    }];
}





- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
