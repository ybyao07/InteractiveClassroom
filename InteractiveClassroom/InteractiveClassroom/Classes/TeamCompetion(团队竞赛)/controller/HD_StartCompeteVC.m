//
//  HD_StartCompeteVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_StartCompeteVC.h"
#import "QuickAnswerView.h"

@interface HD_StartCompeteVC ()


@end

@implementation HD_StartCompeteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"团队竞赛";
    self.hd_navigationBar.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for (UIView *subView in self.view.subviews) {
        [subView removeFromSuperview];
    }
    QuickAnswerView *answerView = [QuickAnswerView instanceView];
    answerView.model = self.model;
    [self.view addSubview:answerView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
