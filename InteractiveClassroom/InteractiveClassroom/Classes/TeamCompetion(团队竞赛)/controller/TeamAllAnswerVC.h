//
//  TeamAllAnswerVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmallGroupModel.h"
#import "YBaseViewController.h"

@interface TeamAllAnswerVC : YBaseViewController

@property (strong, nonatomic) SmallGroupModel *model;

@end
