//
//  TeamAllAnswerVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TeamAllAnswerVC.h"
#import "TestStudentProgressCell.h"
#import "HDSocketSendCommandTool.h"
#import "HDRankStarVC.h"
#import "AlertScreenView.h"

#define Subject_CELL_WIDTH (SCREEN_WIDTH - 50*2 - 2 *10 - 2 *28 - 2 * 20 - 10*5)/6

@interface TeamAllAnswerVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,AlertScreenViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation TeamAllAnswerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    _lblTitle.text = [NSString stringWithFormat:@"恭喜小组%@抢答成功",self.model.smallGroup];
    _lblName.text = [NSString stringWithFormat:@"抢答者：%@",self.model.studentName];
}

#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.model.studentList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    TeamStudent *model = self.model.studentList[indexPath.row];
    cell.backgroundColor = JUIColorFromRGB_Major1;
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Subject_CELL_WIDTH, 40);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}


#pragma mark ====== UIButtonAction ======
//加星值 == 只能加星值一次
- (IBAction)increaseStar:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(AddStar) successBlock:^(id result) {
        //
    }];
    sender.enabled = NO;
}
- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(3)} commandCode:@(Praise) successBlock:^(id result) {
        //
    }];
}
// 重新抢答
- (IBAction)reQucickAgain:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rangAction:(UIButton *)sender
{
    HDRankStarVC *rankVC = [HDRankStarVC new];
    [self.navigationController pushViewController:rankVC animated:YES];
}
- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}


#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseTeamCompetion" object:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
