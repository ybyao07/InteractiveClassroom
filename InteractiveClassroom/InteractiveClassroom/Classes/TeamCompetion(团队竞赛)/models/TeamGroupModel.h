//
//  TeamGroupModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SmallGroupModel.h"

@interface TeamGroupModel : NSObject

@property (nonatomic, copy) NSString *groupId;//分组id
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *classId; //班级Id
@property (nonatomic, strong) NSNumber *isPresent;//是否预设分组 1是预设分组 2临时分组

@property (nonatomic, strong) NSArray <SmallGroupModel *> *smallGroupsInfo;

@end
