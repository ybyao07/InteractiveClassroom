//
//  JHttpUpLoader.m
//  PgtThirdParty
//
//  Created by HX on 2017/8/22.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#import "JHttpUpLoader.h"



@implementation JHttpUpLoader

#pragma mark --------单张图片上传-------

+ (void)upLoadImage : (UIImage *)image success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    if ([self checkImageType:image]) {
        [self POST:IMAGE_URL parameters:nil image:image success:^(id responseObject) {
            success(responseObject);
        } failure:^(NSError *error) {
            failure(error);
        }];
    }else {
        NSLog(@"---------传入的参数类型错误，应为“UIImage”--------");
    }
    
}

#pragma mark --------单张图片上传 带参数-------

+ (void)upLoadImage : (UIImage *)image parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    if ([self checkImageType:image]) {
        [self POST:IMAGE_URL parameters:parameter image:image success:^(id responseObject) {
            success(responseObject);
        } failure:^(NSError *error) {
            failure(error);
        }];
    }else {
        NSLog(@"---------传入的参数类型错误，应为“UIImage”--------");
    }
}

#pragma mark --------多张图片上传-------

+ (void)upLoadImages : (NSMutableArray *)images success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    if ([self checkImages:images]) {
        [self POST:IMAGE_URL parameters:nil images:images success:^(id responseObject) {
            success(responseObject);
        } failure:^(NSError *error) {
            failure(error);
        }];
    }else {
        NSLog(@"--------数组不可为空/数组内有非UIImage类型元素--------");
    }
}

#pragma mark --------多张图片上传 带参数-------

+ (void)upLoadImages : (NSMutableArray *)images parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    if ([self checkImages:images]) {
        [self POST:IMAGE_URL parameters:parameter images:images success:^(id responseObject) {
            success(responseObject);
        } failure:^(NSError *error) {
            failure(error);
        }];
    }else {
        NSLog(@"--------数组不可为空/数组内有非UIImage类型元素--------");
    }
}

#pragma mark --------上传多张图片，每次只传一张--------

+ (void)upLoadImagesBySerial : (NSMutableArray *)images parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    if ([self checkImages:images]) {
        dispatch_semaphore_t sem = dispatch_semaphore_create(1);
        dispatch_queue_t queue = dispatch_queue_create("upLoadImage", NULL);
        
        dispatch_async(queue, ^{
            for ( UIImage *image in images ){
                dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
                
                [self POST:IMAGE_URL parameters:parameter image:image success:^(id responseObject) {
                    success(responseObject);
                    dispatch_semaphore_signal(sem);
                } failure:^(NSError *error) {
                    failure(error);
                    dispatch_semaphore_signal(sem);
                }];
            }
        });
    }
}


#pragma mark --------检查图片类型的正确与否--------
+ (BOOL) checkImageType : (UIImage*)image {
    return [image isKindOfClass:[UIImage class]];
}

#pragma mark --------检查图片数组不为空及其元素类型的正确与否--------
+ (BOOL) checkImages : (NSArray*)images {
    if (images.count >0) {
        NSUInteger falg = 0; //记录正确元素的个数
        
        for (UIImage *image in images) {
            if ([image isKindOfClass:[UIImage class]]) {
                falg += 1;
            }
        }
        
        // 判断数组中所有的元素是否正确
        if (falg == images.count) {
            return true;
        }else {
            return false;
        }
    }else{
        return false;
    }
}


@end
