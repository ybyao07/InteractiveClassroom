//
//  PhotoViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/21.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PhotoViewController.h"
#import "DBAddPhotoView.h"
#import "PreviewPhotoVC.h"


#define kLeftMargin 30

@interface PhotoViewController ()<DBAddPhotoViewDelegate>

@property (strong, nonatomic) UIScrollView * scrollView;
@property (strong, nonatomic) DBAddPhotoView *addPhotoView;

@property (strong, nonatomic) UIView *noPicAddView;

@property (strong, nonatomic) UIView *bgView;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = @"上传照片";
    self.hd_navigationBar.hidden = YES;
    self.view.backgroundColor = JUIColorFromRGB_bg;
    [self addScrollView];
    [self addChildView];
    [self addButtons];
    [self.bgView addSubview:self.noPicAddView];
    [self.bgView bringSubviewToFront:self.noPicAddView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeAction) name:@"ClosePhoto" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_Photo"]) {
        if (open == 1) {
             [self removeCoverView];
        }
    }
}

//添加scrollView
-(void)addScrollView{
    _bgView = [[UIView alloc] initWithFrame:Rect(kLeftMargin, 30, SCREEN_WIDTH - kLeftMargin*2, SCREEN_HEIGHT - 30 - 70)];
    _bgView.layer.masksToBounds = YES;
    _bgView.layer.cornerRadius = 16;
    _bgView.backgroundColor = JUIColorFromRGB_bgGrayBlue;
    [self.view addSubview:_bgView];
    _scrollView  = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH-kLeftMargin*2, SCREEN_HEIGHT- 70 - 30 - 80)];
    _scrollView.backgroundColor = JUIColorFromRGB_bgGrayBlue;
    _scrollView.contentSize = CGSizeMake(0, SCREEN_WIDTH-kLeftMargin*2);
    _scrollView.scrollEnabled = NO;
    [self.bgView addSubview:_scrollView];
}

-(void)addChildView{
    _addPhotoView = [[DBAddPhotoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - kLeftMargin * 2, SCREEN_HEIGHT-64-70-60)];
    _addPhotoView.userInteractionEnabled = YES;
    _addPhotoView.delegate = self;
    [_scrollView addSubview:_addPhotoView];
    [_addPhotoView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)addButtons
{
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setImage:[UIImage imageNamed:@"answer_close"] forState:UIControlStateNormal];
    closeBtn.frame = Rect(SCREEN_WIDTH - 120, 20, 40, 40);
    [self.bgView addSubview:closeBtn];
    
    UIView *bgBottomView = [[UIView alloc] initWithFrame:Rect(0, CGRectGetMaxY(_scrollView.frame), SCREEN_WIDTH - 2 * kLeftMargin, 80)];
    bgBottomView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:bgBottomView];
    
    UIButton *btnComplete = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnComplete setTitle:@"完成" forState:UIControlStateNormal];
    [btnComplete setBackgroundImage:[UIImage imageNamed:@"icon_btn_selected"] forState:UIControlStateNormal];
    [btnComplete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnComplete addTarget:self action:@selector(completeAction) forControlEvents:UIControlEventTouchUpInside];
    btnComplete.frame = Rect((SCREEN_WIDTH - 2*kLeftMargin)/2.0 - 160, 20, 140, 40);
    [bgBottomView addSubview:btnComplete];
    
    UIButton *btnClear = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClear setTitle:@"清空" forState:UIControlStateNormal];
    [btnClear setTitleColor:JUIColorFromRGB_ButtonSelected forState:UIControlStateNormal];
    [btnClear setBackgroundImage:[UIImage imageNamed:@"icon_btn_unSelected"] forState:UIControlStateNormal];
    [btnClear addTarget:self action:@selector(clearAction) forControlEvents:UIControlEventTouchUpInside];
    btnClear.frame = Rect((SCREEN_WIDTH - 2*kLeftMargin)/2.0 + 20, 20, 140, 40);
    [bgBottomView addSubview:btnClear];
}


#pragma mark =========  =======
- (void)closeAction
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self showCoverView];
    [self clearAction];
    [self.bgView addSubview:self.noPicAddView];
}
- (void)completeAction
{
    PreviewPhotoVC *preVC = [PreviewPhotoVC new];
    NSMutableArray *dataA = [NSMutableArray arrayWithCapacity:_addPhotoView.getWillUploadImageViews.count];
    [_addPhotoView.getWillUploadImageViews enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PreViewCellModel * cellM = [PreViewCellModel createWithImageView:obj selected:NO];
        [dataA addObject:cellM];
    }];
    preVC.data = dataA;
    [self.navigationController pushViewController:preVC animated:YES];
}
- (void)clearAction
{
    [_addPhotoView removeFromSuperview];
    _addPhotoView = [[DBAddPhotoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64-70-60)];
    _addPhotoView.userInteractionEnabled = YES;
    _addPhotoView.delegate = self;
    [_scrollView addSubview:_addPhotoView];
    [_addPhotoView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

#pragma mark ------- KVO -----
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    HDLog(@"the frame has been changed");
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.addPhotoView.frame.size.height);
}


#pragma mark ----- DBAddPhotoViewDelegate ----
- (void)photoViewClick:(DBAddPhotoView *)photoView imageArray:(NSArray *)imageArray photoViewHight:(CGFloat)hight
{
    [self.noPicAddView removeFromSuperview];
}

- (UIView *)noPicAddView
{
    if (!_noPicAddView) {
        _noPicAddView = [[UIView alloc] initWithFrame:Rect(0,0 , SCREEN_WIDTH - 60, SCREEN_HEIGHT - 30 - 70)];
        _noPicAddView.backgroundColor = [UIColor whiteColor];
        _noPicAddView.layer.cornerRadius = 16;
        UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAdd setImage:[UIImage imageNamed:@"btn_add_68"] forState:UIControlStateNormal];
        [btnAdd setImage:[UIImage imageNamed:@"btn_add_68"] forState:UIControlStateSelected];
        [btnAdd addTarget:self action:@selector(choosePhoto:) forControlEvents:UIControlEventTouchUpInside];
        btnAdd.frame = Rect(_noPicAddView.bounds.size.width/2.0-68/2,_noPicAddView.bounds.size.height/2-68/2, 68, 68);
        [_noPicAddView addSubview:btnAdd];
        UILabel *lblHint = [UILabel new];
        lblHint.textColor = JUIColorFromRGB_Main1;
        lblHint.textAlignment = NSTextAlignmentCenter;
        lblHint.text = @"点击上传照片";
        lblHint.font = JFONT(JFONT_Size_H2);
        lblHint.frame = Rect(0, CGRectGetMaxY(btnAdd.frame), SCREEN_WIDTH - 2 * kLeftMargin, 30);
        [_noPicAddView addSubview:lblHint];
    }
    return _noPicAddView;
}

- (void)choosePhoto:(UIButton *)btn
{
    [_addPhotoView commitBtn:btn];
}

- (void)dealloc
{
    [_addPhotoView removeObserver:self forKeyPath:@"frame"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
