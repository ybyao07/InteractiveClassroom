//
//  PreviewPhotoVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PreviewPhotoVC.h"
#import "PhotoPreviewCell.h"
#import "ThrowPhotoVC.h"
#import "UIImage+Stretch.h"

@interface PreviewPhotoVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray <UIImage *> *selectedCellModel;

@end

static CGFloat cellHeight = 0.f;
static CGFloat cellWidth = 0.f;

@implementation PreviewPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"照片预览";
    [self.collectionView registerNib:[UINib nibWithNibName:@"PhotoPreviewCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoPreviewCell"];
    cellHeight = (SCREEN_HEIGHT - 64 - 100 - 3*20)/3;
    cellWidth = (SCREEN_WIDTH - 2 * 50  - 2*30)/3;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"全选" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(chooseAllAction:) forControlEvents:UIControlEventTouchUpInside];
    self.hd_navigationBar.rightButton = btn;
    [self.selectedCellModel removeAllObjects];
//    [self addTestData];
}

- (void)addTestData
{
    self.data = [NSMutableArray array];
    for (int i = 0; i < 100 ; i++) {
        PreViewCellModel *model = [[PreViewCellModel alloc] init];
        [self.data addObject:model];
    }
}

#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoPreviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoPreviewCell" forIndexPath:indexPath];
    cell.model = self.data[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellWidth, cellHeight);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PreViewCellModel *model = self.data[indexPath.row];
    model.isSelected = !model.isSelected;
    [self.collectionView reloadData];
}

- (void)chooseAllAction:(UIButton *)btn
{
    [self.data enumerateObjectsUsingBlock:^(PreViewCellModel * _Nonnull model, NSUInteger idx, BOOL * _Nonnull stop) {
        model.isSelected = YES;
    }];
    [self.collectionView reloadData];
}

- (IBAction)close:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ClosePhoto" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)throwAction:(UIButton *)sender {
    [self.selectedCellModel removeAllObjects];
    [self.data enumerateObjectsUsingBlock:^(PreViewCellModel * _Nonnull model, NSUInteger idx, BOOL * _Nonnull stop) {
        if(model.isSelected){
            [self.selectedCellModel addObject:model.imgView.image];
        }
    }];
    if (self.selectedCellModel.count < 1) {
        [HelperToast showMessageWithHud:@"请先选择要投屏的图片" addTo:self yOffset:0];
        return;
    }
    ThrowPhotoVC *throwVC = [ThrowPhotoVC new];
    throwVC.modelArray = self.selectedCellModel;
//    throwVC.testImage = afterImage;
    [self.navigationController pushViewController:throwVC animated:YES];
}


- (NSMutableArray <UIImage *> *)selectedCellModel
{
    if (!_selectedCellModel) {
        _selectedCellModel = [NSMutableArray array];
    }
    return _selectedCellModel;
}


- (NSMutableArray *)data
{
    if (!_data) {
        _data = [NSMutableArray array];
    }
    return _data;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
