//
//  ThrowPhotoVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/31.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

@interface ThrowPhotoVC : YBaseViewController

@property (strong, nonatomic) NSMutableArray <UIImage *> *modelArray;

//@property (nonatomic, strong) UIImage *testImage;

@end

