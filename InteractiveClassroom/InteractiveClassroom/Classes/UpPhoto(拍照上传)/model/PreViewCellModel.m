//
//  PreViewCellModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PreViewCellModel.h"

@implementation PreViewCellModel

+ (instancetype)createWithImageView:(UIImageView *)imgV selected:(BOOL)selected
{
    PreViewCellModel *model = [[PreViewCellModel alloc] init];
    model.imgView = imgV;
    model.isSelected = selected;
    return model;
}
@end
