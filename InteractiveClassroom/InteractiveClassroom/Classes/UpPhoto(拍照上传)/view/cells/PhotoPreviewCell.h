//
//  PhotoPreviewCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreViewCellModel.h"

@interface PhotoPreviewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnChoose;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (nonatomic, strong) PreViewCellModel *model;


@end
