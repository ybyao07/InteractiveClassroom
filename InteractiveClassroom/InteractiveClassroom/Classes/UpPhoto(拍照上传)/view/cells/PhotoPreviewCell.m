//
//  PhotoPreviewCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PhotoPreviewCell.h"

@implementation PhotoPreviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(PreViewCellModel *)model
{
    _model = model;
//    self.imgView = model.imgView;
    self.imgView.image = model.imgView.image;
    self.btnChoose.selected = model.isSelected;
}
@end
