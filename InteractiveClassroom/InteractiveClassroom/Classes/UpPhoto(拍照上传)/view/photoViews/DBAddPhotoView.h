//
//  DBAddPhotoView.h
//  danbai_client_ios
//
//  Created by dbjyz on 15/9/14.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DBAddPhotoView;
@protocol DBAddPhotoViewDelegate <NSObject>
@optional
-(void)photoViewClick:(DBAddPhotoView *)photoView imageArray:(NSArray *)imageArray photoViewHight:(CGFloat)hight;
@end


@interface DBAddPhotoView : UIView

@property(nonatomic,weak) id<DBAddPhotoViewDelegate> delegate;

@property (nonatomic , assign) int shengyutu;
@property (nonatomic , weak) UIButton *addButton;

- (NSArray<UIImageView*> *)getWillUploadImageViews;

- (void)commitBtn:(UIButton *)btn;

@end
