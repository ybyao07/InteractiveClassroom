//
//  DBAddPhotoView.m
//  danbai_client_ios
//
//  Created by dbjyz on 15/9/14.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import "DBAddPhotoView.h"
#import "DBPhotoView.h"
//#import "CTAssetsPickerController.h"
#import "UIView+Extension.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+Stretch.h"
#import "SDWebImageDownloader.h"
#import "UIImage+URL.h"
#import "upload.h"
#import "ConstDef.h"

@interface DBAddPhotoView ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, weak) DBPhotoView *photosView;

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@end

const static int kCountTotalPhotos = 10;
@implementation DBAddPhotoView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupPhotosView];
        self.userInteractionEnabled = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCheckflag:) name:@"postinfoCheckflag" object:nil];
    }
    return self;
}

//设置放图片的View
-(void)setupPhotosView {
    DBPhotoView *photosView = [[DBPhotoView alloc] init];
//    photosView.backgroundColor = [UIColor yellowColor];
    photosView.frame = CGRectMake(0, 0, SCREEN_WIDTH - 2 * 30, SCREEN_HEIGHT - 64 - 70 - 60);
    [self addSubview:photosView];
    self.photosView = photosView;
    CGFloat width = (SCREEN_WIDTH - 2 * 30 - 2*spaceLeft - (ColumnCount - 1)*space)/ColumnCount;
    CGFloat height = (SCREEN_HEIGHT - 64 - 70 - 60 - 2*spaceTop - (RowCount - 1)*spaceVertical)/RowCount;
    UIButton *addButton = [[UIButton alloc]init];
    [addButton setBackgroundImage:[UIImage imageNamed:@"addPic"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(commitBtn:) forControlEvents:UIControlEventTouchUpInside];
    addButton.frame = CGRectMake(30, 30, width, height);
    [self addSubview:addButton];
    self.addButton = addButton;
    self.photosView.HandleAddLocation = ^{
        NSUInteger i = self.photosView.totalImages.count;
        NSUInteger line = i / ColumnCount;
        NSUInteger column = i % ColumnCount;
        CGFloat imageViewX = spaceLeft + column*(width + space);
        CGFloat imageViewY = line * (height + spaceVertical) + spaceTop;
        addButton.frame = CGRectMake(imageViewX, imageViewY, width, height);
//        photosView.frame = CGRectMake(0, 0, SCREEN_WIDTH, imageViewY + height);
        if ([self.delegate respondsToSelector:@selector(photoViewClick:imageArray:photoViewHight:)]) {
            [self.delegate photoViewClick:self imageArray:self.photosView.totalImages photoViewHight:imageViewY + width];
        }
        if (self.photosView.totalImages.count == kCountTotalPhotos) {
            addButton.hidden = YES;
        } else{
            addButton.hidden = NO;
        }
        self.photosView.frame = CGRectMake(0, 0, SCREEN_WIDTH - 2 * 30, imageViewY+height);
        [self layoutIfNeeded];
    };
}

-(void)setCheckflag:(NSNotification*)notification
{
    int i=[[notification object] intValue];
    self.shengyutu=i;
}

-(void)commitBtn:(UIButton *)btn {
    [self openCamera];
}
/** 调用摄像头 */
- (void)openCamera {
    BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
    if (!isCamera) {
        return;
    }
    [self openPhotoAlbumOrCamera:UIImagePickerControllerSourceTypeCamera];
}

- (void)openPhotoAlbumOrCamera:(UIImagePickerControllerSourceType)type {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    imagePicker.allowsEditing = YES;
    self.imagePicker = imagePicker;
    self.imagePicker.sourceType = type;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[self getCurrentViewController] presentViewController:self.imagePicker animated:YES completion:nil];
        }];
    }else{
        [[self getCurrentViewController] presentViewController:self.imagePicker animated:YES completion:nil];
    }
}

#pragma mark =================== 照相机控制器的代理 ===============
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // 1.销毁picker控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
    // 2.返回的图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.photosView addImage:image];
    //加号位置
    self.photosView.HandleAddLocation();
}

- (NSArray<UIImageView *> *)getWillUploadImageViews
{
    return _photosView.totalImageViews;
}


- (void)layoutSubviews
{
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH - 2 * 30, self.photosView.frame.size.height);
    [super layoutSubviews];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
