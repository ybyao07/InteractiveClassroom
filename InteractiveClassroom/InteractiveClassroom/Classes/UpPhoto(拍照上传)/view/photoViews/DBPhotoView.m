//
//  DBPhotoView.m
//  danbai_client_ios
//
//  Created by dbjyz on 15/7/6.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import "DBPhotoView.h"
#import "ZLPhotoPickerBrowserViewController.h"
#import "UIImageView+Progress.h"
#import "ConstDef.h"
#import "UIImage+URL.h"
#import "UIImageView+URL.h"
#import "UIView+Extension.h"
#import "upload.h"

@interface DBPhotoView ()<ZLPhotoPickerBrowserViewControllerDelegate,ZLPhotoPickerBrowserViewControllerDataSource>

@property (nonatomic,assign) CGFloat photoW;
@property (nonatomic,assign) CGFloat photoH;
@property (nonatomic, strong) NSMutableArray<UIImageView *> *imageViews;

@end

@implementation DBPhotoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
    }
    return self;
}

//增加图片
- (void)addImage:(UIImage *)image
{
    if (image == nil) {
        return;
    }
    self.addimageCount+= 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postinfoCheckflag" object:@(self.addimageCount)];
    //NSLog(@"图片总数=%d",self.addimageCount);
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.image = image;
    if (image.url) {
        imageView.url = image.url;
    }
    self.photoW = (SCREEN_WIDTH - 2 * 30 - 2*spaceLeft - (ColumnCount - 1)*space)/ColumnCount;
    self.photoH = (SCREEN_HEIGHT - 64 - 70 - 60 - 2*spaceTop -(ColumnCount - 1)*spaceVertical)/ColumnCount;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.photoW, self.photoH) cornerRadius:5.0];
    CAShapeLayer *shape = [CAShapeLayer layer];
    shape.path = path.CGPath;
    imageView.layer.mask = shape;
    [self addSubview:imageView];
    [self.imageViews addObject:imageView];
    //浏览大图手势
    UITapGestureRecognizer *tapBrowserImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(browPhotoView:)];
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:tapBrowserImage];
    CGFloat w = 20;
    CGFloat h = 20;
    CGFloat x = self.photoW - w;
    CGFloat y = 0;
    //删除手势
    UIImageView *deleteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    deleteImageView.image = [UIImage imageNamed:@"icon_delete"];
    deleteImageView.tag = 10001; // 必须设置，否则上传的时候会被删除掉
    [imageView addSubview:deleteImageView];
    UITapGestureRecognizer *tapDeleteImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteImage:)];
    deleteImageView.userInteractionEnabled = YES;
    [deleteImageView addGestureRecognizer:tapDeleteImage];
    //上传图片
    if (!image.url) {//如果存在下载过的图片，则不需要上传
//        [[YBUploadQiNiuManager sharedUploadManager] uploadImage:imageView];
    }
}


//删除图片
- (void)deleteImage:(UITapGestureRecognizer *)gesture
{
//    [((BaseViewController *)[self getCurrentViewController]) presentAlertVCTitle:@"" subtitle:@"确定删除当前照片？" leftBtnTitle:@"取消" rightBtnTitle:@"确定" leftBlock:^{
//        //
//    } rightBlock:^{
//        if ([self.imageViews containsObject:(UIImageView *)gesture.view.superview]) {
//            [self.imageViews removeObject:(UIImageView *)gesture.view.superview];
//        }
//        [gesture.view.superview removeFromSuperview];
//        if (self.HandleAddLocation) {
//            self.HandleAddLocation();
//        }
//        self.addimageCount-=1;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"postinfoCheckflag" object:@(self.addimageCount)];
//    }];
}



- (void)browPhotoView:(UITapGestureRecognizer *)gesture
{
    UIImageView *currentSelectedView = (UIImageView *)gesture.view;
    if ([currentSelectedView.status intValue] == 1) {
        NSLog(@"需要重新上传才行");
        return;
    }
    __block NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.imageViews enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([gesture.view isEqual:obj]) {
            indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
            *stop = YES;
        }
    }];
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    pickerBrowser.editing = YES;
    // 当前显示的分页数
    pickerBrowser.currentIndexPath = indexPath;
    // 展示控制器
    [pickerBrowser show];
}

#pragma mark ZLPhotoPickerBrowserViewControllerDataSource-------Delegate
- (NSInteger) numberOfSectionInPhotosInPickerBrowser:(ZLPhotoPickerBrowserViewController *) pickerBrowser
{
    return 1;
}

/**
 *  每个组多少个图片
 */
- (NSInteger) photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section
{
    return self.imageViews.count;
}

/**
 *  每个对应的IndexPath展示什么内容
 */
- (ZLPhotoPickerBrowserPhoto *)photoBrowser:(ZLPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath
{
    ZLPhotoPickerBrowserPhoto *browserPhoto = [ZLPhotoPickerBrowserPhoto photoAnyImageObjWith:self.totalImages[indexPath.row]];
//    browserPhoto.toView = nil;
    browserPhoto.thumbImage = self.totalImages[indexPath.row];
    return browserPhoto;
}

#pragma mark 删除调用
- (void)photoBrowser:(ZLPhotoPickerBrowserViewController *)photoBrowser removePhotoAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.totalImages.count) return;
    [self.imageViews[indexPath.row] removeFromSuperview];
    [self.imageViews removeObjectAtIndex:indexPath.row];
    if (self.HandleAddLocation) {
        self.HandleAddLocation();
    }
    self.addimageCount-=1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postinfoCheckflag" object:@(self.addimageCount)];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DeleteImage" object:nil];
}

//设置view的Frame会触发layoutSubviews，当然前提是frame的值设置前后发生了变化
//滚动一个UIScrollView会触发layoutSubviews
//改变一个UIView大小的时候也会触发父UIView上的layoutSubviews事件
- (void)layoutSubviews
{
    [super layoutSubviews];
    //获取视图上的所有子视图的个数
    NSInteger count = self.subviews.count;
    for (int i = 0; i < count; i++) {
        //根据视图的个数获取子视图
        UIImageView *imageView = self.subviews[i];
        int line = i / ColumnCount;
        int column = i % ColumnCount;
        CGFloat imageViewX = spaceLeft + column*(self.photoW + space);
        CGFloat imageViewY = line * (self.photoH + spaceVertical) + spaceTop ;
        imageView.frame = CGRectMake(imageViewX, imageViewY, self.photoW, self.photoH);
    }
}

- (NSArray *)totalImages
{
    NSMutableArray *images = [NSMutableArray array];
    for (UIImageView *imageView in self.subviews) {
        @try {
            [images addObject:imageView.image];
        }@catch (NSException *exception) {
            NSLog(@"%@ %@", [exception name], [exception reason]);
        }
        @finally {
        }
    }
    return images;
}

- (NSArray<UIImageView *>*)totalImageViews
{
    return self.imageViews;
}

- (NSMutableArray *)imageViews
{
    if (!_imageViews) {
        _imageViews = [NSMutableArray array];
    }
    return  _imageViews;
}


@end
