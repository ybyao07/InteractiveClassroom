//
//  UIImageView+Progress.h
//  TestMultiPhoto
//
//  Created by 姚永波 on 2017/9/21.
//  Copyright © 2017年 姚永波. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIImageView (Progress)

@property (nonatomic, strong) UILabel * progressIndicatorView; //百分比

@property (nonatomic, strong) UIView * failureView; //上传失败

@property (nonatomic, strong) UIView * bgBlackView; // 黑色蒙版

@property (nonatomic, strong) NSNumber *status;    // 0 初始状态 1需要重新上传

@end
