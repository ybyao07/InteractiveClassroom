//
//  UIImageView+Progress.m
//  TestMultiPhoto
//
//  Created by 姚永波 on 2017/9/21.
//  Copyright © 2017年 姚永波. All rights reserved.
//

#import "UIImageView+Progress.h"
#import <objc/runtime.h>
#import "ConstDef.h"
static char kXLImageProgressIndicatorKey;
static char kXLImageFailureKey;
static char kXLImageStatus;
static char kXLImageBgBlackKey;

//间距
#define space 5
//一行几个数
#define ColumnCount 4


#define kViewWidth  (SCREEN_WIDTH - 2*space - (ColumnCount - 1)*space)/ColumnCount

@interface UIImageView (_Progress)

@property (strong, nonatomic,setter=yb_setProgressIndicatorView:) UILabel *yb_progressIndicatorLabel;

@end

@implementation UIImageView (_Progress)

@dynamic yb_progressIndicatorLabel;
@end

@implementation UIImageView (Progress)

@dynamic progressIndicatorView;
@dynamic failureView;
@dynamic bgBlackView;

- (UILabel *)progressIndicatorView
{
    return  [self yb_progressIndicatorLabel];
}

- (UILabel *)yb_progressIndicatorLabel
{
    UILabel * progressIndicator = (UILabel *)objc_getAssociatedObject(self, &kXLImageProgressIndicatorKey);
    if (progressIndicator) return progressIndicator;
    progressIndicator = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, kViewWidth)];
    progressIndicator.center = CGPointMake(kViewWidth/ 2, kViewWidth / 2);
    progressIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    progressIndicator.textAlignment = NSTextAlignmentCenter;
    progressIndicator.textColor = [UIColor whiteColor];
    progressIndicator.text = @"等待中";
    [self yb_setProgressIndicatorView:progressIndicator];
    return progressIndicator;
}

- (void)yb_setProgressIndicatorView:(UILabel *)yb_progressIndicatorLabel
{
    objc_setAssociatedObject(self, &kXLImageProgressIndicatorKey, yb_progressIndicatorLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBgBlackView:(UIView *)bgBlackView
{
    objc_setAssociatedObject(self, &kXLImageBgBlackKey, bgBlackView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIView *)bgBlackView
{
    UIView * bgView = (UIView *)objc_getAssociatedObject(self, &kXLImageBgBlackKey);
    if (bgView) return bgView;
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, kViewWidth)];
    bgView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4];
    [self setBgBlackView:bgView];
    return  bgView;
}

- (void)setFailureView:(UIView *)failureView
{
    objc_setAssociatedObject(self, &kXLImageFailureKey, failureView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)failureView
{
    UIView * failView = (UIView *)objc_getAssociatedObject(self, &kXLImageFailureKey);
    if (failView) return failView;
    failView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, kViewWidth)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"failure_retry"]];
    imgView.center = CGPointMake(kViewWidth / 2, kViewWidth/ 2 - 10);
    [failView addSubview:imgView];
    UILabel *hint = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imgView.frame), kViewWidth, 20)];
    hint.textAlignment = NSTextAlignmentCenter;
    hint.text = @"重新上传";
    hint.textColor = [UIColor whiteColor];
    [failView addSubview:hint];
    return failView;
}

- (void)setStatus:(NSNumber *)status
{
    objc_setAssociatedObject(self, &kXLImageStatus,status, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)status
{
    NSNumber *uploadStatus = objc_getAssociatedObject(self, &kXLImageStatus);
    return uploadStatus;
}


@end
