//
//  upload.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/9/26.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#ifndef upload_h
#define upload_h

//间距
#define spaceLeft 100
#define spaceTop 20


#define space 30
#define spaceVertical 20

//一行几个数
#define ColumnCount 3
//能显示几行
#define RowCount 3

#define tableViewRowHeight 80.0f

#endif /* upload_h */
