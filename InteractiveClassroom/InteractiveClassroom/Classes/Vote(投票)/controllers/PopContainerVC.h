//
//  PopContainerVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopDeletegate <NSObject>

- (void)changePopType:(int)typeIndex;

@end

@interface PopContainerVC : UIViewController

@property (nonatomic, weak) id<PopDeletegate> delegate;

@property (nonatomic, strong) NSArray<NSString *> *dataSource;


@end
