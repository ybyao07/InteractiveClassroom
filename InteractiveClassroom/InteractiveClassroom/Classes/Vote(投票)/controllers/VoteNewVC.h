//
//  VoteNewVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "VoteItem.h"
@interface VoteNewVC : YBaseViewController<UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
