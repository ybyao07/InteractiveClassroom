//
//  VoteNewVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteNewVC.h"
#import "VoteOptionEditCell.h"
#import "VoteOptionModel.h"
#import "VoteDetailModel.h"
#import "VoteProgressVC.h"

@interface VoteNewVC ()<VoteOptionDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
//@property (nonatomic, strong) VoteDetailModel *voteModel;
@property (strong, nonatomic) UIView *footView;
@property (strong, nonatomic) NSMutableArray <VoteOptionModel *> *arrModels;

@property (nonatomic, strong) VoteDetailModel *voteModel;

@end

@implementation VoteNewVC

const int kMaxOption = 6;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    self.lblTitle.text = @"新建投票";
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteOptionEditCell" bundle:nil] forCellReuseIdentifier:@"VoteOptionEditCell"];
    self.tableView.tableFooterView = self.footView;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
    
    //初始话2个选项
    VoteOptionModel *voteObj1 = [VoteOptionModel createOptionLetter:[self letterGenarater] content:@""];
    [self.arrModels addObject:voteObj1];
    VoteOptionModel *voteObj2 = [VoteOptionModel createOptionLetter:[self letterGenarater] content:@""];
    [self.arrModels addObject:voteObj2];
}
- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteOption) {
        _voteModel = [VoteDetailModel mj_objectWithKeyValues:dic[@"data"]];
        VoteProgressVC *progressVC = [VoteProgressVC new];
        progressVC.voteModel = self.voteModel;
        [self.navigationController pushViewController:progressVC animated:YES];
    }
    if ([dic[@"code"] integerValue] == CreateVoteResponse) { //创建成功
        // id
    }
}

#pragma mark ======= UITableView ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrModels.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  100.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteOptionEditCell *editCell = [tableView dequeueReusableCellWithIdentifier:@"VoteOptionEditCell"];
    editCell.selectionStyle = UITableViewCellSelectionStyleNone;
    editCell.model = self.arrModels[indexPath.row];
    if (indexPath.row == 0 || indexPath.row == 1) {
        editCell.btnDelete.hidden = YES;
    }else{
        editCell.btnDelete.hidden = NO;
    }
    editCell.delegate = self;
    return editCell;
}


#pragma mark ====== VoteOptionDelegate ===
- (void)voteDelete:(VoteOptionModel *)model
{
    [self.arrModels removeObject:model];
    [self reSortLetter];
    [self.tableView reloadData];
    if (self.arrModels.count == kMaxOption) {
        self.footView.hidden = YES;
    }else{
        self.footView.hidden = NO;
    }
}

#pragma mark ====== action ======
- (IBAction)closeAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)completeAction:(UIButton *)sender {
    NSMutableArray *optionD = [NSMutableArray array];
    [self.arrModels enumerateObjectsUsingBlock:^(VoteOptionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dic = @{
                              @"optionLetter":obj.optionLetter,
                              @"optionContent":obj.optionContent.length>0?obj.optionContent:@""
                              };
        [optionD addObject:dic];
    }];
    [HDSocketSendCommandTool sendSocketParam:@{@"optionData":optionD} commandCode:@(CreateVote) successBlock:^(id result) {
    }];
}

- (void)addOptionAction:(UIButton *)btn
{
    VoteOptionModel *voteObj = [VoteOptionModel createOptionLetter:[self letterGenarater] content:@""];
    [self.arrModels addObject:voteObj];
    [self.tableView reloadData];
    if (self.arrModels.count == kMaxOption) {
        self.footView.hidden = YES;
    }else{
        self.footView.hidden = NO;
    }
}

- (UIView *)footView
{
    if (!_footView) {
        _footView = [[UIView alloc] initWithFrame:Rect(0, 0, self.tableView.bounds.size.width, 100)];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = JUIColorFromRGB_White;
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_add_option"] forState:UIControlStateNormal];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        btn.titleLabel.font = kFont18;
        [btn setTitle:@"添加选项" forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"vote_add"] forState:UIControlStateNormal];
        [btn setTitleColor:JUIColorFromRGB_Main1 forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addOptionAction:) forControlEvents:UIControlEventTouchUpInside];
        btn.frame = Rect(34, 10, SCREEN_WIDTH - 420, 90);
        btn.layer.cornerRadius = 30;
        [_footView addSubview:btn];
    }
    return _footView;
}



- (NSString *)letterGenarater
{
    switch (self.arrModels.count) {
        case 0:
            return @"A";
            break;
        case 1:
            return @"B";
            break;
        case 2:
            return @"C";
            break;
        case 3:
            return @"D";
            break;
        case 4:
            return @"E";
            break;
        case 5:
            return @"F";
            break;
        default:
            return @"A";
            break;
    }
}

- (void)reSortLetter
{
    [self.arrModels enumerateObjectsUsingBlock:^(VoteOptionModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        switch (idx) {
            case 0:
            {
                obj.optionLetter = @"A";
            }
                break;
            case 1:
            {
                obj.optionLetter = @"B";
            }
                break;
                case 2:
            {
                obj.optionLetter = @"C";
            }
                break;
                case 3:
            {
                obj.optionLetter = @"D";
            }
                break;
                case 4:
            {
                obj.optionLetter = @"E";
            }
                break;
            default:
                break;
        }
    }];
}


#pragma mark ==== accessory ===
- (NSMutableArray *)arrModels
{
    if (!_arrModels) {
        _arrModels = [NSMutableArray array];
    }
    return _arrModels;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
