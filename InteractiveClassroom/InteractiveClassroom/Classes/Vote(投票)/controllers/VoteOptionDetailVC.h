//
//  VoteOptionDetailVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "VoteItem.h"

@interface VoteOptionDetailVC : YBaseViewController

@property (strong, nonatomic) VoteItem *voteItem;

@end
