//
//  VoteOptionDetailVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteOptionDetailVC.h"
#import "VoteListNoEditCell.h"
#import "VoteDetailModel.h"
#import "VoteProgressVC.h"

@interface VoteOptionDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, strong) VoteDetailModel *voteModel;

@end

@implementation VoteOptionDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.hd_navigationBar.hidden = YES;
    self.lblTitle.text = self.voteItem.voteContent;
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteListNoEditCell" bundle:nil] forCellReuseIdentifier:@"VoteListNoEditCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
//    [self addTestData];
}

- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"voteId"] = @"121";
    dic[@"voteContent"] = @"春游目的投票";
    dic[@"optionList"] = @[@{
                               @"optionId":@"",
                               @"optionLetter":@"A",
                               @"optionContent":@"长城"
                               },
                           @{
                               @"optionId":@"",
                               @"optionLetter":@"B",
                               @"optionContent":@"故宫"
                               },
                           @{
                               @"optionId":@"",
                               @"optionLetter":@"A",
                               @"optionContent":@"天坛"
                               }];
    _voteModel = [VoteDetailModel mj_objectWithKeyValues:dic];
    [self.tableView reloadData];
}
//获取投票详情
- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{@"voteId":self.voteItem.voteId} commandCode:@(VoteDetails) successBlock:^(id result) {
        //
    }];
}
- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteOption) {
//        NSArray *arr = [VoteItem mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        _voteModel = [VoteDetailModel mj_objectWithKeyValues:dic[@"data"]];
        [self.tableView reloadData];
    }
}


#pragma mark ======= UITableView ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.voteModel.optionList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  100.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteListNoEditCell *noEditCell = [tableView dequeueReusableCellWithIdentifier:@"VoteListNoEditCell"];
        noEditCell.selectionStyle = UITableViewCellSelectionStyleNone;
        noEditCell.model = self.voteModel.optionList[indexPath.row];
        return noEditCell;
}

- (IBAction)startVoteAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"voteId":self.voteItem.voteId} commandCode:@(StartVote) successBlock:^(id result) {
    }];
    VoteProgressVC *progressVC = [VoteProgressVC new];
    progressVC.voteModel = self.voteModel;
    [self.navigationController pushViewController:progressVC animated:YES];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCloseVoteNotification object:nil];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
