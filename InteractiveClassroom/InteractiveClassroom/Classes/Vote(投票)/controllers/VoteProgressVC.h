//
//  VoteProgressVC.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteDetailModel.h"
#import "YBaseViewController.h"
@interface VoteProgressVC : YBaseViewController

@property (nonatomic, strong) VoteDetailModel *voteModel;

@end
