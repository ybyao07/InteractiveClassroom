//
//  VoteResultVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/31.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteResultVC.h"
#import "VoteOptionModel.h"
#import "YBBarCharHorizonView.h"

@interface VoteResultVC ()

@property (strong, nonatomic) NSMutableArray *arrData;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation VoteResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
    self.hd_navigationBar.hidden = YES;
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(VoteEnd) successBlock:^(id result) {
    }];
}
- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteResult) { //投票结果
        NSArray *arr = [VoteOptionModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        NSMutableArray *xlabels = [NSMutableArray arrayWithCapacity:arr.count];
        NSMutableArray *yValues = [NSMutableArray arrayWithCapacity:arr.count];
        NSMutableArray *colors = [NSMutableArray arrayWithCapacity:arr.count];
        __block int index = 0;
        __block int maxValue = 0; //maxValue可能是X 无效的人数
        __block int noValue = 0;
        [arr enumerateObjectsUsingBlock:^(VoteOptionModel * _Nonnull voteM, NSUInteger idx, BOOL * _Nonnull stop) {
            [xlabels addObject:[NSString stringWithFormat:@"选项%@     %@",voteM.optionLetter,voteM.optionContent]];
            [yValues addObject:[NSString stringWithFormat:@"%d人",(int)[voteM.selectedCount integerValue]]];
            [colors addObject:JUIColorFromRGB_Main3];//绿色
            if ([voteM.optionLetter isEqualToString:@"X"]||[voteM.optionLetter isEqualToString:@"x"]) {
                noValue = [voteM.selectedCount intValue];
                [colors replaceObjectAtIndex:idx withObject:JUIColorFromRGB_Gray2];//灰色
                [xlabels replaceObjectAtIndex:idx withObject:[NSString stringWithFormat:@"选项     %@",voteM.optionContent]];
            }else{
                if ([voteM.selectedCount intValue]  > maxValue) {
                    maxValue = [voteM.selectedCount intValue];
                    index = (int)idx;
                }
            }
        }];
        [colors replaceObjectAtIndex:index withObject:JUIColorFromRGB_Major1];//蓝色
        CGRect frame = CGRectMake(200, 10.0, SCREEN_WIDTH-400, SCREEN_HEIGHT - 300);
        YBBarCharHorizonView *barChartView = [[YBBarCharHorizonView alloc] initWithFrame:frame
                                                                  startPoint:CGPointMake(20, 50)
                                                                      values:yValues
                                                                      colors:colors
                                                                    maxValue:maxValue > noValue?maxValue:noValue
                                                              textIndicators:xlabels
                                                                   textColor:[UIColor blackColor]
                                                                   barHeight:40
                                                                 barMaxWidth:SCREEN_WIDTH-700
                                                                    ];
        
        
        [self.containerView addSubview:barChartView];
    }
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
     [[NSNotificationCenter defaultCenter] postNotificationName:kCloseVoteNotification object:nil];
}

#pragma mark ======  accessory =====
- (NSMutableArray *)arrData
{
    if (!_arrData) {
        _arrData = [NSMutableArray array];
    }
    return _arrData;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
