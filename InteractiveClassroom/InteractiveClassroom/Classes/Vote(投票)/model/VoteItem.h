//
//  VoteItem.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoteItem : NSObject

@property (nonatomic, copy) NSString *voteContent;
@property (nonatomic, copy) NSString *voteId;

@end
