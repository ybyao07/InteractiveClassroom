//
//  VoteOptionEditCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteOptionModel.h"

@protocol VoteOptionDelegate <NSObject>

- (void)voteDelete:(VoteOptionModel *)model;

@end
@interface VoteOptionEditCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfContent;

@property (strong, nonatomic) VoteOptionModel *model;
@property (nonatomic, weak) id<VoteOptionDelegate> delegate;

@end
