//
//  VoteOptionEditCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteOptionEditCell.h"

@implementation VoteOptionEditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _tfContent.delegate  = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(VoteOptionModel *)model
{
    _model = model;
    _lblTitle.text = [NSString stringWithFormat:@"选项%@",model.optionLetter];
    _tfContent.text = model.optionContent;
}
- (IBAction)editActiion:(UIButton *)sender {
    [_tfContent becomeFirstResponder];
}
- (IBAction)deleteAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(voteDelete:)]) {
        [self.delegate voteDelete:self.model];
    }
}


#pragma mark UITextField
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.model.optionContent = textField.text;
}






@end
