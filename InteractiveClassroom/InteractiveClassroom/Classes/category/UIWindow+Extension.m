//
//  UIWindow+Extension.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "UIWindow+Extension.h"
#import "AppDelegate.h"
//#define myDelegate  [UIApplication sharedApplication].delegate

@implementation UIWindow (Extension)
+ (UIWindow *) changeWindowRootViewController:(UIViewController *) viewController animated:(BOOL)animated
{
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow *window = myDelegate.window;
    if (animated) {
        [UIView transitionFromView:window.rootViewController.view toView:viewController.view duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft completion:^(BOOL finished) {
            window.rootViewController = viewController;
            [window makeKeyAndVisible];
        }];
    }else{
        window.rootViewController = viewController;
        [window makeKeyAndVisible];
    }
    return window;
}


+ (UIWindow *)getCurrentWindow
{
    return [[UIApplication sharedApplication].windows lastObject];
}

@end
