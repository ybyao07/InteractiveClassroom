//
//  HelperToast.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HelperToast.h"
#import "MBProgressHUD.h"

@implementation HelperToast
+(void)showLoadingWithView:(UIView *)aView{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:aView animated:YES];
    hud.label.text = @"加载中...";
    hud.label.font = [UIFont systemFontOfSize:14.0f];
}

+(void)hiddonLoadingWithView:(UIView *)aView{
    [MBProgressHUD hideHUDForView:aView animated:YES];
}

//显示提示框
+ (void)showMessageWithHud:(NSString*)message
                     addTo:(UIViewController*)controller
                   yOffset:(CGFloat)yoffset
{
    MBProgressHUD* hud = nil;
    if (controller.view) {
        hud = [MBProgressHUD showHUDAddedTo:controller.view animated:YES];
    }
    hud.mode = MBProgressHUDModeText;
    hud.label.text = message;
    hud.label.font = [UIFont systemFontOfSize:20];
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:2];
}
@end
