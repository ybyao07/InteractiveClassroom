//
//  JUITools.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JUITools : NSObject

/**
 *  get current root controller
 */
+(UINavigationController*)mainRootController;

/**
 *  get current root controller
 */
+(UINavigationController*)currentRootController;

//+(AppDelegate*)currentDelegate;

/**
 *  create back button
 *
 *  @param target   target
 *  @param selector selector
 *
 *  @return back button
 */
+ (UIBarButtonItem*)navButtonBack:(id)target selector:(SEL)selector;

/**
 *  create navegation button
 *
 *  @param target   target
 *  @param selector selector
 *  @param title    title
 *  @param aImage   image
 *
 *  @return navegation button
 */
+ (UIBarButtonItem*)navButtonWithTarget:(id)target selector:(SEL)selector title:(NSString*)title
                                  image:(UIImage*)aImage
                                 hAlign:(UIControlContentHorizontalAlignment)hAlign;

+ (void) showMessageBox:(NSString *) strTip;

//获取透明背景的searchBar
+ (UISearchBar *)getClearSearchBar:(UISearchBar *)oldSearchBar;

@end
