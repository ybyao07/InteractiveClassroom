//
//  JXNavigationBar+JXAdditions.h
//  jiuxian
//
//  Created by 万昌军 on 16/1/27.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import "JXNavigationBar.h"

NS_ASSUME_NONNULL_BEGIN
@interface JXNavigationBar (JXAdditions)

- (void)jx_setBackgroundColor:(UIColor *)backgroundColor;

- (void)jx_setTranslationY:(CGFloat)translationY;

- (void)jx_reset;

@end
NS_ASSUME_NONNULL_END
