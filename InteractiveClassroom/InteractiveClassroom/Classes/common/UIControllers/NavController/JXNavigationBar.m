//
//  JXNavigationBar.m
//  jiuxian
//
//  Created by 万昌军 on 16/1/26.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import "JXNavigationBar.h"
#import "Masonry.h"
#import "UIView+Extension.h"
@implementation JXNavigationBar

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)]) {
        [self initSelf];
    }
    return self;
}

- (void)initSelf{
    UIImage *backgroundImage = [UIImage imageNamed:@"navigationbg7"];
    self.layer.contents = (id)backgroundImage.CGImage;
    _separationLineView = [[UIView alloc] initWithFrame:CGRectZero];
    UIImage *separationImage = [UIImage imageNamed:@"JXHomeNavBar_UnderLine"];
    _separationLineView.layer.contents = (id)separationImage.CGImage;
    [self addSubview:_separationLineView];
    __weak __typeof__(self) weakSelf = self;
    [_separationLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.height.mas_equalTo(@.5);
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
    }];
    self.backgroundColor = JUIColorFromRGB_bg;
}

- (void)setupLeftButton{
    _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_leftButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _leftButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_leftButton setAdjustsImageWhenHighlighted:NO];
    [self addSubview:_leftButton];
    __weak __typeof__(self) weakSelf = self;
    [_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50, 44));
    }];
}

- (void)setupRightButton{
    _rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:_rightButton];
    _rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_rightButton setAdjustsImageWhenHighlighted:NO];
    __weak __typeof__(self) weakSelf = self;
    [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50, 44));
    }];
}

- (void)setupTitleView{
    _titleView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:_titleView];
    __weak __typeof__(self) weakSelf = self;
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.left.equalTo(self.mas_left).offset(50);
        make.right.equalTo(self.mas_right).offset(-50);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(@44);
    }];
}

- (void)setupTitleLabel{
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [_titleView addSubview:_titleLabel];
    _titleLabel.font = [UIFont systemFontOfSize:18];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor whiteColor];
    [_titleView addSubview:_titleLabel];
    __weak __typeof__(self) weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.left.equalTo(self.titleView.mas_left);
        make.top.equalTo(self.titleView.mas_top);
        make.bottom.equalTo(self.titleView.mas_bottom);
        make.right.equalTo(self.titleView.mas_right);
    }];
}

- (void)resetNavigationBar{
    if (_leftButton) {
        [_leftButton removeFromSuperview];
    }
    if (_rightButton) {
        [_rightButton removeFromSuperview];
    }
    if (_titleView) {
        [_titleView removeFromSuperview];
    }
    if (_separationLineView) {
        [_separationLineView removeFromSuperview];
    }
    if (_titleLabel) {
        [_titleLabel removeFromSuperview];
        _title = nil;
    }
    self.layer.contents = nil;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    _titleLabel.text = title;
}

- (void)setLeftButton:(UIButton *)leftButton{
    if (_leftButton) {
        [_leftButton removeFromSuperview];
    }
    _leftButton = leftButton;
    [self addSubview:_leftButton];
    if (_leftButton.y != 0) {
        return;
    }
    __weak __typeof__(self) weakSelf = self;
    [_leftButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50, 44));
    }];
}

- (void)setRightButton:(UIButton *)rightButton{
    if (_rightButton) {
        [_rightButton removeFromSuperview];
    }
    _rightButton = rightButton;
    [self addSubview:_rightButton];
    if (_rightButton.y != 0) {
        return;
    }
    __weak __typeof__(self) weakSelf = self;
    [_rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50, 44));
    }];
}

- (void)setTitleView:(UIView *)titleView{
    if (_titleView) {
        [_titleView removeFromSuperview];
    }
    _titleView = titleView;
    [self addSubview:_titleView];
    __weak __typeof__(self) weakSelf = self;
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        __typeof__(self) self = weakSelf;
        make.left.equalTo(self.mas_left).offset(50);
        make.right.equalTo(self.mas_right).offset(-50);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(@44);
    }];
}

- (void)setTitleLabel:(UILabel *)titleLabel{
    if (_titleLabel) {
        [_titleLabel removeFromSuperview];
    }
    _titleLabel = titleLabel;
    _title = titleLabel.text;
    if (_titleView) {
        [_titleView addSubview:_titleLabel];
        __weak __typeof__(self) weakSelf = self;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            __typeof__(self) self = weakSelf;
            make.left.equalTo(self.titleView.mas_left);
            make.top.equalTo(self.titleView.mas_top);
            make.bottom.equalTo(self.titleView.mas_bottom);
            make.right.equalTo(self.titleView.mas_right);
        }];
    }
    
}

@end
