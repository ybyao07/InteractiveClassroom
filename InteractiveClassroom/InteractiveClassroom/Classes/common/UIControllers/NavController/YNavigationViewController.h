//
//  YNavigationViewController.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNavigationViewController : UINavigationController

-(void)showSysTab:(UIView*)tab;

@end
