//
//  YNavigationViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "YNavigationViewController.h"

@interface YNavigationViewController ()

@end

@implementation YNavigationViewController

-(void)showSysTab:(UIView *)tab
{
    if (self.topViewController.view)
    {
        [self.topViewController.view addSubview:tab];
        tab.layer.zPosition = 99;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.hidden = YES;
//    [[UINavigationBar appearance] setTintColor:JUIColorFromRGB_White];
//    [[UINavigationBar appearance] setTitleTextAttributes: @{NSFontAttributeName:[UIFont boldSystemFontOfSize:20], NSForegroundColorAttributeName:JUIColorFromRGB_White}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
