//
//  TabToolBar.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TabToolBar.h"
#import "TabBarButton.h"
#import "UIWindow+Extension.h"
#import "HDClassMainViewController.h"

@interface TabToolBar()

@end

@implementation TabToolBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        _selected = -1;
        _fixedSpace = YES;
        _topShade = nil;
    }
    return self;
}

#pragma mark ===== 设置属性 =========
- (void)setBackgroundView:(UIImageView *)backgroundView
{
    UIImageView *bg = backgroundView;
    [_backgroundView removeFromSuperview];
    _backgroundView = bg;
    [self addSubview:_backgroundView];
    [self sendSubviewToBack:_backgroundView];
}
- (void)setItems:(NSArray *)items
{
    if (_items != items) {
        _items = items;
        for (TabBarButton *btn in _items) {
            [btn addTarget:self action:@selector(click:)];
            [self addSubview:btn];
        }
    }
}

-(void)setTabTopShade
{
    if (!_topShade)
    {
        UIImage* img    = [UIImage imageNamed:@"tabbarUpShadow.png"];
        _topShade       = [[UIImageView alloc] initWithImage: img];
        _topShade.frame =  CGRectMake(0, 0 - img.size.height,
                                      img.size.width,img.size.height);
        
        [self addSubview:_topShade];
        //        [_topShade release];
    }
}

- (TabBarButton*) getSelectedItem
{
    if (_selected >= 0 && _items)
    {
        return  (TabBarButton*)[_items objectAtIndex:_selected];
    }
    return nil;
}

- (void)setTabBarSelected:(int)nSelTag
{
    for (TabBarButton *btn in _items)
    {
        if (btn.tag == nSelTag)
        {
            [self click:btn];
            TabBarButton * sel = [self getSelectedItem];
            [sel setState:UIControlStateSelected];
        }
    }
}

- (void)clearCurSelected
{
    TabBarButton * sel = [self getSelectedItem];
    sel.currentSelected = NO;
    [sel setState:UIControlStateNormal];
    _selected   = -1;
}

-(void)layoutSubviews
{
    [self layoutButton];
}

-(void)layoutButton
{
    if ([_items count] <=0) return;
    if (_fixedSpace)
    {
        CGFloat step = self.frame.size.width / [_items count];
        CGFloat start = 0;
        for (UIView* view in _items)
        {
            CGRect frame = view.frame;
            view.frame = CGRectMake((int)( start + (step - frame.size.width)/2 +0.5 ),
                                    0,
                                    frame.size.width,
                                    frame.size.height);
            start += step;
            if ([_items indexOfObject:view] == 2) {
            }
        }
    }
    else {
        CGFloat gap, maxWidth = 0;
        for (UIView* view in _items)
        {
            if (view.frame.size.width > maxWidth) {
                maxWidth = view.frame.size.width;
            }
        }
        gap = (self.frame.size.width - maxWidth * [_items count])/([_items count] + 1);
        CGFloat start = gap;
        for (UIView* view in _items)
        {
            CGRect frame = view.frame;
            view.frame = CGRectMake((int)(start + (maxWidth - frame.size.width)/2 + 0.5),
                                    0,
                                    frame.size.width,
                                    frame.size.height);
            start += maxWidth + gap;
            if ([_items indexOfObject:view] == 2) {
            }
        }
    }
}


#pragma ================ mark click ===============
-(void)changeView:(TabBarButton *)sender
{
    NSInteger senderIndex = [_items indexOfObject:sender];
    if (_selected == senderIndex && sender.currentSelected){
        return;
    }
    //clear
    if (_selected >= 0){
        TabBarButton* lastBtn = (TabBarButton*)[_items objectAtIndex:_selected];
        [lastBtn setState:UIControlStateNormal];
    }
    _selected = [_items indexOfObject:sender];
}

-(void)click:(TabBarButton *)sender
{
    if (self.clickDelegate && [self.clickDelegate respondsToSelector:@selector(tabItemClicked:withPerSelected:)]){
        TabBarButton* perSeletedBtn = nil;
        perSeletedBtn = (_selected >= 0)? [_items objectAtIndex:_selected]:nil;
        NSInteger senderIndex = [_items indexOfObject:sender];
        if (senderIndex == 8) {
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseConnect) successBlock:^(id result) {
            }];
            HDClassMainViewController *controller = [[HDClassMainViewController alloc] init];
            [UIWindow changeWindowRootViewController:controller animated:YES];
            return;
        }
        if (senderIndex == _selected && sender.currentSelected) {
            return;
        }
//        if (sender.currentSelected) {
//            sender.currentSelected = NO;
//            TabBarButton* lastBtn = (TabBarButton*)[_items objectAtIndex:_selected];
//            [lastBtn setState:UIControlStateNormal];
//        }else{
            if (_selected >= 0){
                sender.currentSelected = YES;
                TabBarButton* lastBtn = (TabBarButton*)[_items objectAtIndex:_selected];
                [lastBtn setState:UIControlStateSelected];
            }else{
                _selected = 0;
                sender.currentSelected = YES;
                TabBarButton* lastBtn = (TabBarButton*)[_items objectAtIndex:_selected];
                [lastBtn setState:UIControlStateSelected];
            }
//        }
        [self.clickDelegate tabItemClicked:sender withPerSelected:perSeletedBtn];
    }
    [self changeView:sender];
}



- (void)dealloc
{
    self.items = nil;
    self.backgroundView = nil;
    _topShade = nil;
}

@end
