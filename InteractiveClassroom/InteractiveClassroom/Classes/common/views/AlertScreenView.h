//
//  AlertScreenView.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/22.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "FullScreenView.h"


@class AlertScreenView;

@protocol AlertScreenViewDelegate <NSObject>

- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag;


@end

@interface AlertScreenView : FullScreenView

@property (nonatomic, weak) id<AlertScreenViewDelegate> delegate;
@property (nonatomic, strong) UILabel *lblMessage;


/**
    左右2个按钮
 @param title
 @param messge
 @param delegate 可以不设置
 @param cancelTitle 取消
 @param otherTitle 其它按妞
 */
-(instancetype)initWithTitle:(NSString *)title message:(NSString *)messge delegate:(id)delegate cancelButtonTitle:(NSString *)cancelTitle otherButtonTitle:(NSString *)otherTitle;


/**
    单个按钮，只有确定
 @param title
 @param messge
 @param delegate 可以不设置
 @param sureTitle 确定按钮
 */
-(instancetype)initWithTitle:(NSString *)title message:(NSString *)messge delegate:(id)delegate sureButtonTitle:(NSString *)sureTitle;

@end
