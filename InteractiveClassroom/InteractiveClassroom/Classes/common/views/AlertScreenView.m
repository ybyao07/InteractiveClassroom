//
//  AlertScreenView.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/22.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "AlertScreenView.h"
#import "UIView+Extension.h"

@interface AlertScreenView()
@property (nonatomic, strong) NSString *title;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *cancelTitle;
@property (strong, nonatomic) NSString *otherTitle;

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *otherBtn;



@property (nonatomic, strong) UIButton *sureBtn;

@end

@implementation AlertScreenView
static const CGFloat topHeight = 50.f;
static const CGFloat leftMargin = 100.f;

-(instancetype)initWithTitle:(NSString *)title message:(NSString *)messge delegate:(id)delegate cancelButtonTitle:(NSString *)cancelTitle otherButtonTitle:(NSString *)otherTitle
{
    self = [super init];
    if (self) {
        _delegate = delegate;
        _title = title;
        _message = messge;
        _cancelTitle = cancelTitle;
        _otherTitle = otherTitle;
        [self addSubview:self.lblTitle];
        [self addSubview:self.lblMessage];
        [self addSubview:self.cancelBtn];
        [self addSubview:self.otherBtn];
    }
    return self;
}

-(instancetype)initWithTitle:(NSString *)title message:(NSString *)messge delegate:(id)delegate sureButtonTitle:(NSString *)sureTitle
{
    self = [super init];
    if (self) {
        _delegate = delegate;
        _title = title;
        _message = messge;
        _otherTitle = sureTitle;
        [self addSubview:self.lblTitle];
        [self addSubview:self.lblMessage];
        CGFloat _width = [UIScreen mainScreen].bounds.size.width - 100;;
        CGSize messageSize =[_message sizeWithFont:self.lblMessage.font constrainedToSize:CGSizeMake(_width - 2*leftMargin, CGFLOAT_MAX)];
        if (messageSize.height > 40) {
            self.lblMessage.textAlignment = NSTextAlignmentLeft;
        }
        [self addSubview:self.sureBtn];
     }
    return self;
}

#pragma mark -- accessor -----
- (UILabel *)lblTitle{
    if (!_lblTitle) {
        _lblTitle = [UILabel new];
        _lblTitle.textColor = JUIColorFromRGB_Main1;
        _lblTitle.font = JFONT_BOLD(JFONT_Size_H1);
        _lblTitle.text = _title;
        _lblTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _lblTitle;
}


- (UILabel *)lblMessage{
    if (!_lblMessage) {
        _lblMessage = [UILabel new];
        _lblMessage.numberOfLines = 0;
        _lblMessage.textColor = JUIColorFromRGB_Main4;
        _lblMessage.font = JFONT(JFONT_Size_H2);
        _lblMessage.text = _message;
        _lblMessage.textAlignment = NSTextAlignmentCenter;
    }
    return _lblMessage;
}

- (UIButton *)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [self buttonStyle:AlertBtnCancelStyle];
        [_cancelBtn setTitle:_cancelTitle forState:UIControlStateNormal];
        _cancelBtn.tag = 2;
        [_cancelBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}
- (UIButton *)otherBtn{
    if (!_otherBtn) {
        _otherBtn =  [self buttonStyle:AlertBtnSureStyle];
        [_otherBtn setTitle:_otherTitle forState:UIControlStateNormal];
        _otherBtn.tag = 1;
        [_otherBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _otherBtn;
}
- (UIButton *)sureBtn
{
    if (!_sureBtn) {
        _sureBtn =  [self buttonStyle:AlertBtnSureStyle];
        [_sureBtn setTitle:_otherTitle forState:UIControlStateNormal];
        _sureBtn.tag = 2;
        [_sureBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sureBtn;
}

-(void)buttonAction:(UIButton *)btn
{
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(alertScreenView:clickedButtonAtIndex:)]) {
        [self.delegate alertScreenView:self clickedButtonAtIndex:(btn.tag)];
    }
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat startY = 0.f;
    CGFloat _width = [UIScreen mainScreen].bounds.size.width - 500;;
    CGFloat _height = 0;
    if (_title.length) {
        startY += topHeight;
        _lblTitle.frame =  Rect(0, topHeight, _width, 50);
        startY += 50;
        startY += topHeight;
    }
    if (_message.length) {
        CGSize messageSize =[_message sizeWithFont:self.lblMessage.font constrainedToSize:CGSizeMake(_width - 2*leftMargin, CGFLOAT_MAX)];
        //上下留白
        if (messageSize.height < 50) {
            startY += 40.f;
        }else{
            startY += 20.f;
        }
        _lblMessage.frame = Rect(20.f, startY, _width - 2*leftMargin, messageSize.height);
        startY += messageSize.height;
        if (messageSize.height < 50) {
            startY += 40.f;
        }else{
            startY += 20.f;
        }
    }
    CGFloat btnWidth = (_width - leftMargin * 2 - 2 * 10)/2;
    if (_sureBtn) {  //单个按钮
        _sureBtn.frame =  Rect(leftMargin, startY, 60, 40);
        _sureBtn.centerX = self.centerX;
        _height = CGRectGetMaxY(_sureBtn.frame)+20;
    }else{  //两个按钮
        _cancelBtn.frame =  Rect(_width/2.0 + 10, startY, btnWidth, 40);
        _otherBtn.frame = Rect(leftMargin, startY, btnWidth, 40);
        _height = CGRectGetMaxY(_cancelBtn.frame)+20;
    }
    self.frame = Rect(self.origin.x, self.origin.y, _width, _height);
    self.center = CGPointMake(SCREEN_WIDTH/2.0f,
                              SCREEN_HEIGHT/2.0f);
}

@end
