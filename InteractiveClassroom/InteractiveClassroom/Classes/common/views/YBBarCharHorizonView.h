//
//  YBBarCharHorizonView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBBarCharHorizonView : UIView

@property (nonatomic, strong) NSMutableArray *values;
@property (nonatomic, strong) NSMutableArray *colors;
@property (nonatomic) float maxValue;
@property (nonatomic, strong) NSMutableArray *textIndicators;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic) float barHeight;
@property (nonatomic) float barMaxWidth;
@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGGradientRef gradient;

- (id)initWithFrame:(CGRect)frame
         startPoint:(CGPoint)startPoint
             values:(NSMutableArray *)values
             colors:(NSMutableArray *)colors
           maxValue:(float)maxValue
     textIndicators:(NSMutableArray *)textIndicators
          textColor:(UIColor *)textColor
          barHeight:(float)barHeight
        barMaxWidth:(float)barMaxWidth;
@end
