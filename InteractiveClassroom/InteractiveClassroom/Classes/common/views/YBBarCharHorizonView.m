//
//  YBBarCharHorizonView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "YBBarCharHorizonView.h"
@interface YBBarCharHorizonView()
@property (nonatomic) CGContextRef context;
@property (nonatomic, strong) NSMutableArray *textIndicatorsLabels;
@property (nonatomic, strong) NSMutableArray *digitIndicatorsLabels;
@end
@implementation YBBarCharHorizonView

- (id)initWithFrame:(CGRect)frame
         startPoint:(CGPoint)startPoint
             values:(NSMutableArray *)values
             colors:(NSMutableArray *)colors
           maxValue:(float)maxValue
     textIndicators:(NSMutableArray *)textIndicators
          textColor:(UIColor *)textColor
          barHeight:(float)barHeight
        barMaxWidth:(float)barMaxWidth
{
    self = [super initWithFrame:frame];
    if (self) {
        _values = values;
        _colors = colors;
        _maxValue = maxValue;
        _textIndicatorsLabels = [[NSMutableArray alloc] initWithCapacity:[values count]];
        _digitIndicatorsLabels = [[NSMutableArray alloc] initWithCapacity:[values count]];
        _textIndicators = textIndicators;
        _startPoint = startPoint;
        _textColor = textColor ? textColor : [UIColor orangeColor];
        _barHeight = barHeight;
        _barMaxWidth = barMaxWidth;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)setLabelDefaults:(UILabel *)label
{
    label.textColor = self.textColor;
    [label setTextAlignment:NSTextAlignmentLeft];
//    label.adjustsFontSizeToFitWidth = YES;
    //label.adjustsLetterSpacingToFitWidth = YES;
    label.backgroundColor = [UIColor clearColor];
}

- (void)drawRectangle:(CGRect)rect context:(CGContextRef)context color:(UIColor *)color
{
    CGContextSetFillColorWithColor(self.context,color.CGColor);
    CGContextFillRect(context, rect);
}

- (void)drawRect:(CGRect)rect
{
    self.context = UIGraphicsGetCurrentContext();
    int count = (int)[self.values count];
    float startx = self.startPoint.x;
    float starty = self.startPoint.y;
    float barMargin = 50;
    float marginOfBarAndDigit = 20;
    float marginOfTextAndBar = 8;
    float digitWidth = 50;
    float textWidth = 200;
    float textHeight = 34;
    for (int i = 0; i < count; i++) {
        //handle textlabel
        float textMargin_y = (i * (self.barHeight + barMargin)) + starty;
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(startx, textMargin_y, textWidth, self.barHeight)];
        textLabel.text = self.textIndicators[i];
        [self setLabelDefaults:textLabel];
        @try {
            UILabel *originalTextLabel = self.textIndicatorsLabels[i];
            if (originalTextLabel) {
                [originalTextLabel removeFromSuperview];
            }
        }
        @catch (NSException *exception) {
            [self.textIndicatorsLabels insertObject:textLabel atIndex:i];
        }
        [self addSubview:textLabel];
        
        //handle bar
        float barMargin_y = (i * (self.barHeight + barMargin)) + starty + textHeight;
        float v = [self.values[i] floatValue] <= self.maxValue ? [self.values[i] floatValue]: self.maxValue;
        float rate = v / self.maxValue;
        float barWidth = rate * self.barMaxWidth;
        CGRect barFrame = CGRectMake(startx, barMargin_y, barWidth, self.barHeight);
        [self drawRectangle:barFrame context:self.context color:self.colors[i]];
        //handle digitlabel
        UILabel *digitLabel = [[UILabel alloc] initWithFrame:CGRectMake(barFrame.origin.x + barFrame.size.width + marginOfBarAndDigit, barFrame.origin.y, digitWidth, barFrame.size.height)];
        digitLabel.text = self.values[i];
        digitLabel.textAlignment = NSTextAlignmentRight;
        [self setLabelDefaults:digitLabel];
        @try {
            UILabel *originalDigitLabel = self.digitIndicatorsLabels[i];
            if (originalDigitLabel) {
                [originalDigitLabel removeFromSuperview];
            }
        }
        @catch (NSException *exception) {
            [self.digitIndicatorsLabels insertObject:digitLabel atIndex:i];
        }
        [self addSubview:digitLabel];
    }
}

- (void)setValues:(NSMutableArray *)values
{
    for (int i = 0; i < [values count]; i++) {
        _values[i] = values[i];
    }
    [self setNeedsDisplay];
}


@end
