//
//  ResourcePageViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ResourcePageViewController.h"
#import "ResourceCell.h"
#import "PPTViewController.h"


#define Resource_CELL_WIDTH (SCREEN_WIDTH - 60*2 - 10*3)/4
//#define Resource_CELL_HEIGHT 160
@interface ResourcePageViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak) IBOutlet UICollectionView *myCollectionView;

@property (strong, nonatomic) NSMutableArray *resources;


@end

static CGFloat cellHeight = 0.f;
static CGFloat cellWidth = 0.f;

@implementation ResourcePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"ResourceCell" bundle:nil] forCellWithReuseIdentifier:@"ResourceCell"];
    [self.hd_navigationBar removeFromSuperview];
    self.view.backgroundColor = [UIColor clearColor];
    [self removeBackgourdImageView];
    cellHeight = (self.view.bounds.size.height - 80 - 20*2)/3;
    cellWidth =  (self.view.bounds.size.height - 10*3)/4;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
//    [self addTestData];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"data":@[
                                  @{
                                      @"fId":@"12",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"aaa",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      }, @{
                                      @"fId":@"12",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      },
                                  @{
                                      @"fId":@"12",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      },
                                  @{
                                      @"fId":@"12",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      }
                                  ]
                          };
    [self.resources removeAllObjects];
    NSArray *array = [ResourceModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.myCollectionView reloadData];
}


- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == MaterialList) {
        int materialType = [dic[@"materialType"] intValue];
        if ((materialType == ResourceTab_Courseware && self.pageType == ResourceTab_Courseware) || (materialType == ResourceTab_Materials && self.pageType == ResourceTab_Materials) || (materialType == ResourceTab_Upload && self.pageType == ResourceTab_Upload)) { //课件
            [self.resources removeAllObjects];
            NSArray *array = [ResourceModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.resources addObjectsFromArray:array];
            [self.myCollectionView reloadData];
        }
    }
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ResourceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ResourceCell" forIndexPath:indexPath];
    cell.resourceTag = self.pageType;
    cell.model = self.resources[indexPath.row];
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Resource_CELL_WIDTH, cellHeight);
}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 10, 0, 10);
//}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ResourceModel *resourceM = self.resources[indexPath.row];
    if ([resourceM.fileType isEqualToString:@"ppt"] || [resourceM.fileType isEqualToString:@"pptx"]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"fileId"] = resourceM.fId;
        [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(OpenMaterial) successBlock:^(id result) {
        }];
        PPTViewController *pptVC = [[PPTViewController alloc] initWithNibName:@"PPTViewController" bundle:nil];
        pptVC.fileId = resourceM.fId;
        pptVC.pptTitle = resourceM.documentName;
        pptVC.view.frame = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 500);
        [[JUITools currentRootController] pushViewController:pptVC animated:YES];
    }
}

#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
