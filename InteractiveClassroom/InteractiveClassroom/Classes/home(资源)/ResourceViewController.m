//
//  ResourceViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ResourceViewController.h"
#import "ResourceCell.h"
#import "UIView+Extension.h"
#import "ResourcePageViewController.h"
#import "CocoaAsyncSocket.h"
#import "HDNetTools.h"

@interface ResourceViewController ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate, GCDAsyncSocketDelegate>

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle; // 标题

@property(strong, nonatomic) NSMutableArray *pages;
@property(strong, nonatomic) UIPageViewController *pageController;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) UIButton *currentSelectedBtn;

@property (nonatomic,assign) NSInteger counter;

@end

@implementation ResourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = JUIColorFromRGB_bg;
    self.hd_navigationBar.hidden = YES;
    [self initUI];
    [self setPageViewController];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOrCloseView:) name:kOpenOrCloseViewNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self showCoverView];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lblTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"chapterName"];
}
- (void)initUI
{
    [self.buttons enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setBackgroundColor:[UIColor clearColor]];
        [obj setTitleColor:JUIColorFromRGB_ButtonSelected forState:UIControlStateNormal];
        [obj setBackgroundImage:[UIImage imageNamed:@"icon_btn_unSelected"] forState:UIControlStateNormal];
        [obj setTitleColor:JUIColorFromRGB_White forState:UIControlStateSelected];
        [obj setBackgroundImage:[UIImage imageNamed:@"icon_btn_selected"] forState:UIControlStateSelected];
    }];
    self.firstBtn.selected = YES;
    self.currentSelectedBtn = self.firstBtn;
}
- (void)setPageViewController {
    [self addOrderPage];
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [self.pageController setDelegate:self];
    self.pageController.view.center = CGPointMake(self.view.center.x, self.view.center.y - 40);
    self.pageController.view.bounds = CGRectMake(0, 0, self.view.frame.size.width - 2*60, SCREEN_HEIGHT - 70 - 360);
    NSArray *viewControllers = [NSArray arrayWithObject:[self.pages objectAtIndex:0]];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (void)addOrderPage {
    _pages = [NSMutableArray array];
    for (NSInteger i = 0; i < 4; i++) {
        ResourcePageViewController *page = [[ResourcePageViewController alloc] init];
        switch (i) {
            case 0:
                page.pageType = ResourceTab_Courseware;
                break;
            case 1:
                page.pageType = ResourceTab_Materials;
                break;
            case 2:
                page.pageType = ResourceTab_Upload;
                break;
            case 3:
                page.pageType = ResourceTab_Recommand;
                break;
            default:
                page.pageType = ResourceTab_Courseware;
                break;
        }
        [_pages addObject:page];
    }
}
- (IBAction)typeBtnAction:(UIButton *)sender {
    [self.buttons enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (sender.tag == obj.tag) {
            obj.selected = YES;
        }else{
            obj.selected = NO;
        }
    }];
    self.currentSelectedBtn = sender;
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    switch (sender.tag) {
        case 0: // 课件
        {
            dic[@"materialType"] = @(2);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
                
            }];
        }
            break;
        case 1: // 素材
        {
            dic[@"materialType"] = @(3);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
                
            }];
        }
            break;
        case 2: // 我的上传
        {
            dic[@"materialType"] = @(5);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
            }];
        }
            break;
        case 3:
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"敬请期待" message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }
            break;
        default:
            break;
    }
    
    NSArray *toViewController = [NSArray arrayWithObject:[self.pages objectAtIndex:sender.tag]];
    [_pageController setViewControllers:toViewController direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}


- (IBAction)closeAction:(UIButton *)sender {
    [self showCoverView];
    [[NSNotificationCenter defaultCenter] postNotificationName:kMainClassOpenOrCloseViewNotification object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
}


- (void)startAction{
    [self removeCoverView];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
    }];
}

- (void)openOrCloseView:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    NSString *vcName = dic[@"controllerName"];
    int open = [dic[@"currentSelected"] intValue];
    if ([vcName isEqualToString:@"eMainControlTabbarTag_Resource"]) {
        if (open == 1) {
            [self startAction];
        }else{//close
            [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
            }];
            [self showCoverView];
        }
    }
}



- (void)hideView:(UIView *)parentView
{
    if (parentView.subviews.count > 0) {
        for (UIView *view in parentView.subviews) {
            view.hidden = YES;
            [self hideView:view];
        }
    }
}

#pragma mark - UIPageViewControllerDataSource & UIPageViewControllerDelegate
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger currentIndex = [self.pages indexOfObject:viewController];    // get the index of the current view controller on display
    // check if we are at the end and decide if we need to present the next viewcontroller
    if (currentIndex < [self.pages count] - 1) {
        return [self.pages objectAtIndex:currentIndex + 1];                   // return the next view controlle
    } else {
        return nil;                                                         // do nothing
    }
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger currentIndex = [self.pages indexOfObject:viewController];    // get the index of the current view controller on display
    // check if we are at the beginning and decide if we need to present the previous viewcontroller
    if (currentIndex > 0) {
        return [self.pages objectAtIndex:currentIndex - 1];                   // return the previous viewcontroller
    } else {
        return nil;                                                         // do nothing
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
