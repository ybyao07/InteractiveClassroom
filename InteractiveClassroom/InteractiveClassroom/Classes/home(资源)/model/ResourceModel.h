//
//  ResourceModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResourceModel : NSObject

@property (nonatomic, copy) NSString *fId;
@property (nonatomic, copy) NSString *fileType; //文件类型
@property (nonatomic, copy) NSString *documentName;// 文件名称
//@property (nonatomic, copy) NSString *fileUrl;
//@property (nonatomic, copy) NSString *documentExplain; //文档说明
@property (nonatomic, assign) BOOL isTeach;
@property (nonatomic, copy) NSString *docSource;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *collectionTime; //收藏时间
@property (nonatomic, copy) NSString *uploadTime; //收藏时间




@end
