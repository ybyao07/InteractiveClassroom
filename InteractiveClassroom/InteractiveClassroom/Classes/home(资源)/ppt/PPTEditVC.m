//
//  PPTEditVC.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/14.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTEditVC.h"
#import "HBDrawingBoard.h"

@interface PPTEditVC ()<HBDrawingBoardDelegate>
@property (nonatomic, strong) HBDrawingBoard *drawView;
@end

@implementation PPTEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.drawView];
}
#pragma mark   ==================  HBDrawingBoardDelegate =========
- (void)drawBoard:(HBDrawingBoard *)drawView drawingStatus:(HBDrawingStatus)drawingStatus model:(HBDrawModel *)model{
    NSLog(@"%@",model.mj_keyValues);
}
- (void)closeBoard
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (HBDrawingBoard *)drawView
{
    if (!_drawView) {
        _drawView = [[HBDrawingBoard alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _drawView.delegate = self;
        _drawView.backImage.image = [UIImage imageWithData:self.imgData];
    }
    return _drawView;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
