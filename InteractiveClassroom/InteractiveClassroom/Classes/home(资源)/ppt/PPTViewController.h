//
//  PPTViewController.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

@interface PPTViewController : YBaseViewController

@property (nonatomic, copy) NSString *fileId;
@property (nonatomic, copy) NSString *pptTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@end
