//
//  PPTViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTViewController.h"
//#import "PPTFullScreenView.h"
#import "PPTModel.h"
#import "PPTThumbModel.h"
#import "PPTIconCell.h"
#import "PPTEditVC.h"

@interface PPTViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    //缩略图
    NSDictionary *dicJson;
    NSData *thumbData;
    NSMutableData *multiData;
    long multiJsonLength;
    NSArray *thumbSizes;
    //大图
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollBigView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIButton *preBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblPagehint;

@property (nonatomic, assign) NSInteger totalPage;
@property (nonatomic, assign) NSInteger currentIndex;

@property (strong, nonatomic) PPTModel *pptM;

@property (strong, nonatomic) NSMutableArray <PPTThumbModel *> *arrIcons;

//@property (strong, nonatomic) UIImageView *currentImageView;
@property (strong, nonatomic) IBOutlet UIImageView *showImageView;

@property (strong, nonatomic) NSData *imgData;





@end

static CGFloat scrollWidth = 600;
static CGFloat scrollHeight = 500;
@implementation PPTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"资源";
    self.hd_navigationBar.hidden = YES;
//    self.scrollBigView.pagingEnabled = YES;
    self.view.frame = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 500);
    scrollWidth = SCREEN_WIDTH - 160 - 20 -  20 - 20;
    scrollHeight = [[UIScreen mainScreen] bounds].size.height*560/750;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocketStream object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocketPPTStream object:nil];
    [self.collectionView registerNib:[UINib nibWithNibName:@"PPTIconCell" bundle:nil] forCellWithReuseIdentifier:@"PPTIconCell"];
    _containerView.bounds = Rect(0, 0, scrollWidth, scrollHeight);
    [self showLoadAnimation];
    self.currentIndex = 1;
    _lblTitle.text = self.pptTitle;
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"fileId":@"",
                          @"documentName":@"",
                          @"fileType":@"",
                          @"totalPage":@(10),
                          @"currentPage":@(1),
                          @"icons":
                                @[
                                  [UIImage imageNamed:@"icon_home_5_call"],
                                  [UIImage imageNamed:@"icon_home_6_answer"],
                                  [UIImage imageNamed:@"icon_home_5_call"],
                                  [UIImage imageNamed:@"icon_home_6_answer"],
                                  [UIImage imageNamed:@"icon_home_5_call"],
                                  [UIImage imageNamed:@"icon_home_6_answer"],
                                  [UIImage imageNamed:@"icon_home_5_call"],
                                  [UIImage imageNamed:@"icon_home_6_answer"],
                                  [UIImage imageNamed:@"icon_home_5_call"],
                                  [UIImage imageNamed:@"icon_home_6_answer"],
                                  ]
                          };
    self.pptM = [PPTModel mj_objectWithKeyValues:dic];
    [self.pptM.icons enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PPTThumbModel *thumbM = [[PPTThumbModel alloc] init];
        thumbM.img = obj;
        thumbM.isSelected = NO;
        [self.arrIcons addObject:thumbM];
    }];
    self.currentIndex = 1;
}
//================= 缩略图 ===========
- (void)refreshOnSocketData:(NSNotification *)notif
{
    HDLog(@" ====== 缩略图回调 ==========");
    NSDictionary *dicOrigin = notif.userInfo;
    NSData *data = dicOrigin[@"data"];
    // ======= 整合数据，显示 ======
    // pptinfo0x:xxxx
    // pptinfo1x:数据
    // pptinfo2x:数据
    // pptinfo3x:xxxx数据
    Byte *dataByte = (Byte *)[data bytes];
    Byte judgeByte[1],pageByte[1];
    memcpy(&judgeByte, &dataByte[7], 1);
    long status = judgeByte[0]&0xFF;
    if (status == 3) {
        //先Json再Thumb
        Byte jsonFourByte[4];
        memcpy(&jsonFourByte, &dataByte[9], 4);
        long jsonLength = [self heightBytesToInt:jsonFourByte];
        NSData *jsonData = [data subdataWithRange:NSMakeRange(13, jsonLength)];
        dicJson = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
        thumbData = [data subdataWithRange:NSMakeRange(13 + jsonLength , data.length - jsonLength - 13)];
        thumbSizes =  dicJson[@"icons"];
    }else if (status == 0){
        Byte jsonFourByte[4];
        memcpy(&jsonFourByte, &dataByte[9], 4);
        multiJsonLength = [self heightBytesToInt:jsonFourByte];
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(13, data.length - 13)]];
//        [multiData appendData:data];
    }else if (status == 1){
        NSData *pptData = [data subdataWithRange:NSMakeRange(0,6)];
        NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
        NSLog(@"======= pptString is %@",pptStr);
        [multiData appendData:[data subdataWithRange:NSMakeRange(9 , data.length - 9)]];
    }else if (status == 2){
        [multiData appendData:[data subdataWithRange:NSMakeRange(9 , data.length - 9)]];
        NSData *jsonData = [multiData subdataWithRange:NSMakeRange(0, multiJsonLength)];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"======= jsonString is %@",jsonString);
        dicJson = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
        thumbData = [multiData subdataWithRange:NSMakeRange(multiJsonLength , multiData.length - multiJsonLength)];
        thumbSizes =  dicJson[@"icons"];
    }
    if ([dicJson[@"code"] integerValue] == MaterialInfo) {
//        [self removeLoadAnimation];
        PPTModel *model = [PPTModel mj_objectWithKeyValues:dicJson];
        self.pptM = model;
        __block long long offsetData = 0;
        [self.arrIcons removeAllObjects];
        [thumbSizes enumerateObjectsUsingBlock:^(NSNumber *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (thumbData.length >= (offsetData + [obj integerValue])) {
                NSData *data = [thumbData subdataWithRange:NSMakeRange(offsetData,[obj integerValue])];
                PPTThumbModel *thumbM;
                if (idx == 0) { //默认第一个选中
                    thumbM = [PPTThumbModel initWithImage:data selected:YES];
                }else{
                    thumbM = [PPTThumbModel initWithImage:data selected:NO];
                }
                [self.arrIcons addObject:thumbM];
                offsetData =  offsetData + [obj integerValue];
            }
        }];
        [self.collectionView reloadData];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"fileId"] = self.fileId;
        dic[@"currentPage"] = @(self.currentIndex);
        [self showLoadAnimation];
        [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
        }];
        self.nextBtn.enabled = YES;
        if (self.currentIndex == 1) {
            self.preBtn.enabled = NO;
        }else{
            self.preBtn.enabled = YES;
        }
    }
}

// ====================== 大图 ==================
- (void)refreshViewSockentData:(NSNotification *)notif
{
    HDLog(@" ====== 大图回调 ==========");
    NSDictionary *dicOrigin = notif.userInfo;
    NSData *data = dicOrigin[@"data"];
    // ======= 整合数据，显示 ======
    // pptpic0x:数据   0
    Byte *dataByte = (Byte *)[data bytes];
    Byte judgeByte[1],pageByte[1];
    memcpy(&judgeByte, &dataByte[6], 1);
    memcpy(&pageByte, &dataByte[7], 1);
    long status = judgeByte[0]&0xFF;
    if (status == 3) {
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
        int page = pageByte[0]&0xFF;
        self.currentIndex = page;
        _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
        self.imgData = multiData;
        self.showImageView.image = [UIImage imageWithData:multiData];
        [self removeLoadAnimation];
    }else if (status == 0){
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
    }else if (status == 1){
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
    }else if (status == 2){
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
        int page = pageByte[0]&0xFF;
        self.currentIndex = page;
        _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
        self.imgData = multiData;
        self.showImageView.image = [UIImage imageWithData:multiData];
        [self removeLoadAnimation];
    }
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrIcons.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPTIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PPTIconCell" forIndexPath:indexPath];
    cell.model = self.arrIcons[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, 120);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeModelWithIndex:indexPath.row];    
}

- (void)changeModelWithIndex:(NSInteger)index {
    for (PPTThumbModel *model in self.arrIcons) {
        model.isSelected = NO;
    }
    PPTThumbModel *model = [self.arrIcons objectAtIndex:index];
    self.currentIndex = index + 1;
    model.isSelected = YES;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.nextBtn.enabled = YES;
    if (self.currentIndex == 1) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
    [self.collectionView reloadData];
}

- (IBAction)preAction:(UIButton *)sender {
    if (self.currentIndex <= 1) {
        return;
    }
    self.currentIndex--;
    _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.nextBtn.enabled = YES;
    if (self.currentIndex == 1) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
}
- (IBAction)nextAction:(UIButton *)sender {
    if (self.currentIndex == [self.pptM.totalPage integerValue]) {
        return;
    }
    self.currentIndex++;
    _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.preBtn.enabled = YES;
    if (self.currentIndex == [self.pptM.totalPage integerValue]) {
        self.nextBtn.enabled = NO;
    }else{
        self.nextBtn.enabled = YES;
    }
}

- (IBAction)edit:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(OpenSign) successBlock:^(id result) {
    }];
//    PPTFullScreenView *fullView = [[PPTFullScreenView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
//    fullView.bigImgView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:self.imgData]];
    PPTEditVC *pptEdit = [[PPTEditVC alloc] init];
    pptEdit.imgData = self.imgData;
    [self presentViewController:pptEdit animated:YES completion:nil];
//    [[JUITools currentRootController].view addSubview:self.drawView];
}
- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {        
    }];
    [self.navigationController popViewControllerAnimated:YES];
}




-(long)heightBytesToInt:(Byte[]) byte
{
    long one = byte[0]&0xFF;
    long two = byte[1]&0xFF;  // 03
    long twoHext = two * 16 * 16;
    
    long three = byte[2]&0xFF;
    long threeHex = three * 16 * 16 * 16;
    
    long four = byte[3]&0xFF;
    long fourHex = four * 16 * 16 * 16 * 16;
    long total = fourHex + threeHex +  twoHext + one;
    return total;
}

- (NSMutableArray *)arrIcons
{
    if (!_arrIcons) {
        _arrIcons = [NSMutableArray array];
    }
    return _arrIcons;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
