//
//  PPTIconCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/30.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTIconCell.h"

@implementation PPTIconCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(PPTThumbModel *)model
{
    _model = model;
    _imgView.image = model.img;
    if (model.isSelected) {
        _imgView.layer.borderWidth = 1;
        _imgView.layer.borderColor = JUIColorFromRGB_Major3.CGColor;
    }else{
        _imgView.layer.borderWidth = 1;
        _imgView.layer.borderColor = JUIColorFromRGB_line.CGColor;
    }
}

@end
