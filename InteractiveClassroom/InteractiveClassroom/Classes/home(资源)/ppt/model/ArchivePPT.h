//
//  ArchivePPT.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/23.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArchivePPT : NSObject<NSCopying, NSCoding>

@property (nonatomic, assign) NSInteger indexColor;
@property (assign, nonatomic) NSInteger indexWidth;
@property (assign, nonatomic) NSInteger indexErazorWidth;
@property (assign, nonatomic) BOOL pen;

@end
