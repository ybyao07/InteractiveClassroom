//
//  ArchivePPT.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/23.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ArchivePPT.h"

#define kLineColor @"kLineColor"
#define kLineWidth @"kLineWidth"
#define kLineErazorWidth @"kLineErazorWidth"
#define kPen @"kPen"

@implementation ArchivePPT

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:_indexColor forKey:kLineColor];
    [aCoder encodeInteger:_indexWidth forKey:kLineWidth];
    [aCoder encodeInteger:_indexErazorWidth forKey:kLineErazorWidth];
    [aCoder encodeBool:_pen forKey:kPen];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _indexColor = [aDecoder decodeIntegerForKey:kLineColor];
        _indexWidth = [aDecoder decodeIntegerForKey:kLineWidth];
        _indexErazorWidth = [aDecoder decodeIntegerForKey:kLineErazorWidth];
        _pen = [aDecoder decodeBoolForKey:kPen];
    }
    return self;
}

#pragma mark - NSCoping
- (id)copyWithZone:(NSZone *)zone {
    ArchivePPT *copy = [[[self class] allocWithZone:zone] init];
    copy.indexColor = self.indexColor ;
    copy.indexWidth = self.indexWidth;
    copy.indexErazorWidth = self.indexErazorWidth;
    copy.pen = self.pen;
    return copy;
}

@end
