//
//  PPTModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDBaseModel.h"

@interface PPTModel : HDBaseModel

@property (nonatomic, copy) NSString *fileId;
@property (nonatomic, copy) NSString *documentName;
@property (nonatomic, copy) NSString *fileType;
@property (strong, nonatomic) NSNumber *totalPage;
@property (strong, nonatomic) NSNumber *currentPage;
@property (strong, nonatomic) NSData *img;  //当前页面的image

@property (nonatomic, strong) NSArray<UIImage *> *icons; //缩略图

@end
