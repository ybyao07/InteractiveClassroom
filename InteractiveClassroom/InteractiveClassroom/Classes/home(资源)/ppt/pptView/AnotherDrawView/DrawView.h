//
//  DrawView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawView : UIView
{
    CGMutablePathRef path;
    NSMutableArray *pathModalArray;
}
@property (nonatomic,strong) UIColor *lineColor;
@property (nonatomic,assign) CGFloat lineWidth;

- (void)undoAction;
- (void)clearAction;

@property (nonatomic, strong) UIImageView *bigImgView;

@end
