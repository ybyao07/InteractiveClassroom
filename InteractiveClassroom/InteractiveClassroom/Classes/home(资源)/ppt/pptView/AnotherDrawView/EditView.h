//
//  EditView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ColorSelectBlock)(UIColor *color);
typedef void (^WidthSelectBlock)(CGFloat width);
typedef void (^OtherSelectBlock)(void);

@interface EditView : UIView
{
    UIView *colorView;
    UIView *widthView;
    
    NSDictionary *_colorDic;    //可以用数组来实现
    
    UIImageView *bgImageView;
    UIImageView *widthBgImageView;
    UIImageView *colorBgImageView;
    
}

@property (nonatomic,copy) ColorSelectBlock colorBlock;
@property (nonatomic,copy) WidthSelectBlock widthBlock;
@property (nonatomic,copy) OtherSelectBlock eraserBlock;
@property (nonatomic,copy) OtherSelectBlock undoBlock;
@property (nonatomic,copy) OtherSelectBlock clearBlock;

@end
