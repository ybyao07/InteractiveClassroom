 //
//  HBDrawingBoard.m
//  DemoAntiAliasing
//
//  Created by 伍宏彬 on 15/11/2.
//  Copyright (c) 2015年 HB. All rights reserved.
//

#import "HBDrawingBoard.h"
#import "UIView+Extension.h"
#import "HBBackImageBoard.h"
#import "UIColor+help.h"
#import "HBDrawPoint.h"
#import "MJExtension.h"
#import "HBDrawSettingBoard.h"
#import "ZXCustomWindow.h"
#import "NSFileManager+Helper.h"
#import "PPTSocketClient.h"
#import "ArchivePPT.h"

@interface HBDrawingBoard()
@property (nonatomic, strong) NSMutableArray *paths;
@property (nonatomic, strong) NSMutableArray *tempPoints;
@property (nonatomic, strong) NSMutableArray *tempPath;
@property (nonatomic, strong) UIImageView *drawImage;
@property (nonatomic, strong) HBDrawView *drawView;
@property (nonatomic, strong) HBDrawSettingBoard *settingBoard;

@property (nonatomic, strong) UIColor *lineColor;
@property (strong, nonatomic) UIColor *lastColor;
@property (assign, nonatomic) NSInteger lineWidth;
@property (assign, nonatomic) NSInteger lineErazorWidth;

@property (nonatomic, assign) NSInteger colorIndex;
@property (assign, nonatomic) setType currentType;

@end
#define ThumbnailPath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"HBThumbnail"]

@implementation HBDrawingBoard

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backImage];
        [self addSubview:self.drawImage];
        [self.drawImage addSubview:self.drawView];
        [self addSubview:self.settingBoard];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeColorWidth:) name:SendColorAndWidthNotification object:nil];
        [self initLineValues];
    }
    return self;
}

- (void)initLineValues
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [path stringByAppendingPathComponent:@"ppt.data"];
    ArchivePPT *archieve = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    if (archieve == nil) {
    _lineWidth = 8;
    _lineErazorWidth = 10;
    _lineColor = [UIColor redColor];
    }
}

- (void)changeColorWidth:(NSNotification *)notif
{
    NSDictionary *dicNotif = notif.userInfo;
    if (dicNotif[@"lineColor"] != nil) {
        _lineColor = dicNotif[@"lineColor"];
        _colorIndex = [dicNotif[@"colorIndex"] intValue];
    }
    if (dicNotif[@"lineWidth"] != nil) {
        _lineWidth = [dicNotif[@"lineWidth"] intValue];
    }
    if (dicNotif[@"lineErazorWidth"] != nil) {
        _lineErazorWidth = [dicNotif[@"lineErazorWidth"] intValue];
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"penThickness"] = @(_lineWidth);
    dic[@"penColor"] = [UIColor hexStringFromColor:_lineColor];
    dic[@"eraseThickness"] = @(_lineErazorWidth);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(SignSettings) successBlock:^(id result) {
    }];
}



#pragma mark - Public_Methd
- (BOOL)drawWithPoints:(HBDrawModel *)model{
    self.userInteractionEnabled = NO;
    //比值
    CGFloat xPix = ([UIScreen mainScreen].bounds.size.width * [UIScreen mainScreen].scale);
    CGFloat yPix = ([UIScreen mainScreen].bounds.size.height * [UIScreen mainScreen].scale);
    CGFloat xp = model.width.floatValue / xPix;
    CGFloat yp = model.height.floatValue / yPix;
    HBDrawPoint *point = [model.pointList firstObject];
    HBPath *path = [HBPath pathToPoint:CGPointMake(point.x.floatValue * xp , point.y.floatValue * yp) pathWidth:model.paintSize.floatValue isEraser:model.isEraser.boolValue];
    path.pathColor = [UIColor colorWithHexString:model.paintColor];
    [self.paths addObject:path];
    NSMutableArray *marray = [model.pointList mutableCopy];
    [marray removeObjectAtIndex:0];
    [marray enumerateObjectsUsingBlock:^(HBDrawPoint *point, NSUInteger idx, BOOL *stop) {
        [path pathLineToPoint:CGPointMake(point.x.floatValue * xp , point.y.floatValue * yp) WithType:HBDrawingShapeCurve];
        [self.drawView setBrush:path];
    }];
    self.userInteractionEnabled = YES;
    return YES;
}
+ (HBDrawModel *)objectWith:(NSDictionary *)dic
{
    return [HBDrawModel mj_objectWithKeyValues:dic];
}

#pragma mark - CustomMethd
- (CGPoint)getTouchSet:(NSSet *)touches{
    UITouch *touch = [touches anyObject];
    return [touch locationInView:self];
}

#pragma mark - Touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [self getTouchSet:touches];
    [self sendPoint:point operation:0];
    HBPath *path;
    if (self.ise) {
        path = [HBPath pathToPoint:point pathWidth:_lineErazorWidth isEraser:self.ise];
    }else{
        path = [HBPath pathToPoint:point pathWidth:_lineWidth isEraser:self.ise];
    }
    path.pathColor = _lineColor;
    path.imagePath = [NSString stringWithFormat:@"%@.png",[self getTimeString]];
    [self.paths addObject:path];
    [self.tempPoints addObject:[HBDrawPoint drawPoint:point]];
    if ([self.delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)]) {
        [self.delegate drawBoard:self drawingStatus:HBDrawingStatusBegin model:nil];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [self getTouchSet:touches];
    [self sendPoint:point operation:1];
    HBPath *path = [self.paths lastObject];
    [path pathLineToPoint:point WithType:self.shapType];
    if (self.ise) {
        [self setEraseBrush:path];
    }else{
        [self.drawView setBrush:path];
    }
    [self.tempPoints addObject:[HBDrawPoint drawPoint:point]];
    if ([self.delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)]) {
        [self.delegate drawBoard:self drawingStatus:HBDrawingStatusMove model:nil];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
    CGPoint point = [self getTouchSet:touches];
    [self sendPoint:point operation:2];
    HBPath *path = [self.paths lastObject];
    UIImage *image = [self screenshot:self.drawImage];
    self.drawImage.image = image;
    [self.drawView setBrush:nil];
    NSData *imageData = UIImagePNGRepresentation(image);//UIImageJPEGRepresentation(image, 0.4);
    NSString *filePath = [ThumbnailPath stringByAppendingPathComponent:path.imagePath];
    BOOL isSave = [NSFileManager hb_saveData:imageData filePath:filePath];
    if (isSave) {
//        NSLog(@"%@", [NSString stringWithFormat:@"保存成功: %@",filePath]);
    }
    HBDrawModel *model = [[HBDrawModel alloc] init];
    model.paintColor = [_lineColor toColorString];
    model.paintSize = @(_lineWidth);
    model.isEraser = [NSNumber numberWithBool:path.isEraser];
    model.pointList = self.tempPoints;
    model.shapType = [NSNumber numberWithInteger:self.shapType];
    if ([self.delegate respondsToSelector:@selector(drawBoard:drawingStatus:model:)]) {
        [self.delegate drawBoard:self drawingStatus:HBDrawingStatusEnd model:model];
    }
    //清空
    [self.tempPoints removeAllObjects];
}


- (void)sendPoint:(CGPoint)point operation:(int)oper
{
    NSMutableData *data = [NSMutableData data];
    [data appendData:[NSData dataWithBytes:&oper length:1]];
    //分辨率x,y
    CGRect rect_screen = [[UIScreen mainScreen] bounds];
    CGSize size_screen = rect_screen.size;
//    CGFloat scale_screen = [UIScreen mainScreen].scale;
    int width = (int)size_screen.width;
//    width=ntohl(width);
    [data appendData:[NSData dataWithBytes:&width length:4]];
    int heith = (int)size_screen.height;
//    heith=ntohl(heith);
    [data appendData:[NSData dataWithBytes:&heith length:4]];
    //8位x,y坐标
    CGFloat x = point.x , y = point.y ;
    [data appendData:[NSData dataWithBytes:&x length:8]];
    [data appendData:[NSData dataWithBytes:&y length:8]];
    [[PPTSocketClient shareInstance] sendSynData:data];
}


- (void)setEraseBrush:(HBPath *)path{
    UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0);
    [self.drawImage.image drawInRect:self.bounds];
    [[UIColor clearColor] set];
    path.bezierPath.lineWidth = _lineErazorWidth;
    [path.bezierPath strokeWithBlendMode:kCGBlendModeClear alpha:1.0];
    [path.bezierPath stroke];
    self.drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}
- (UIImage *)screenshot:(UIView *)shotView{
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [shotView.layer renderInContext:context];
    UIImage *getImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return getImage;
}
- (NSString *)getTimeString{
    NSDateFormatter  *dateformatter = nil;
    if (!dateformatter) {
        dateformatter = [[NSDateFormatter alloc] init];
    }
    [dateformatter setDateFormat:@"YYYYMMddHHmmssSSS"];
    return [dateformatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:0]];
}
#pragma mark - Lazying
- (NSMutableArray *)paths{
    if (!_paths) {
        _paths = [NSMutableArray array];
    }
    return _paths;
}
- (NSMutableArray *)tempPoints{
    if (!_tempPoints) {
        _tempPoints = [NSMutableArray array];
    }
    return _tempPoints;
}
- (NSMutableArray *)tempPath{
    if (!_tempPath) {
        _tempPath = [NSMutableArray array];
    }
    return _tempPath;
}
- (void)setShapType:(HBDrawingShapeType)shapType{
    if (self.ise) {
        return;
    }
    _shapType = shapType;
}

- (void)setLineColor:(UIColor *)lineColor
{
    _lineColor = lineColor;
}
- (void)setLineWidth:(NSInteger)lineWidth
{
    _lineWidth = lineWidth;
}
- (UIImageView *)backImage
{
    if (!_backImage) {
        _backImage = [[UIImageView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-90)];
    }
    return _backImage;
}
- (UIImageView *)drawImage
{
    if (!_drawImage) {
        _drawImage = [[UIImageView alloc] initWithFrame:self.bounds];
        _drawImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _drawImage;
}
- (HBDrawView *)drawView{
    if (!_drawView) {
        _drawView = [HBDrawView new];
        _drawView.backgroundColor = [UIColor clearColor];
        _drawView.frame = self.bounds;
    }
    return _drawView;
}
- (HBDrawSettingBoard *)settingBoard
{
    if (!_settingBoard) {
        _settingBoard = [[[NSBundle mainBundle] loadNibNamed:@"HBDrawSettingBoard" owner:nil options:nil] firstObject];
        _settingBoard.frame = CGRectMake(0,self.frame.size.height - 90, [UIScreen mainScreen].bounds.size.width, 90);
        __weak typeof(self) weakSelf = self;
        weakSelf.ise = NO;
        _settingBoard.settingBlock  = ^(setType type) {
            switch (type) {
                case setTypePen:
                {
                    weakSelf.currentType = setTypePen;
                    if (weakSelf.ise) { // 从橡皮擦过来的
                        weakSelf.ise = NO;
                        weakSelf.lineColor = weakSelf.lastColor;
                        [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(0)} commandCode:@(SignOperation) successBlock:^(id result) {
                        }];
                    }else{
                        weakSelf.ise = NO;
                        weakSelf.shapType = HBDrawingShapeCurve;
                        [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(0)} commandCode:@(SignOperation) successBlock:^(id result) {
                        }];
                    }
                }
                    break;
                case setTypeSave:
                {
                    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(4)} commandCode:@(SignOperation) successBlock:^(id result) {
                    }];
                }
                    break;
                case setTypeEraser:
                {
                    weakSelf.currentType = setTypeEraser;
                    if (!weakSelf.ise) {
                        //保存上次绘制状态
                    //设置橡皮擦属性
                    weakSelf.lastColor = weakSelf.lineColor;
                    weakSelf.lineColor = [UIColor clearColor];
                    weakSelf.ise = YES;
                    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(1)} commandCode:@(SignOperation) successBlock:^(id result) {
                    }];
                    }
                }
                    break;
                case setTypeBack:
                {
                    if (weakSelf.paths.count == 0) {
                        NSLog(@"已经最后一张了，不能撤退了");
                        return;
                    }
                    HBPath *lastpath = [weakSelf.paths lastObject];
                    [weakSelf.tempPath addObject:lastpath];
                    [weakSelf.paths removeLastObject];
                    HBPath *path = [weakSelf.paths lastObject];
                    UIImage *getImage = [NSFileManager hb_getImageFileName:[ThumbnailPath stringByAppendingPathComponent:path.imagePath]];
                    weakSelf.drawImage.image = getImage;
                    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(2)} commandCode:@(SignOperation) successBlock:^(id result) {
                    }];
                }
                    break;
                case setTyperegeneration:
                {
                    if (weakSelf.tempPath.count == 0) {
                        NSLog(@"已经最新一张了，不能回滚了");
                        return;
                    }
                    HBPath *lastpath = [weakSelf.tempPath lastObject];
                    [weakSelf.paths addObject:lastpath];
                    [weakSelf.tempPath removeLastObject];
                    HBPath *path = [weakSelf.paths lastObject];
                    UIImage *getImage = [NSFileManager hb_getImageFileName:[ThumbnailPath stringByAppendingPathComponent:path.imagePath]];
                    weakSelf.drawImage.image = getImage;
                }
                    break;
                case setTypeClearAll:
                {
                    [weakSelf.paths removeAllObjects];
                    [weakSelf.tempPath removeAllObjects];
                    [weakSelf.tempPoints removeAllObjects];
                    [NSFileManager deleteFile:ThumbnailPath];
                    weakSelf.drawImage.image = nil;
                    [HDSocketSendCommandTool sendSocketParam:@{@"operation":@(3)} commandCode:@(SignOperation) successBlock:^(id result) {
                    }];
                }
                    break;
                case setTypeClose:
                {
                    [weakSelf.paths removeAllObjects];
                    [weakSelf.tempPath removeAllObjects];
                    [weakSelf.tempPoints removeAllObjects];
                    [NSFileManager deleteFile:ThumbnailPath];
                    weakSelf.drawImage.image = nil;
                    //保存画笔的状态到本地
                    ArchivePPT *archive = [ArchivePPT new];
                    archive.indexColor = weakSelf.colorIndex;
                    if (weakSelf.currentType  == setTypeEraser) {
                        archive.pen = NO;
                    }else{
                        archive.pen = YES;
                    }
                    switch (weakSelf.lineWidth) {
                        case 4:
                        {
                            archive.indexWidth = 0;
                        }
                            break;
                        case 8:
                        {
                            archive.indexWidth = 1;
                        }
                            break;
                        case 12:
                        {
                            archive.indexWidth = 2;
                        }
                            break;
                        default:
                        {
                            archive.indexWidth = 0;
                        }
                            break;
                    }
                    switch (weakSelf.lineErazorWidth) {
                        case 10:
                        {
                            archive.indexErazorWidth = 0;
                        }
                            break;
                        case 15:
                        {
                            archive.indexErazorWidth = 1;
                        }
                            break;
                        case 20:
                        {
                            archive.indexErazorWidth = 2;
                        }
                            break;
                        default:
                        {
                            archive.indexErazorWidth= 0;
                        }
                            break;
                    }
                    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
                    NSString *filePath = [path stringByAppendingPathComponent:@"ppt.data"];
                    [NSKeyedArchiver archiveRootObject:archive toFile:filePath];
                    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseSign) successBlock:^(id result) {
                    }];
                    if ([weakSelf.delegate respondsToSelector:@selector(closeBoard)]) {
                        [weakSelf.delegate closeBoard];
                    }
                }
                    break;
                default:
                    break;
            }
        };
        NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
        NSString *filePath = [path stringByAppendingPathComponent:@"ppt.data"];
        ArchivePPT *archieve = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
        if (archieve != nil) {
            if (archieve.indexWidth == 0) {
                weakSelf.lineWidth  = 4;
            }else if (archieve.indexWidth == 1){
                weakSelf.lineWidth  = 8;
            }else if (archieve.indexWidth == 2){
                weakSelf.lineWidth  = 12;
            }else{
                weakSelf.lineWidth  = 4;
            }
            if (archieve.indexErazorWidth == 0) {
                weakSelf.lineErazorWidth = 10;
            }else if (archieve.indexErazorWidth == 1){
                weakSelf.lineErazorWidth = 15;
            }else if (archieve.indexErazorWidth == 2){
                weakSelf.lineErazorWidth = 20;
            }else{
                weakSelf.lineErazorWidth = 10;
            }
            weakSelf.colorIndex = archieve.indexColor;
            weakSelf.lineColor = [UIColor colorWithHexString:weakSelf.settingBoard.colors[archieve.indexColor]];
            weakSelf.lastColor = weakSelf.lineColor;
            if (archieve.pen) {
                weakSelf.currentType = setTypePen;
            }else{
                weakSelf.currentType = setTypeEraser;
            }
            weakSelf.colorIndex = archieve.indexColor;
        }
        [_settingBoard initialPenSet];
    }
    return _settingBoard;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ImageBoardNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SendColorAndWidthNotification object:nil];
}
@end

#pragma mark - HBPath
@interface HBPath()
@property (nonatomic, assign) CGPoint beginPoint;
@property (nonatomic, assign) CGFloat pathWidth;
@end

@implementation HBPath
+ (instancetype)pathToPoint:(CGPoint)beginPoint pathWidth:(CGFloat)pathWidth isEraser:(BOOL)isEraser
{
    HBPath *path = [[HBPath alloc] init];
    path.beginPoint = beginPoint;
    path.pathWidth = pathWidth;
    path.isEraser = isEraser;
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    bezierPath.lineWidth = pathWidth;
    bezierPath.lineCapStyle = kCGLineCapRound;
    bezierPath.lineJoinStyle = kCGLineJoinRound;
    [bezierPath moveToPoint:beginPoint];
    path.bezierPath = bezierPath;
    return path;
}
//HBDrawingShapeCurve = 0,//曲线
//HBDrawingShapeLine,//直线
//HBDrawingShapeEllipse,//椭圆
//HBDrawingShapeRect,//矩形
- (void)pathLineToPoint:(CGPoint)movePoint WithType:(HBDrawingShapeType)shapeType
{
    //判断绘图类型
    _shapType = shapeType;
    switch (shapeType) {
        case HBDrawingShapeCurve:
        {
            [self.bezierPath addLineToPoint:movePoint];
        }
            break;
        case HBDrawingShapeLine:
        {
            self.bezierPath = [UIBezierPath bezierPath];
            self.bezierPath.lineCapStyle = kCGLineCapRound;
            self.bezierPath.lineJoinStyle = kCGLineJoinRound;
            self.bezierPath.lineWidth = self.pathWidth;
            [self.bezierPath moveToPoint:self.beginPoint];
            [self.bezierPath addLineToPoint:movePoint];
        }
            break;
        case HBDrawingShapeEllipse:
        {
            self.bezierPath = [UIBezierPath bezierPathWithRect:[self getRectWithStartPoint:self.beginPoint endPoint:movePoint]];
            self.bezierPath.lineCapStyle = kCGLineCapRound;
            self.bezierPath.lineJoinStyle = kCGLineJoinRound;
            self.bezierPath.lineWidth = self.pathWidth;
        }
            break;
        case HBDrawingShapeRect:
        {
            self.bezierPath = [UIBezierPath bezierPathWithOvalInRect:[self getRectWithStartPoint:self.beginPoint endPoint:movePoint]];
            self.bezierPath.lineCapStyle = kCGLineCapRound;
            self.bezierPath.lineJoinStyle = kCGLineJoinRound;
            self.bezierPath.lineWidth = self.pathWidth;
        }
            break;
        default:
            break;
    }
//    self.shape.path = self.bezierPath.CGPath;
}

- (CGRect)getRectWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint
{
    CGPoint orignal = startPoint;
    if (startPoint.x > endPoint.x) {
        orignal = endPoint;
    }
    CGFloat width = fabs(startPoint.x - endPoint.x);
    CGFloat height = fabs(startPoint.y - endPoint.y);
    return CGRectMake(orignal.x , orignal.y , width, height);
}

@end

@implementation HBDrawView
+ (Class)layerClass
{
    return [CAShapeLayer class];
}
- (void)setBrush:(HBPath *)path
{
    CAShapeLayer *shapeLayer = (CAShapeLayer *)self.layer;
    shapeLayer.strokeColor = path.pathColor.CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.lineJoin = kCALineJoinRound;
    shapeLayer.lineCap = kCALineCapRound;
    shapeLayer.lineWidth = path.bezierPath.lineWidth;
    ((CAShapeLayer *)self.layer).path = path.bezierPath.CGPath;
}


@end
