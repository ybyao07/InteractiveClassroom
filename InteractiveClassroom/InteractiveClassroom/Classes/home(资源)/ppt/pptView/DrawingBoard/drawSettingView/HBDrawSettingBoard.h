//
//  HBDrawSettingBoard.h
//  DemoAntiAliasing
//
//  Created by 伍宏彬 on 15/11/4.
//  Copyright © 2015年 HB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HBDrawCommon.h"

typedef NS_ENUM(NSInteger,setType) {
    setTypePen,
    setTypeCamera,
    setTypeAlbum,
    setTypeSave,
    setTypeEraser,
    setTypeBack,
    setTyperegeneration,
    setTypeClearAll,
    setTypeClose
};

typedef void(^boardSettingBlock)(setType type);

@interface HBDrawSettingBoard : UIView

@property (nonatomic, copy) boardSettingBlock settingBlock;
@property (nonatomic, strong) NSArray *colors;

- (NSInteger)getLineWidth;
- (NSInteger)getLineErazorWidth;
- (UIColor *)getLineColor;


- (void)initialPenSet; // 初始选择的默认设置

@end

//画笔展示的球
@interface HBColorBall : UIView
@property (nonatomic, strong) UIColor *ballColor;
@property (nonatomic, assign) CGFloat ballSize;
@property (nonatomic, assign) NSInteger lineWidth;
@property (nonatomic, assign) NSInteger lineErazorWidth;

@end
