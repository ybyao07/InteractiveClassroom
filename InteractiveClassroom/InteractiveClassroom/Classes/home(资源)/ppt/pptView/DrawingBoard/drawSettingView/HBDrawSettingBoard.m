//
//  HBDrawSettingBoard.m
//  DemoAntiAliasing
//
//  Created by 伍宏彬 on 15/11/4.
//  Copyright © 2015年 HB. All rights reserved.
//

#import "HBDrawSettingBoard.h"
#import "MJExtension.h"
#import "HBBallColorModel.h"
#import "HBBackImageBoard.h"
#import "UIColor+help.h"
#import "UIView+Extension.h"
#import "UIButton+ImageTitleCentering.h"
#import "ArchivePPT.h"

static NSString * const collectionCellID = @"collectionCellID";

@interface HBDrawSettingBoard()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSIndexPath *_lastIndexPath;
    NSInteger _lineWidth;     // 线的宽度
    UIColor *_lineColor;    // 线的颜色
    NSInteger _lineErazorWidth;   // 橡皮擦的宽度
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *colorSelectModels;

@property (weak, nonatomic) IBOutlet UIButton *btnChexiao;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnExit;



@property (strong, nonatomic) UIButton *currentSizeBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *sizeBtns;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *sizeErazorBtns;

@property (weak, nonatomic) IBOutlet UIButton *btnInitialErazorOne;
@property (weak, nonatomic) IBOutlet UIButton *btnInitialPenTwo;


@property (weak, nonatomic) IBOutlet UIButton *btnPen;
@property (weak, nonatomic) IBOutlet UIButton *btnErazor;

@property (nonatomic, assign) setType currentType; //记录是Pen还是 Erazor

@end

@implementation HBDrawSettingBoard

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.btnChexiao centerVerticallyWithPadding:-4];
    [self.btnClear centerVerticallyWithPadding:-4];
    [self.btnSave centerVerticallyWithPadding:-4];
    [self.btnExit centerVerticallyWithPadding:-4];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:collectionCellID];
    self.btnInitialPenTwo.selected = YES;
    self.btnInitialErazorOne.selected = YES;
    self.currentType = setTypePen;
}
- (NSInteger)getLineWidth
{
    return _lineWidth;
}
- (UIColor *)getLineColor
{
    return _lineColor;
}

- (NSInteger)getLineErazorWidth
{
    return _lineErazorWidth;
}

- (IBAction)penSetting:(id)sender {
    self.btnErazor.backgroundColor = [UIColor clearColor];
    self.btnPen.backgroundColor = [UIColor whiteColor];
    self.currentType = setTypePen;
    if (self.settingBlock) {
        self.settingBlock(setTypePen);
    }
    
}

- (IBAction)saveImage:(id)sender {
    if (self.settingBlock) {
        self.settingBlock(setTypeSave);
    }
}
- (IBAction)eraser:(id)sender {
    self.btnPen.backgroundColor = [UIColor clearColor];
    self.btnErazor.backgroundColor = [UIColor whiteColor];
    self.currentType = setTypeEraser;
    if (self.settingBlock) {
        self.settingBlock(setTypeEraser);
    }
}
- (IBAction)back:(id)sender {
    if (self.settingBlock) {
        self.settingBlock(setTypeBack);
    }
}
- (IBAction)clearAll:(id)sender {
    if (self.settingBlock) {
        self.settingBlock(setTypeClearAll);
    }
}
- (IBAction)close:(id)sender {
    if (self.settingBlock) {
        self.settingBlock(setTypeClose);
    }
}

#pragma mark =======  画笔大小 =======
- (IBAction)changePenSize:(UIButton *)btn
{
    [self.sizeBtns enumerateObjectsUsingBlock:^(UIButton *  _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (button.tag == btn.tag) {
            button.selected = YES;
        }else{
            button.selected = NO;
        }
    }];
    if (btn.tag == 0) {
        _lineWidth = 4;
    }else if (btn.tag == 1){
        _lineWidth = 8;
    }else{
        _lineWidth = 12;
    }
    HDLog(@"the line width:%d",(int)_lineWidth);
    self.btnErazor.backgroundColor = [UIColor clearColor];
    self.btnPen.backgroundColor = [UIColor whiteColor];
    if (self.currentType == setTypeEraser) {
        if (self.settingBlock) {
            self.currentType = setTypePen;
            self.settingBlock(setTypePen);
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:SendColorAndWidthNotification object:nil userInfo:@{@"lineWidth":@(_lineWidth)}];
}
- (IBAction)changeErazorSize:(UIButton *)btn
{
    [self.sizeErazorBtns enumerateObjectsUsingBlock:^(UIButton *  _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (button.tag == btn.tag) {
            button.selected = YES;
        }else{
            button.selected = NO;
        }
    }];
    if (btn.tag == 0) {
        _lineErazorWidth = 10;
    }else if (btn.tag == 1){
        _lineErazorWidth = 15;
    }else{
        _lineErazorWidth = 20;
    }
    // 点击之后就切换Type
    self.btnErazor.backgroundColor = [UIColor whiteColor];
    self.btnPen.backgroundColor = [UIColor clearColor];
    if (self.currentType == setTypePen) {
        if (self.settingBlock) {
            self.currentType = setTypeEraser;
            self.settingBlock(setTypeEraser);
        }
    }
    HDLog(@"the erazor width :%d",(int)_lineErazorWidth);
    [[NSNotificationCenter defaultCenter] postNotificationName:SendColorAndWidthNotification object:nil userInfo:@{@"lineErazorWidth":@(_lineErazorWidth)}];

}

- (void)changPenSizeView:(UIButton *)btn
{
    [self.sizeBtns enumerateObjectsUsingBlock:^(UIButton *  _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (button.tag == btn.tag) {
            button.selected = YES;
        }else{
            button.selected = NO;
        }
    }];
}
- (void)changEraSizeView:(UIButton *)btn
{
    [self.sizeErazorBtns enumerateObjectsUsingBlock:^(UIButton *  _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (button.tag == btn.tag) {
            button.selected = YES;
        }else{
            button.selected = NO;
        }
    }];
}
- (void)initialPenSet
{
    HBBallColorModel *model = self.colorSelectModels[0];
    model.isBallColor = YES;
    _lineColor = [UIColor colorWithHexString:self.colors[[model.ballColor integerValue]]];
    _lineWidth = 8;
    _lineErazorWidth = 10;
    _lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [path stringByAppendingPathComponent:@"ppt.data"];
    ArchivePPT *archieve = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    if (archieve != nil) {
        if (archieve.pen) {
            self.currentType = setTypePen;
            if (self.settingBlock) {
                self.settingBlock(setTypePen);
            }
            self.btnErazor.backgroundColor = [UIColor clearColor];
            self.btnPen.backgroundColor = [UIColor whiteColor];
        }else{
            self.currentType = setTypeEraser;
            if (self.settingBlock) {
                self.settingBlock(setTypeEraser);
            }
            self.btnErazor.backgroundColor = [UIColor whiteColor];
            self.btnPen.backgroundColor = [UIColor clearColor];
        }
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if (archieve.indexWidth == 0) {
            dic[@"penThickness"] = @(4);
            _lineWidth  = 4;
        }else if (archieve.indexWidth == 1){
            dic[@"penThickness"] = @(8);
            _lineWidth  = 8;
        }else if (archieve.indexWidth == 2){
            dic[@"penThickness"] = @(12);
            _lineWidth  = 12;
        }else{
            dic[@"penThickness"] = @(4);
            _lineWidth  = 4;
        }
        //重置pen的大小
        UIButton *btnPen = [UIButton buttonWithType:UIButtonTypeCustom];
        btnPen.tag = archieve.indexWidth;
        [self changPenSizeView:btnPen];
        if (archieve.indexErazorWidth == 0) {
            dic[@"eraseThickness"] = @(10);
            _lineErazorWidth = 10;
        }else if (archieve.indexErazorWidth == 1){
            dic[@"eraseThickness"] = @(15);
            _lineErazorWidth = 15;
        }else if (archieve.indexErazorWidth == 2){
            dic[@"eraseThickness"] = @(20);
            _lineErazorWidth = 20;
        }else{
            dic[@"eraseThickness"] = @(10);
            _lineErazorWidth = 10;
        }
        //设置橡皮擦
        UIButton *btnEra = [UIButton buttonWithType:UIButtonTypeCustom];
        btnEra.tag = archieve.indexErazorWidth;
        [self changEraSizeView:btnEra];
        int indexColor = (int)archieve.indexColor;
        dic[@"penColor"] = self.colors[indexColor];
        [self.colorSelectModels enumerateObjectsUsingBlock:^(HBBallColorModel *  _Nonnull colorM, NSUInteger idx, BOOL * _Nonnull stop) {
            colorM.isBallColor = NO;
        }];
        HBBallColorModel *model = self.colorSelectModels[indexColor];
        model.isBallColor = YES;
        _lineColor = [UIColor colorWithHexString:self.colors[[model.ballColor integerValue]]];
        _lastIndexPath = [NSIndexPath indexPathForRow:indexColor inSection:0];
        [self.collectionView reloadData];
        [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(SignSettings) successBlock:^(id result) {
        }];
    }else{
        if (self.settingBlock) {
            self.settingBlock(setTypePen);
        }
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"penThickness"] = @(_lineWidth);
        dic[@"penColor"] = [UIColor hexStringFromColor:_lineColor];
        dic[@"eraseThickness"] = @(_lineErazorWidth);
        [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(SignSettings) successBlock:^(id result) {
        }];
    }
}

#pragma mark - collectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.colorSelectModels.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionCellID forIndexPath:indexPath];
    HBBallColorModel *model = self.colorSelectModels[indexPath.item];
    cell.backgroundColor = [UIColor colorWithHexString:self.colors[[model.ballColor integerValue]]];
    cell.layer.cornerRadius = 3;
    if (model.isBallColor) {
        cell.layer.borderWidth = 3;
        cell.layer.borderColor = [UIColor purpleColor].CGColor;
    }else{
        cell.layer.borderWidth = 0;
    }
    cell.layer.masksToBounds = YES;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_lastIndexPath) {
        HBBallColorModel *lastModel = self.colorSelectModels[_lastIndexPath.item];
        lastModel.isBallColor = NO;
        [self.collectionView reloadItemsAtIndexPaths:@[_lastIndexPath]];
    }
    _lastIndexPath = indexPath;
    HBBallColorModel *model = self.colorSelectModels[indexPath.item];
    _lineColor = [UIColor colorWithHexString:self.colors[[model.ballColor integerValue]]];
    self.btnErazor.backgroundColor = [UIColor clearColor];
    self.btnPen.backgroundColor = [UIColor whiteColor];
    if (self.currentType == setTypeEraser) {
        self.currentType = setTypePen;
        if (self.settingBlock) {
            self.settingBlock(setTypePen);
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:SendColorAndWidthNotification object:nil userInfo:@{@"lineColor":_lineColor,@"colorIndex":@([model.ballColor intValue])}];
    model.isBallColor = YES;
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

#pragma mark - lazy
- (NSArray *)colorSelectModels
{
    if (!_colorSelectModels) {
        NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@(0),@"ballColor", nil];
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@(1),@"ballColor", nil];
        NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@(2),@"ballColor", nil];
        NSDictionary *dic4 = [NSDictionary dictionaryWithObjectsAndKeys:@(3),@"ballColor", nil];
        NSArray *array = [NSArray arrayWithObjects:dic1,
                                            dic2,
                                            dic3,
                                            dic4,
                                            nil];
        _colorSelectModels = [HBBallColorModel mj_objectArrayWithKeyValuesArray:array];
    }
    return _colorSelectModels;
}
- (NSArray *)colors
{
    if (!_colors) {
        _colors = [NSArray arrayWithObjects:@"#ff0000",
                                            @"#f8e71c",
                                            @"#0000ff",
                                            @"#000000",
                                            nil];
    }
    return _colors;
}
- (void)dealloc
{
    
}
@end


