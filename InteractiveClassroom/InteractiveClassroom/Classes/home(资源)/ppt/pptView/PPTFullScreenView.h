//
//  PPTFullScreenView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawView.h"
#import "EditView.h"

@interface PPTFullScreenView : UIView

@property (nonatomic, strong) UIImageView *bigImgView;

@end
