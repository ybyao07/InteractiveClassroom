//
//  PPTFullScreenView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTFullScreenView.h"
#import "HBDrawingBoard.h"
#import "MJExtension.h"
#import "UIView+Extension.h"
@interface PPTFullScreenView()<HBDrawingBoardDelegate>

@property (nonatomic, strong) HBDrawingBoard *drawView;

@end
@implementation PPTFullScreenView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.drawView];
    }
    return self;
}



#pragma mark   ==================  HBDrawingBoardDelegate =========
- (void)drawBoard:(HBDrawingBoard *)drawView drawingStatus:(HBDrawingStatus)drawingStatus model:(HBDrawModel *)model{
    NSLog(@"%@",model.mj_keyValues);
}
- (void)closeBoard
{
    [self.drawView removeFromSuperview];
    self.drawView = nil;
    [self removeFromSuperview];
}

- (HBDrawingBoard *)drawView
{
    if (!_drawView) {
        _drawView = [[HBDrawingBoard alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.bounds.size.height)];
        _drawView.delegate = self;
    }
    return _drawView;
}
- (void)setBigImgView:(UIImageView *)bigImgView
{
    _bigImgView = bigImgView;
    self.drawView.backImage.image = bigImgView.image;
}

- (void)willRemoveSubview:(UIView *)subview
{
    [super willRemoveSubview:subview];
}
- (void)dealloc
{
    
}

@end
