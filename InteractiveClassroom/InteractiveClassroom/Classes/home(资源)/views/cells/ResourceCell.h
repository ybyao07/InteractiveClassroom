//
//  ResourceCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResourceModel.h"
#import "ResourcePageViewController.h"
@interface ResourceCell : UICollectionViewCell

@property (nonatomic, assign) ResourceTag resourceTag;
@property (strong, nonatomic) ResourceModel *model;

@end
