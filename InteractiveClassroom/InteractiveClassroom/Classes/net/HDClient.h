//
//  HDClient.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CocoaAsyncSocket.h"

typedef void(^BLOCK)(id result);

@interface HDClient : NSObject<GCDAsyncSocketDelegate,NSStreamDelegate>

@property (nonatomic,strong) GCDAsyncSocket *socket;
@property (nonatomic,copy) BLOCK block;
@property (nonatomic,assign) NSInteger counter;

//#define IP @"192.168.1.108"
//#define PORT 9999

+ (HDClient *)shareInstance;
- (void)sendMessage:(NSString *)str complete:(BLOCK)block;

- (void)sendData:(NSData *)data complete:(BLOCK)block;



// ------- 同步发送 ----
- (void)sendSynData:(NSData *)data;


@end
