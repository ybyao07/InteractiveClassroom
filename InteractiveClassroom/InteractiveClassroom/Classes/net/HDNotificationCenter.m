//
//  HDNotificationCenter.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDNotificationCenter.h"

@implementation HDNotificationCenter

NSString * const kUseDisconnectInternet = @"kUseDisconnectInternet";
NSString * const kUseWiFiConnectInternet = @"kUseWiFiConnectInternet";
NSString * const kUseMobileNetworkConnectInternet = @"kUseMobileNetworkConnectInternet";
NSString * const kUseWifiInternetStatus = @"kUseWifiInternetStatus";

NSString * const kLinkNotification = @"kLinkNotification";

NSString * const kOpenOrCloseViewNotification = @"kOpenOrCloseViewNotification";

NSString * const kCloseVoteNotification = @"kCloseVoteNotification";


NSString * const kMainClassOpenOrCloseViewNotification = @"kMainClassOpenOrCloseViewNotification";


NSString * const kUseDefaultIP = @"kUseDefaultIP";
NSString * const kUseDefaultPort = @"kUseDefaultPort";
NSString * const kUseDefaultToken = @"kUseDefaultToken";



@end
