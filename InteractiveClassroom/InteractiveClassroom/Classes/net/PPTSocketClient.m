//
//  PPTSocketClient.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTSocketClient.h"


@interface PPTSocketClient()
{
    NSOutputStream *_outputStream;//对应输出流
}
@end

@implementation PPTSocketClient

+ (PPTSocketClient *)shareInstance
{
    static PPTSocketClient *client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[PPTSocketClient alloc] init];
    });
    return client;
}
- (instancetype)init
{
    if (self = [super init]) {
//        //1.建立连接
//        NSString *host = [[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultIP];
//        CFReadStreamRef readStream;
//        CFWriteStreamRef writeStream;
//        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, PPT_PORT, &readStream, &writeStream);
//        _outputStream = (__bridge NSOutputStream *)(writeStream);
//        _outputStream.delegate= self;
    }
    return self;
}


#pragma mark =======  同步发送 =====
- (void)sendSynData:(NSData *)data
{
    if (_outputStream == nil) {
        //1.建立连接
        NSString *host = [[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultIP];
        CFReadStreamRef readStream;
        CFWriteStreamRef writeStream;
        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, PPT_PORT, &readStream, &writeStream);
        _outputStream = (__bridge NSOutputStream *)(writeStream);
        _outputStream.delegate= self;
        [_outputStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [_outputStream open];
    }
    [_outputStream write:data.bytes maxLength:data.length];
}

-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    NSLog(@"%@",[NSThread currentThread]);
    switch(eventCode) {
        case NSStreamEventOpenCompleted:
            HDLog(@"输入输出流打开完成");
            break;
        case NSStreamEventHasBytesAvailable:
            HDLog(@"有字节可读");
            break;
        case NSStreamEventHasSpaceAvailable:
            HDLog(@"可以发送字节");
            break;
        case NSStreamEventErrorOccurred:
            HDLog(@"连接出现错误");
            break;
        case NSStreamEventEndEncountered:
        {
            HDLog(@"连接结束");
            [aStream close];
            aStream = nil;
            //从主运行循环移除
            [aStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        }
            break;
        default:
            break;
    }
}
@end
