//
//  HDWifiView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDWifiView.h"
#import "UIView+Extension.h"
#import "ScanContainView.h"
#import "HDNetTools.h"

@interface HDWifiView()<ScanContaiViewDelegate>

@property (nonnull, strong) UILabel *wifiStatusTitle;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) CGFloat viewWidth;
@property (nonatomic, assign) CGFloat viewHeight;

@property (strong, nonatomic) UIView *wifiView;
@property (strong, nonatomic) ScanContainView *erCodeView; // 二维码页面

@end

@implementation HDWifiView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changWifiStatus:) name:kUseWifiInternetStatus object:nil];
        [self initSelfWithFrame:frame];
    }
    return self;
}
- (void)initSelfWithFrame:(CGRect)frame{
    _scrollView = [[UIScrollView alloc] initWithFrame:frame];
    [_scrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.directionalLockEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.alwaysBounceHorizontal = NO;
    _scrollView.alwaysBounceVertical = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];

    _viewWidth = CGRectGetWidth(frame);
    _viewHeight = CGRectGetHeight(frame);
    
    [_scrollView addSubview:self.wifiView];
    
    _scrollView.contentSize = CGSizeMake(1 *_viewWidth, _viewHeight);
}



- (UIView *)wifiView
{
    if (!_wifiView) {
        _wifiView = [[UIView alloc] initWithFrame:self.frame];
        
        UILabel *title = [[UILabel alloc] initWithFrame:Rect(20, 20, SCREEN_WIDTH - 40, 50)];
        self.wifiStatusTitle = title;
        title.text = @"当前WiFi:未连接到WiFi";
        title.textColor = JUIColorFromRGB_Major1;
        title.font = JFONT_BOLD(JFONT_Size_H1);
        title.textAlignment = NSTextAlignmentCenter;
        title.hidden = YES;
        [_wifiView addSubview:title];
        UILabel *hint = [[UILabel alloc] initWithFrame:Rect(20, 20, SCREEN_WIDTH - 40, 20)];
        hint.text = @"请将平板端和授课端连接至同一WiFi环境下";
        hint.textAlignment = NSTextAlignmentCenter;
        hint.font = JFONT(JFONT_Size_H2);
        [_wifiView addSubview:hint];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_wifi"]];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        [imgView sizeToFit];
        imgView.center = _wifiView.center;
        [_wifiView addSubview:imgView];
        hint.centerY = _wifiView.centerY - imgView.bounds.size.height/2 - 50;
        title.centerY = hint.centerY - 30;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"扫描授课端二维码" forState:UIControlStateNormal];
        [btn setTitleColor:JUIColorFromRGB_White forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"icon_btn_selected"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(goScanView) forControlEvents:UIControlEventTouchUpInside];
        [btn sizeToFit];
        btn.titleLabel.font = JFONT(JFONT_Size_H2);
        btn.frame = Rect(0, CGRectGetMaxY(imgView.frame)+20, btn.bounds.size.width+40, btn.bounds.size.height+20);
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 8;
        btn.centerX = imgView.centerX;
        [_wifiView addSubview:btn];
        _wifiView.backgroundColor = JUIColorFromRGB_bgGrayBlue;
    }
    return _wifiView;
}

- (ScanContainView *)erCodeView
{
    if (!_erCodeView) {
        _erCodeView = [[ScanContainView alloc] initWithFrame:Rect(0, 0, _viewWidth, _viewHeight)];
        _erCodeView.backgroundColor = [UIColor clearColor];
        _erCodeView.delegate = self;
    }
    return _erCodeView;
}

- (void)changWifiStatus:(NSNotification *)notif
{
    NSNumber  *isWifi = notif.userInfo[@"isWifi"];
    self.wifiStatusTitle.hidden = ([isWifi intValue] == 1);
}

#pragma mark ========== 进入扫描二维码的页面 ========
- (void)goScanView
{
    if (!_erCodeView) {
        [self.scrollView addSubview:self.erCodeView];
        self.erCodeView.frame = Rect(_viewWidth,0, _viewWidth, _viewHeight);
        _scrollView.contentSize = CGSizeMake(2 *_viewWidth, _viewHeight);
        CGPoint offset = CGPointMake(_viewWidth, 0);
        [_scrollView setContentOffset:offset animated:YES];
    }else{
        CGPoint offset = CGPointMake(_viewWidth, 0);
        [_scrollView setContentOffset:offset animated:YES];
    }
}



#pragma mark ====== ScanContaiViewDelegate =====
- (void)scanContainViewBack
{
    [_scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
