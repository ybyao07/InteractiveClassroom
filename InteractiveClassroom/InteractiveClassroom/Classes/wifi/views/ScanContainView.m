//
//  ScanContainView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ScanContainView.h"
#import "ScannerView.h"
#import "HDNetTools.h"

@interface ScanContainView()<AVCaptureMetadataOutputObjectsDelegate>
{
    ScannerView *scannerView;
    
    CGRect scanRect;
    
    BOOL _isScanning;
}

@property (strong, nonatomic) AVCaptureDevice *device;
@property (strong, nonatomic) AVCaptureDeviceInput *input;
@property (strong, nonatomic) AVCaptureMetadataOutput *output;
@property (strong, nonatomic) AVCaptureSession *session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *preview;

@property (strong, nonatomic) UIButton *btnBack;



@property (strong, nonatomic) UILabel *inforTitle;
@property (strong, nonatomic) UILabel *inforHint;


@property (strong, nonatomic) UILabel *inforLabel;

@end
@implementation ScanContainView
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _isScanning = NO;
        [self createUI];
        [self initScanner];
    }
    return self;
}

- (void)createUI
{
    scannerView = [[ScannerView alloc] initWithFrame:self.frame];
    [self addSubview:scannerView];
    CGRect targetRect = [scannerView getBoxFrame];
    scanRect = targetRect;
    
    self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnBack setImage:[UIImage imageNamed:@"icon_erwei_back"] forState:UIControlStateNormal];
    self.btnBack.frame = Rect(60, 40, 22, 18);
    [self.btnBack addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnBack];
    
    CGFloat bottomY = targetRect.origin.y;
    self.inforTitle = [[UILabel alloc] initWithFrame:Rect(0, bottomY - 100, SCREEN_WIDTH, 40)];
    self.inforTitle.font = JFONT_BOLD(JFONT_Size_H1);
    self.inforTitle.text = @"扫描二维码";
    self.inforTitle.textColor = JUIColorFromRGB_Major1;
    self.inforTitle.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.inforTitle];
    
    self.inforHint = [[UILabel alloc] initWithFrame:Rect(0, CGRectGetMaxY(self.inforTitle.frame)+10, SCREEN_WIDTH, 20)];
    self.inforHint.font = JFONT(JFONT_Size_H2);
    self.inforHint.textAlignment = NSTextAlignmentCenter;
    self.inforHint.text = @"请将平板端和授课端连接至同一WiFi环境下";
    self.inforHint.textColor = [UIColor whiteColor];
    [self addSubview:self.inforHint];
    //扫描提示
    self.inforLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(targetRect) + 10, SCREEN_WIDTH, 40)];
    self.inforLabel.numberOfLines = 0;
    self.inforLabel.textColor = [UIColor whiteColor];
    self.inforLabel.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.inforLabel.shadowOffset = CGSizeMake(self.inforLabel.frame.size.width, 3);
    self.inforLabel.text = @"请将二维码放入框内";
    self.inforLabel.font = kFont16;
    self.inforLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.inforLabel];
}


#pragma mark - 摄像头相关
- (void)initScanner{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请开启相机访问权限,否则无法进行扫描" message:@"设置-->隐私-->相机-->«互动课堂»--> 开启开关" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
        [alert show];
        return ;
    }
    self.inforLabel.hidden = NO;
    _device = nil;
    _input = nil;
    _output = nil;
    _session = nil;
    _preview = nil;
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    _output.rectOfInterest = Rect(scanRect.origin.y/(SCREEN_HEIGHT-64), scanRect.origin.x/SCREEN_WIDTH, scanRect.size.height/(SCREEN_HEIGHT-64), scanRect.size.width/SCREEN_WIDTH);
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input]){
        [_session addInput:self.input];
    }
    if ([_session canAddOutput:self.output]){
        [_session addOutput:self.output];
    }
    // 二维码类型 AVMetadataObjectTypeQRCode
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];//二维码
//    [_output setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code,nil]];
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:_session];
    _preview.videoGravity =AVLayerVideoGravityResizeAspectFill;
//    _preview.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    _preview.connection.videoOrientation = [self videoOrientationFromCurrentDeviceOrientation];    //指定屏幕方向
    [_preview setFrame:self.layer.bounds];
    [self.layer insertSublayer:_preview atIndex:0];
    [self fireTimer];
    [_session startRunning];
}

- (AVCaptureVideoOrientation) videoOrientationFromCurrentDeviceOrientation {
    switch ([[UIApplication sharedApplication]statusBarOrientation]) {
        case UIInterfaceOrientationPortrait: {
            return AVCaptureVideoOrientationPortrait;
        }
        case UIInterfaceOrientationLandscapeLeft: {
            return AVCaptureVideoOrientationLandscapeLeft;
        }
        case UIInterfaceOrientationLandscapeRight: {
            return AVCaptureVideoOrientationLandscapeRight;
        }
        case UIInterfaceOrientationPortraitUpsideDown: {
            return AVCaptureVideoOrientationPortraitUpsideDown;
        }
    }
    return AVCaptureVideoOrientationLandscapeLeft;
}

#pragma mark - 扫描结果 代理
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    //    NSLog(@"%@",metadataObjects);
    NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~");
    NSString *stringValue;
    NSString *type;
    if ([metadataObjects count] >0){
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
        type = metadataObject.type;
        if ([type hasSuffix:@"QRCode"]) {//二维码
            NSData *jsonData = [stringValue dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
            NSDictionary *dicInfo = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
#warning test
            [[NSUserDefaults standardUserDefaults] setObject:dicInfo[@"ip"] forKey:kUseDefaultIP];
            [[NSUserDefaults standardUserDefaults] setObject:dicInfo[@"port"] forKey:kUseDefaultPort];
//            [[NSUserDefaults standardUserDefaults] setObject:@"192.168.1.107" forKey:kUseDefaultIP];
//            [[NSUserDefaults standardUserDefaults] setObject:@"9999" forKey:kUseDefaultPort];
            [[NSUserDefaults standardUserDefaults] setObject:dicInfo[@"token"] forKey:kUseDefaultToken];
            if (dicInfo[@"chapterName"] != nil && dicInfo[@"chapterName"] != [NSNull null]) {
                [[NSUserDefaults standardUserDefaults] setValue:dicInfo[@"chapterName"] forKey:@"chapterName"];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            //获取授课端IP地址端口号等信息，保存到本地
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"扫描条码成功" message: [NSString stringWithFormat:@"%@",stringValue]   delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"打开",nil];
//            [alert show];
//            [self scannerPause];
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"code"] = @(Connected);
            dic[@"ip"] = [HDNetTools localWiFiIPAddress];
            dic[@"port"] = @(9999);
            dic[@"token"] = [[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultToken];
            NSError *error;
            NSData *socketData = [NSJSONSerialization dataWithJSONObject:dic
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            [[HDClient shareInstance] sendData:socketData complete:^(id result) {
            }];
             [self scannerPause];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLinkNotification object:nil];
        }
    }
}


- (void)scannerRestart{
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session startRunning];
    [scannerView beginLineAnimation];
}

- (void)scannerPause{
    [_session stopRunning];
    [scannerView stopLineAnimation];
    [_output setMetadataObjectsDelegate:nil queue:dispatch_get_main_queue()];
}



- (void)backAction
{
    if ([self.delegate respondsToSelector:@selector(scanContainViewBack)]) {
        [self.delegate scanContainViewBack];
    }
}



- (void)fireTimer{
    [scannerView beginLineAnimation];
}




@end
